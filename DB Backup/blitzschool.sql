CREATE DATABASE  IF NOT EXISTS `blitzrye_blitzschool` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `blitzrye_blitzschool`;
-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: 103.53.43.45    Database: blitzrye_blitzschool
-- ------------------------------------------------------
-- Server version	5.6.41-84.1-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activities`
--

DROP TABLE IF EXISTS `activities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `activities` (
  `activitiesID` int(11) NOT NULL AUTO_INCREMENT,
  `activitiescategoryID` int(11) NOT NULL,
  `description` text NOT NULL,
  `create_date` datetime NOT NULL,
  `modify_date` datetime NOT NULL,
  `time_to` varchar(40) DEFAULT NULL,
  `time_from` varchar(40) DEFAULT NULL,
  `time_at` varchar(40) DEFAULT NULL,
  `usertypeID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `schoolyearID` int(11) NOT NULL,
  PRIMARY KEY (`activitiesID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activities`
--

LOCK TABLES `activities` WRITE;
/*!40000 ALTER TABLE `activities` DISABLE KEYS */;
/*!40000 ALTER TABLE `activities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `activitiescategory`
--

DROP TABLE IF EXISTS `activitiescategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `activitiescategory` (
  `activitiescategoryID` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `fa_icon` varchar(40) DEFAULT NULL,
  `schoolyearID` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `modify_date` datetime NOT NULL,
  `userID` int(11) NOT NULL,
  `usertypeID` int(40) NOT NULL,
  PRIMARY KEY (`activitiescategoryID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activitiescategory`
--

LOCK TABLES `activitiescategory` WRITE;
/*!40000 ALTER TABLE `activitiescategory` DISABLE KEYS */;
INSERT INTO `activitiescategory` VALUES (1,'Photos','fa-picture-o',19,'2017-04-30 09:04:15','2017-08-01 05:15:23',1,1),(2,'Food','fa-cutlery',19,'2017-04-30 02:28:09','2017-04-30 02:28:09',1,1),(3,'Sleep','fa-bed',19,'2017-04-30 02:51:08','2017-04-30 02:51:08',1,1),(4,'Sports','fa-trophy',19,'2017-04-30 02:52:04','2017-04-30 02:52:04',1,1),(5,'Activities','fa-puzzle-piece',19,'2017-04-30 02:52:36','2017-04-30 02:56:41',1,1),(6,'Note','fa-edit',19,'2017-04-30 02:55:08','2017-04-30 02:55:08',1,1),(7,'Incident','fa-times',19,'2017-04-30 03:00:54','2017-04-30 03:02:37',1,1),(8,'Meds','fa-medkit',19,'2017-04-30 03:02:47','2017-04-30 03:02:47',1,1),(9,'Art','fa-paint-brush',19,'2017-04-30 03:06:07','2017-04-30 03:06:07',1,1);
/*!40000 ALTER TABLE `activitiescategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `activitiescomment`
--

DROP TABLE IF EXISTS `activitiescomment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `activitiescomment` (
  `activitiescommentID` int(11) NOT NULL AUTO_INCREMENT,
  `activitiesID` int(11) NOT NULL,
  `comment` text NOT NULL,
  `schoolyearID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `usertypeID` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  PRIMARY KEY (`activitiescommentID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activitiescomment`
--

LOCK TABLES `activitiescomment` WRITE;
/*!40000 ALTER TABLE `activitiescomment` DISABLE KEYS */;
/*!40000 ALTER TABLE `activitiescomment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `activitiesmedia`
--

DROP TABLE IF EXISTS `activitiesmedia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `activitiesmedia` (
  `activitiesmediaID` int(11) NOT NULL AUTO_INCREMENT,
  `activitiesID` int(11) NOT NULL,
  `attachment` text NOT NULL,
  `create_date` datetime NOT NULL,
  PRIMARY KEY (`activitiesmediaID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activitiesmedia`
--

LOCK TABLES `activitiesmedia` WRITE;
/*!40000 ALTER TABLE `activitiesmedia` DISABLE KEYS */;
/*!40000 ALTER TABLE `activitiesmedia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `activitiesstudent`
--

DROP TABLE IF EXISTS `activitiesstudent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `activitiesstudent` (
  `activitiesstudentID` int(11) NOT NULL AUTO_INCREMENT,
  `activitiesID` int(11) NOT NULL,
  `studentID` int(11) NOT NULL,
  `classesID` int(11) NOT NULL,
  PRIMARY KEY (`activitiesstudentID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activitiesstudent`
--

LOCK TABLES `activitiesstudent` WRITE;
/*!40000 ALTER TABLE `activitiesstudent` DISABLE KEYS */;
/*!40000 ALTER TABLE `activitiesstudent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alert`
--

DROP TABLE IF EXISTS `alert`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `alert` (
  `alertID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `itemID` int(128) NOT NULL,
  `userID` int(11) NOT NULL,
  `usertypeID` int(11) NOT NULL,
  `itemname` varchar(128) NOT NULL,
  PRIMARY KEY (`alertID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alert`
--

LOCK TABLES `alert` WRITE;
/*!40000 ALTER TABLE `alert` DISABLE KEYS */;
/*!40000 ALTER TABLE `alert` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asset`
--

DROP TABLE IF EXISTS `asset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `asset` (
  `assetID` int(11) NOT NULL AUTO_INCREMENT,
  `serial` varchar(255) DEFAULT NULL,
  `description` text COMMENT 'Title',
  `manufacturer` varchar(255) DEFAULT NULL,
  `brand` varchar(255) DEFAULT NULL,
  `asset_number` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `asset_condition` int(11) DEFAULT NULL,
  `attachment` text,
  `originalfile` text,
  `asset_categoryID` int(11) DEFAULT NULL,
  `asset_locationID` int(11) DEFAULT NULL,
  `create_date` date NOT NULL,
  `modify_date` date NOT NULL,
  `create_userID` int(11) NOT NULL,
  `create_usertypeID` int(11) NOT NULL,
  PRIMARY KEY (`assetID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asset`
--

LOCK TABLES `asset` WRITE;
/*!40000 ALTER TABLE `asset` DISABLE KEYS */;
/*!40000 ALTER TABLE `asset` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asset_assignment`
--

DROP TABLE IF EXISTS `asset_assignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `asset_assignment` (
  `asset_assignmentID` int(11) NOT NULL AUTO_INCREMENT,
  `assetID` int(11) NOT NULL COMMENT 'Description and title',
  `usertypeID` int(11) DEFAULT NULL,
  `check_out_to` int(11) NOT NULL,
  `due_date` date DEFAULT NULL,
  `note` text,
  `assigned_quantity` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `asset_locationID` int(11) DEFAULT NULL,
  `check_out_date` date DEFAULT NULL,
  `check_in_date` date DEFAULT NULL,
  `create_date` date NOT NULL,
  `modify_date` date NOT NULL,
  `create_userID` int(11) NOT NULL,
  `create_usertypeID` int(11) NOT NULL,
  PRIMARY KEY (`asset_assignmentID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asset_assignment`
--

LOCK TABLES `asset_assignment` WRITE;
/*!40000 ALTER TABLE `asset_assignment` DISABLE KEYS */;
/*!40000 ALTER TABLE `asset_assignment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asset_category`
--

DROP TABLE IF EXISTS `asset_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `asset_category` (
  `asset_categoryID` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) NOT NULL,
  `create_date` date NOT NULL,
  `modify_date` date NOT NULL,
  `create_userID` int(11) NOT NULL,
  `create_usertypeID` int(11) NOT NULL,
  `active` int(11) NOT NULL,
  PRIMARY KEY (`asset_categoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asset_category`
--

LOCK TABLES `asset_category` WRITE;
/*!40000 ALTER TABLE `asset_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `asset_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `assignment`
--

DROP TABLE IF EXISTS `assignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `assignment` (
  `assignmentID` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `description` text NOT NULL,
  `deadlinedate` date NOT NULL,
  `usertypeID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `originalfile` text NOT NULL,
  `file` text NOT NULL,
  `classesID` longtext NOT NULL,
  `schoolyearID` int(11) NOT NULL,
  `sectionID` longtext,
  `subjectID` longtext,
  `assignusertypeID` int(11) DEFAULT NULL,
  `assignuserID` int(11) DEFAULT NULL,
  PRIMARY KEY (`assignmentID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assignment`
--

LOCK TABLES `assignment` WRITE;
/*!40000 ALTER TABLE `assignment` DISABLE KEYS */;
/*!40000 ALTER TABLE `assignment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `assignmentanswer`
--

DROP TABLE IF EXISTS `assignmentanswer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `assignmentanswer` (
  `assignmentanswerID` int(11) NOT NULL AUTO_INCREMENT,
  `assignmentID` int(11) NOT NULL,
  `schoolyearID` int(11) NOT NULL,
  `uploaderID` int(11) NOT NULL,
  `uploadertypeID` int(11) NOT NULL,
  `answerfile` text NOT NULL,
  `answerfileoriginal` text NOT NULL,
  `answerdate` date NOT NULL,
  PRIMARY KEY (`assignmentanswerID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assignmentanswer`
--

LOCK TABLES `assignmentanswer` WRITE;
/*!40000 ALTER TABLE `assignmentanswer` DISABLE KEYS */;
/*!40000 ALTER TABLE `assignmentanswer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `attendance`
--

DROP TABLE IF EXISTS `attendance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `attendance` (
  `attendanceID` int(200) unsigned NOT NULL AUTO_INCREMENT,
  `schoolyearID` int(11) NOT NULL,
  `studentID` int(11) NOT NULL,
  `classesID` int(11) NOT NULL,
  `sectionID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `usertype` varchar(60) NOT NULL,
  `monthyear` varchar(10) NOT NULL,
  `a1` varchar(3) DEFAULT NULL,
  `a2` varchar(3) DEFAULT NULL,
  `a3` varchar(3) DEFAULT NULL,
  `a4` varchar(3) DEFAULT NULL,
  `a5` varchar(3) DEFAULT NULL,
  `a6` varchar(3) DEFAULT NULL,
  `a7` varchar(3) DEFAULT NULL,
  `a8` varchar(3) DEFAULT NULL,
  `a9` varchar(3) DEFAULT NULL,
  `a10` varchar(3) DEFAULT NULL,
  `a11` varchar(3) DEFAULT NULL,
  `a12` varchar(3) DEFAULT NULL,
  `a13` varchar(3) DEFAULT NULL,
  `a14` varchar(3) DEFAULT NULL,
  `a15` varchar(3) DEFAULT NULL,
  `a16` varchar(3) DEFAULT NULL,
  `a17` varchar(3) DEFAULT NULL,
  `a18` varchar(3) DEFAULT NULL,
  `a19` varchar(3) DEFAULT NULL,
  `a20` varchar(3) DEFAULT NULL,
  `a21` varchar(3) DEFAULT NULL,
  `a22` varchar(3) DEFAULT NULL,
  `a23` varchar(3) DEFAULT NULL,
  `a24` varchar(3) DEFAULT NULL,
  `a25` varchar(3) DEFAULT NULL,
  `a26` varchar(3) DEFAULT NULL,
  `a27` varchar(3) DEFAULT NULL,
  `a28` varchar(3) DEFAULT NULL,
  `a29` varchar(3) DEFAULT NULL,
  `a30` varchar(3) DEFAULT NULL,
  `a31` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`attendanceID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attendance`
--

LOCK TABLES `attendance` WRITE;
/*!40000 ALTER TABLE `attendance` DISABLE KEYS */;
/*!40000 ALTER TABLE `attendance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `automation_rec`
--

DROP TABLE IF EXISTS `automation_rec`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `automation_rec` (
  `automation_recID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `studentID` int(11) NOT NULL,
  `date` date NOT NULL,
  `day` varchar(3) NOT NULL,
  `month` varchar(3) NOT NULL,
  `year` year(4) NOT NULL,
  `nofmodule` int(11) NOT NULL,
  PRIMARY KEY (`automation_recID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `automation_rec`
--

LOCK TABLES `automation_rec` WRITE;
/*!40000 ALTER TABLE `automation_rec` DISABLE KEYS */;
/*!40000 ALTER TABLE `automation_rec` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `automation_shudulu`
--

DROP TABLE IF EXISTS `automation_shudulu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `automation_shudulu` (
  `automation_shuduluID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `day` varchar(3) NOT NULL,
  `month` varchar(3) NOT NULL,
  `year` year(4) NOT NULL,
  PRIMARY KEY (`automation_shuduluID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `automation_shudulu`
--

LOCK TABLES `automation_shudulu` WRITE;
/*!40000 ALTER TABLE `automation_shudulu` DISABLE KEYS */;
/*!40000 ALTER TABLE `automation_shudulu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `book`
--

DROP TABLE IF EXISTS `book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `book` (
  `bookID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `book` varchar(60) NOT NULL,
  `subject_code` tinytext NOT NULL,
  `author` varchar(100) NOT NULL,
  `price` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `due_quantity` int(11) NOT NULL,
  `rack` tinytext NOT NULL,
  PRIMARY KEY (`bookID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book`
--

LOCK TABLES `book` WRITE;
/*!40000 ALTER TABLE `book` DISABLE KEYS */;
/*!40000 ALTER TABLE `book` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `category` (
  `categoryID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `hostelID` int(11) NOT NULL,
  `class_type` varchar(60) NOT NULL,
  `hbalance` varchar(20) NOT NULL,
  `note` text,
  PRIMARY KEY (`categoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `certificate_template`
--

DROP TABLE IF EXISTS `certificate_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `certificate_template` (
  `certificate_templateID` int(11) NOT NULL AUTO_INCREMENT,
  `usertypeID` int(11) NOT NULL,
  `name` varchar(60) NOT NULL,
  `theme` int(11) NOT NULL,
  `top_heading_title` text,
  `top_heading_left` text,
  `top_heading_right` text,
  `top_heading_middle` text,
  `main_middle_text` text NOT NULL,
  `template` text NOT NULL,
  `footer_left_text` text,
  `footer_right_text` text,
  `footer_middle_text` text,
  `background_image` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`certificate_templateID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `certificate_template`
--

LOCK TABLES `certificate_template` WRITE;
/*!40000 ALTER TABLE `certificate_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `certificate_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `childcare`
--

DROP TABLE IF EXISTS `childcare`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `childcare` (
  `childcareID` int(11) NOT NULL AUTO_INCREMENT,
  `dropped_at` datetime DEFAULT NULL,
  `received_at` datetime DEFAULT NULL,
  `usertypeID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `parentID` int(11) NOT NULL,
  `signature` text,
  `classesID` int(11) NOT NULL,
  `schoolyearID` int(11) NOT NULL,
  `comment` text,
  `received_status` int(11) NOT NULL DEFAULT '0',
  `receiver_name` varchar(40) NOT NULL,
  `phone` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`childcareID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `childcare`
--

LOCK TABLES `childcare` WRITE;
/*!40000 ALTER TABLE `childcare` DISABLE KEYS */;
/*!40000 ALTER TABLE `childcare` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `classes`
--

DROP TABLE IF EXISTS `classes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `classes` (
  `classesID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `classes` varchar(60) NOT NULL,
  `classes_numeric` int(11) NOT NULL,
  `teacherID` int(11) NOT NULL,
  `studentmaxID` int(11) DEFAULT NULL,
  `note` text,
  `create_date` datetime NOT NULL,
  `modify_date` datetime NOT NULL,
  `create_userID` int(11) NOT NULL,
  `create_username` varchar(60) NOT NULL,
  `create_usertype` varchar(60) NOT NULL,
  PRIMARY KEY (`classesID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `classes`
--

LOCK TABLES `classes` WRITE;
/*!40000 ALTER TABLE `classes` DISABLE KEYS */;
/*!40000 ALTER TABLE `classes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `complain`
--

DROP TABLE IF EXISTS `complain`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `complain` (
  `complainID` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `usertypeID` int(11) NOT NULL,
  `userID` int(11) DEFAULT NULL,
  `schoolyearID` int(11) DEFAULT NULL,
  `description` text,
  `attachment` text,
  `originalfile` text,
  `create_userID` int(11) NOT NULL DEFAULT '0',
  `create_usertypeID` int(11) NOT NULL DEFAULT '0',
  `create_date` datetime DEFAULT NULL,
  `modify_date` datetime DEFAULT NULL,
  PRIMARY KEY (`complainID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `complain`
--

LOCK TABLES `complain` WRITE;
/*!40000 ALTER TABLE `complain` DISABLE KEYS */;
/*!40000 ALTER TABLE `complain` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `conversation_message_info`
--

DROP TABLE IF EXISTS `conversation_message_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `conversation_message_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) DEFAULT '0',
  `draft` int(11) DEFAULT '0',
  `fav_status` int(11) DEFAULT '0',
  `create_date` datetime NOT NULL,
  `modify_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conversation_message_info`
--

LOCK TABLES `conversation_message_info` WRITE;
/*!40000 ALTER TABLE `conversation_message_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `conversation_message_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `conversation_msg`
--

DROP TABLE IF EXISTS `conversation_msg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `conversation_msg` (
  `msg_id` int(11) NOT NULL AUTO_INCREMENT,
  `conversation_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `msg` text NOT NULL,
  `attach` text,
  `attach_file_name` text,
  `usertypeID` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `modify_date` datetime NOT NULL,
  `start` int(11) DEFAULT NULL,
  PRIMARY KEY (`msg_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conversation_msg`
--

LOCK TABLES `conversation_msg` WRITE;
/*!40000 ALTER TABLE `conversation_msg` DISABLE KEYS */;
/*!40000 ALTER TABLE `conversation_msg` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `conversation_user`
--

DROP TABLE IF EXISTS `conversation_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `conversation_user` (
  `conversation_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `usertypeID` int(11) NOT NULL,
  `is_sender` int(11) DEFAULT '0',
  `trash` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conversation_user`
--

LOCK TABLES `conversation_user` WRITE;
/*!40000 ALTER TABLE `conversation_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `conversation_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `document`
--

DROP TABLE IF EXISTS `document`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `document` (
  `documentID` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) CHARACTER SET utf8 NOT NULL,
  `file` varchar(200) CHARACTER SET utf8 NOT NULL,
  `userID` int(11) NOT NULL,
  `usertypeID` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `create_userID` int(11) NOT NULL,
  `create_usertypeID` int(11) NOT NULL,
  PRIMARY KEY (`documentID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `document`
--

LOCK TABLES `document` WRITE;
/*!40000 ALTER TABLE `document` DISABLE KEYS */;
/*!40000 ALTER TABLE `document` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eattendance`
--

DROP TABLE IF EXISTS `eattendance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `eattendance` (
  `eattendanceID` int(200) unsigned NOT NULL AUTO_INCREMENT,
  `schoolyearID` int(11) NOT NULL,
  `examID` int(11) NOT NULL,
  `classesID` int(11) NOT NULL,
  `sectionID` int(11) NOT NULL,
  `subjectID` int(11) NOT NULL,
  `date` date NOT NULL,
  `studentID` int(11) DEFAULT NULL,
  `s_name` varchar(60) DEFAULT NULL,
  `eattendance` varchar(20) DEFAULT NULL,
  `year` year(4) NOT NULL,
  `eextra` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`eattendanceID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eattendance`
--

LOCK TABLES `eattendance` WRITE;
/*!40000 ALTER TABLE `eattendance` DISABLE KEYS */;
/*!40000 ALTER TABLE `eattendance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ebooks`
--

DROP TABLE IF EXISTS `ebooks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `ebooks` (
  `ebooksID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `author` varchar(255) NOT NULL,
  `classesID` int(11) NOT NULL,
  `authority` tinyint(4) NOT NULL DEFAULT '0',
  `cover_photo` varchar(200) NOT NULL,
  `file` varchar(200) NOT NULL,
  PRIMARY KEY (`ebooksID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ebooks`
--

LOCK TABLES `ebooks` WRITE;
/*!40000 ALTER TABLE `ebooks` DISABLE KEYS */;
/*!40000 ALTER TABLE `ebooks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emailsetting`
--

DROP TABLE IF EXISTS `emailsetting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `emailsetting` (
  `fieldoption` varchar(100) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`fieldoption`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emailsetting`
--

LOCK TABLES `emailsetting` WRITE;
/*!40000 ALTER TABLE `emailsetting` DISABLE KEYS */;
INSERT INTO `emailsetting` VALUES ('email_engine','sendmail'),('smtp_password',''),('smtp_port',''),('smtp_security',''),('smtp_server',''),('smtp_username',' ');
/*!40000 ALTER TABLE `emailsetting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event`
--

DROP TABLE IF EXISTS `event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `event` (
  `eventID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `fdate` date NOT NULL,
  `ftime` time NOT NULL,
  `tdate` date NOT NULL,
  `ttime` time NOT NULL,
  `title` varchar(128) NOT NULL,
  `details` text NOT NULL,
  `photo` varchar(200) DEFAULT NULL,
  `create_date` datetime NOT NULL,
  `schoolyearID` int(11) NOT NULL,
  `create_userID` int(11) NOT NULL DEFAULT '0',
  `create_usertypeID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`eventID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event`
--

LOCK TABLES `event` WRITE;
/*!40000 ALTER TABLE `event` DISABLE KEYS */;
/*!40000 ALTER TABLE `event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eventcounter`
--

DROP TABLE IF EXISTS `eventcounter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `eventcounter` (
  `eventcounterID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `eventID` int(11) NOT NULL,
  `username` varchar(40) NOT NULL,
  `type` varchar(20) NOT NULL,
  `name` varchar(128) NOT NULL,
  `photo` varchar(200) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`eventcounterID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eventcounter`
--

LOCK TABLES `eventcounter` WRITE;
/*!40000 ALTER TABLE `eventcounter` DISABLE KEYS */;
/*!40000 ALTER TABLE `eventcounter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exam`
--

DROP TABLE IF EXISTS `exam`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `exam` (
  `examID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `exam` varchar(60) NOT NULL,
  `date` date NOT NULL,
  `note` text,
  PRIMARY KEY (`examID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exam`
--

LOCK TABLES `exam` WRITE;
/*!40000 ALTER TABLE `exam` DISABLE KEYS */;
INSERT INTO `exam` VALUES (1,'First Semester','2019-04-01','Don\'t delete it!');
/*!40000 ALTER TABLE `exam` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `examschedule`
--

DROP TABLE IF EXISTS `examschedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `examschedule` (
  `examscheduleID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `examID` int(11) NOT NULL,
  `classesID` int(11) NOT NULL,
  `sectionID` int(11) NOT NULL,
  `subjectID` int(11) NOT NULL,
  `edate` date NOT NULL,
  `examfrom` varchar(10) NOT NULL,
  `examto` varchar(10) NOT NULL,
  `room` tinytext,
  `schoolyearID` int(11) NOT NULL,
  PRIMARY KEY (`examscheduleID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `examschedule`
--

LOCK TABLES `examschedule` WRITE;
/*!40000 ALTER TABLE `examschedule` DISABLE KEYS */;
/*!40000 ALTER TABLE `examschedule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `expense`
--

DROP TABLE IF EXISTS `expense`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `expense` (
  `expenseID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `create_date` date NOT NULL,
  `date` date NOT NULL,
  `expenseday` varchar(11) NOT NULL,
  `expensemonth` varchar(11) NOT NULL,
  `expenseyear` year(4) NOT NULL,
  `expense` varchar(128) NOT NULL,
  `amount` double NOT NULL,
  `file` varchar(200) DEFAULT NULL,
  `userID` int(11) NOT NULL,
  `usertypeID` int(11) NOT NULL,
  `uname` varchar(60) NOT NULL,
  `schoolyearID` int(11) NOT NULL,
  `note` text,
  PRIMARY KEY (`expenseID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `expense`
--

LOCK TABLES `expense` WRITE;
/*!40000 ALTER TABLE `expense` DISABLE KEYS */;
/*!40000 ALTER TABLE `expense` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feetypes`
--

DROP TABLE IF EXISTS `feetypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `feetypes` (
  `feetypesID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `feetypes` varchar(60) NOT NULL,
  `note` text,
  PRIMARY KEY (`feetypesID`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feetypes`
--

LOCK TABLES `feetypes` WRITE;
/*!40000 ALTER TABLE `feetypes` DISABLE KEYS */;
INSERT INTO `feetypes` VALUES (1,'Books Fine','Don\'t delete it!'),(2,'Library Fee','Don\'t delete it!'),(3,'Transport Fee','Don\'t delete it!'),(4,'Hostel Fee','Don\'t delete it!'),(5,'Tuition Fee [Jan]',''),(6,'Tuition Fee [Feb]',''),(7,'Tuition Fee [Mar]',''),(8,'Tuition Fee [Apr]',''),(9,'Tuition Fee [May]',''),(10,'Tuition Fee [Jun]',''),(11,'Tuition Fee [Jul]',''),(12,'Tuition Fee [Aug]',''),(13,'Tuition Fee [Sep]',''),(14,'Tuition Fee [Oct]',''),(15,'Tuition Fee [Nov]',''),(16,'Tuition Fee [Dec]','');
/*!40000 ALTER TABLE `feetypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fmenu`
--

DROP TABLE IF EXISTS `fmenu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `fmenu` (
  `fmenuID` int(11) NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(128) NOT NULL,
  `status` int(11) NOT NULL COMMENT 'Only for active',
  `topbar` int(11) NOT NULL,
  `social` int(11) NOT NULL,
  PRIMARY KEY (`fmenuID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fmenu`
--

LOCK TABLES `fmenu` WRITE;
/*!40000 ALTER TABLE `fmenu` DISABLE KEYS */;
INSERT INTO `fmenu` VALUES (2,'front',1,1,1);
/*!40000 ALTER TABLE `fmenu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fmenu_relation`
--

DROP TABLE IF EXISTS `fmenu_relation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `fmenu_relation` (
  `fmenu_relationID` int(11) NOT NULL AUTO_INCREMENT,
  `fmenuID` int(11) DEFAULT NULL,
  `menu_typeID` int(11) DEFAULT NULL COMMENT '1 => Pages, 2 => Post, 3 => Links',
  `menu_parentID` varchar(128) DEFAULT NULL,
  `menu_orderID` int(11) DEFAULT NULL,
  `menu_pagesID` int(11) DEFAULT NULL,
  `menu_label` varchar(254) DEFAULT NULL,
  `menu_link` text NOT NULL,
  `menu_rand` varchar(128) DEFAULT NULL,
  `menu_rand_parentID` varchar(128) DEFAULT NULL,
  `menu_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`fmenu_relationID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fmenu_relation`
--

LOCK TABLES `fmenu_relation` WRITE;
/*!40000 ALTER TABLE `fmenu_relation` DISABLE KEYS */;
INSERT INTO `fmenu_relation` VALUES (1,2,1,'0',1,9,'Home','#','acbecd2f34efd98b441d5ab69320a78b','0',1),(2,2,1,'0',2,2,'About','#','3124f89a54b17837ae3fbd1d3651d00f','0',1),(3,2,1,'0',3,1,'Admission','#','168fb0dc41a52c46f9c5960509a89158','0',1),(4,2,1,'0',4,6,'Notice','#','ed0bb9dc5d7e402135b9bfd559441e53','0',1),(5,2,1,'0',5,3,'Event','#','4f0c123be60539e1440207362126df46','0',1),(6,2,1,'0',6,4,'Teachers','#','37e8558725e25a499e73570daefc4b2c','0',1),(7,2,1,'0',7,5,'Gallery','#','c36ce8d2c7c18a9e0932530faa154c41','0',1),(8,2,1,'0',8,7,'Blog','#','29bb3d94d84c3208f2c9d40a2ac5937d','0',1),(9,2,1,'0',9,8,'Contact','#','57fd471e8ab9a51e047daa441e190ce7','0',1);
/*!40000 ALTER TABLE `fmenu_relation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `frontend_setting`
--

DROP TABLE IF EXISTS `frontend_setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `frontend_setting` (
  `fieldoption` varchar(100) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`fieldoption`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `frontend_setting`
--

LOCK TABLES `frontend_setting` WRITE;
/*!40000 ALTER TABLE `frontend_setting` DISABLE KEYS */;
INSERT INTO `frontend_setting` VALUES ('description',''),('facebook',''),('google',''),('linkedin',''),('login_menu_status','1'),('online_admission_status','0'),('teacher_email_status','0'),('teacher_phone_status','0'),('twitter',''),('youtube','');
/*!40000 ALTER TABLE `frontend_setting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `frontend_template`
--

DROP TABLE IF EXISTS `frontend_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `frontend_template` (
  `frontend_templateID` int(11) NOT NULL AUTO_INCREMENT,
  `template_name` varchar(128) NOT NULL,
  PRIMARY KEY (`frontend_templateID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `frontend_template`
--

LOCK TABLES `frontend_template` WRITE;
/*!40000 ALTER TABLE `frontend_template` DISABLE KEYS */;
INSERT INTO `frontend_template` VALUES (1,'home'),(2,'about'),(3,'event'),(4,'teacher'),(5,'gallery'),(6,'notice'),(7,'blog'),(8,'contact'),(9,'admission');
/*!40000 ALTER TABLE `frontend_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `globalpayment`
--

DROP TABLE IF EXISTS `globalpayment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `globalpayment` (
  `globalpaymentID` int(11) NOT NULL AUTO_INCREMENT,
  `classesID` int(11) DEFAULT NULL,
  `sectionID` int(11) NOT NULL,
  `studentID` int(11) NOT NULL,
  `clearancetype` varchar(40) NOT NULL,
  `invoicename` varchar(128) NOT NULL,
  `invoicedescription` varchar(128) NOT NULL,
  `paymentyear` varchar(5) NOT NULL,
  `schoolyearID` int(11) NOT NULL,
  PRIMARY KEY (`globalpaymentID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `globalpayment`
--

LOCK TABLES `globalpayment` WRITE;
/*!40000 ALTER TABLE `globalpayment` DISABLE KEYS */;
/*!40000 ALTER TABLE `globalpayment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `grade`
--

DROP TABLE IF EXISTS `grade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `grade` (
  `gradeID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `grade` varchar(60) NOT NULL,
  `point` varchar(11) NOT NULL,
  `gradefrom` int(11) NOT NULL,
  `gradeupto` int(11) NOT NULL,
  `note` text,
  PRIMARY KEY (`gradeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `grade`
--

LOCK TABLES `grade` WRITE;
/*!40000 ALTER TABLE `grade` DISABLE KEYS */;
/*!40000 ALTER TABLE `grade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hmember`
--

DROP TABLE IF EXISTS `hmember`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `hmember` (
  `hmemberID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `hostelID` int(11) NOT NULL,
  `categoryID` int(11) NOT NULL,
  `studentID` int(11) NOT NULL,
  `hbalance` varchar(20) DEFAULT NULL,
  `hjoindate` date NOT NULL,
  PRIMARY KEY (`hmemberID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hmember`
--

LOCK TABLES `hmember` WRITE;
/*!40000 ALTER TABLE `hmember` DISABLE KEYS */;
/*!40000 ALTER TABLE `hmember` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `holiday`
--

DROP TABLE IF EXISTS `holiday`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `holiday` (
  `holidayID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `schoolyearID` int(11) NOT NULL,
  `fdate` date NOT NULL,
  `tdate` date NOT NULL,
  `title` varchar(128) NOT NULL,
  `details` text NOT NULL,
  `photo` varchar(200) DEFAULT NULL,
  `create_date` datetime NOT NULL,
  `create_userID` int(11) NOT NULL DEFAULT '0',
  `create_usertypeID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`holidayID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `holiday`
--

LOCK TABLES `holiday` WRITE;
/*!40000 ALTER TABLE `holiday` DISABLE KEYS */;
/*!40000 ALTER TABLE `holiday` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hostel`
--

DROP TABLE IF EXISTS `hostel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `hostel` (
  `hostelID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `htype` varchar(11) NOT NULL,
  `address` varchar(200) NOT NULL,
  `note` text,
  PRIMARY KEY (`hostelID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hostel`
--

LOCK TABLES `hostel` WRITE;
/*!40000 ALTER TABLE `hostel` DISABLE KEYS */;
/*!40000 ALTER TABLE `hostel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hourly_template`
--

DROP TABLE IF EXISTS `hourly_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `hourly_template` (
  `hourly_templateID` int(11) NOT NULL AUTO_INCREMENT,
  `hourly_grades` varchar(128) NOT NULL,
  `hourly_rate` int(11) NOT NULL,
  PRIMARY KEY (`hourly_templateID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hourly_template`
--

LOCK TABLES `hourly_template` WRITE;
/*!40000 ALTER TABLE `hourly_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `hourly_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `income`
--

DROP TABLE IF EXISTS `income`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `income` (
  `incomeID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `date` date NOT NULL,
  `incomeday` varchar(11) NOT NULL,
  `incomemonth` varchar(11) NOT NULL,
  `incomeyear` year(4) NOT NULL,
  `amount` double NOT NULL,
  `file` varchar(200) NOT NULL,
  `note` text NOT NULL,
  `schoolyearID` int(11) NOT NULL,
  `create_date` date NOT NULL,
  `userID` int(11) NOT NULL,
  `usertypeID` int(11) NOT NULL,
  PRIMARY KEY (`incomeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `income`
--

LOCK TABLES `income` WRITE;
/*!40000 ALTER TABLE `income` DISABLE KEYS */;
/*!40000 ALTER TABLE `income` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ini_config`
--

DROP TABLE IF EXISTS `ini_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `ini_config` (
  `configID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL,
  `config_key` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`configID`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ini_config`
--

LOCK TABLES `ini_config` WRITE;
/*!40000 ALTER TABLE `ini_config` DISABLE KEYS */;
INSERT INTO `ini_config` VALUES (1,'paypal','paypal_api_username',''),(2,'paypal','paypal_api_password',''),(3,'paypal','paypal_api_signature',''),(4,'paypal','paypal_email',''),(5,'paypal','paypal_demo',''),(6,'stripe','stripe_secret',''),(8,'stripe','stripe_demo',''),(9,'payumoney','payumoney_key',''),(10,'payumoney','payumoney_salt',''),(11,'payumoney','payumoney_demo',''),(12,'paypal','paypal_status',''),(13,'stripe','stripe_status',''),(14,'payumoney','payumoney_status',''),(15,'voguepay','voguepay_merchant_id',''),(16,'voguepay','voguepay_merchant_ref',''),(17,'voguepay','voguepay_developer_code',''),(18,'voguepay','voguepay_demo',''),(19,'voguepay','voguepay_status','');
/*!40000 ALTER TABLE `ini_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `instruction`
--

DROP TABLE IF EXISTS `instruction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `instruction` (
  `instructionID` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(512) NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`instructionID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `instruction`
--

LOCK TABLES `instruction` WRITE;
/*!40000 ALTER TABLE `instruction` DISABLE KEYS */;
/*!40000 ALTER TABLE `instruction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoice`
--

DROP TABLE IF EXISTS `invoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `invoice` (
  `invoiceID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `schoolyearID` int(11) NOT NULL,
  `classesID` int(11) NOT NULL,
  `studentID` int(11) NOT NULL,
  `feetypeID` int(11) DEFAULT NULL,
  `feetype` varchar(128) NOT NULL,
  `amount` double NOT NULL,
  `discount` double NOT NULL DEFAULT '0',
  `userID` int(11) DEFAULT NULL,
  `usertypeID` int(11) DEFAULT NULL,
  `uname` varchar(60) DEFAULT NULL,
  `date` date NOT NULL,
  `create_date` date NOT NULL,
  `day` varchar(20) DEFAULT NULL,
  `month` varchar(20) DEFAULT NULL,
  `year` year(4) NOT NULL,
  `paidstatus` int(11) DEFAULT NULL,
  `deleted_at` int(11) NOT NULL DEFAULT '1',
  `maininvoiceID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`invoiceID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoice`
--

LOCK TABLES `invoice` WRITE;
/*!40000 ALTER TABLE `invoice` DISABLE KEYS */;
/*!40000 ALTER TABLE `invoice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `issue`
--

DROP TABLE IF EXISTS `issue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `issue` (
  `issueID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `lID` varchar(128) NOT NULL,
  `bookID` int(11) NOT NULL,
  `serial_no` varchar(40) NOT NULL,
  `issue_date` date NOT NULL,
  `due_date` date NOT NULL,
  `return_date` date DEFAULT NULL,
  `note` text,
  PRIMARY KEY (`issueID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `issue`
--

LOCK TABLES `issue` WRITE;
/*!40000 ALTER TABLE `issue` DISABLE KEYS */;
/*!40000 ALTER TABLE `issue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `leaveapplications`
--

DROP TABLE IF EXISTS `leaveapplications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `leaveapplications` (
  `leaveapplicationID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `leavecategoryID` int(10) unsigned NOT NULL,
  `apply_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `od_status` tinyint(1) NOT NULL DEFAULT '0',
  `from_date` date NOT NULL,
  `from_time` time DEFAULT NULL,
  `to_date` date NOT NULL,
  `to_time` time DEFAULT NULL,
  `leave_days` int(11) NOT NULL,
  `reason` text,
  `attachment` varchar(200) DEFAULT NULL,
  `attachmentorginalname` varchar(200) DEFAULT NULL,
  `create_date` datetime NOT NULL,
  `modify_date` datetime NOT NULL,
  `create_userID` int(11) NOT NULL,
  `create_usertypeID` int(11) unsigned NOT NULL,
  `applicationto_userID` int(11) unsigned DEFAULT NULL,
  `applicationto_usertypeID` int(11) unsigned DEFAULT NULL,
  `approver_userID` int(11) unsigned DEFAULT NULL,
  `approver_usertypeID` int(11) unsigned DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `schoolyearID` int(11) NOT NULL,
  PRIMARY KEY (`leaveapplicationID`),
  KEY `leave_categoryID` (`leavecategoryID`),
  KEY `from_date` (`from_date`),
  KEY `to_date` (`to_date`),
  KEY `approver_userID` (`approver_userID`),
  KEY `approver_usertypeID` (`approver_usertypeID`),
  KEY `applicationto_usertypeID` (`applicationto_usertypeID`),
  KEY `applicationto_userID` (`applicationto_userID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `leaveapplications`
--

LOCK TABLES `leaveapplications` WRITE;
/*!40000 ALTER TABLE `leaveapplications` DISABLE KEYS */;
/*!40000 ALTER TABLE `leaveapplications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `leaveassign`
--

DROP TABLE IF EXISTS `leaveassign`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `leaveassign` (
  `leaveassignID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `leavecategoryID` int(10) unsigned NOT NULL,
  `usertypeID` int(11) unsigned NOT NULL,
  `leaveassignday` int(11) NOT NULL,
  `schoolyearID` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `modify_date` datetime NOT NULL,
  `create_userID` int(11) NOT NULL,
  `create_usertypeID` int(11) NOT NULL,
  PRIMARY KEY (`leaveassignID`),
  KEY `leave_categoryID` (`leavecategoryID`),
  KEY `usertypeID` (`usertypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `leaveassign`
--

LOCK TABLES `leaveassign` WRITE;
/*!40000 ALTER TABLE `leaveassign` DISABLE KEYS */;
/*!40000 ALTER TABLE `leaveassign` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `leavecategory`
--

DROP TABLE IF EXISTS `leavecategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `leavecategory` (
  `leavecategoryID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `leavecategory` varchar(255) NOT NULL,
  `leavegender` int(11) DEFAULT '0' COMMENT '1 = General, 2 = Male, 3 = Femele',
  `create_date` datetime NOT NULL,
  `modify_date` datetime NOT NULL,
  `create_userID` int(11) NOT NULL,
  `create_usertypeID` int(11) unsigned NOT NULL,
  PRIMARY KEY (`leavecategoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `leavecategory`
--

LOCK TABLES `leavecategory` WRITE;
/*!40000 ALTER TABLE `leavecategory` DISABLE KEYS */;
/*!40000 ALTER TABLE `leavecategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lmember`
--

DROP TABLE IF EXISTS `lmember`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `lmember` (
  `lmemberID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `lID` varchar(40) NOT NULL,
  `studentID` int(11) NOT NULL,
  `name` varchar(60) NOT NULL,
  `email` varchar(40) DEFAULT NULL,
  `phone` tinytext,
  `lbalance` varchar(20) DEFAULT NULL,
  `ljoindate` date NOT NULL,
  PRIMARY KEY (`lmemberID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lmember`
--

LOCK TABLES `lmember` WRITE;
/*!40000 ALTER TABLE `lmember` DISABLE KEYS */;
/*!40000 ALTER TABLE `lmember` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `location`
--

DROP TABLE IF EXISTS `location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `location` (
  `locationID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `location` varchar(128) NOT NULL,
  `description` text,
  `create_date` date NOT NULL,
  `modify_date` date NOT NULL,
  `create_userID` int(11) NOT NULL,
  `create_usertypeID` int(11) NOT NULL,
  `active` int(11) NOT NULL,
  PRIMARY KEY (`locationID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `location`
--

LOCK TABLES `location` WRITE;
/*!40000 ALTER TABLE `location` DISABLE KEYS */;
/*!40000 ALTER TABLE `location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loginlog`
--

DROP TABLE IF EXISTS `loginlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `loginlog` (
  `loginlogID` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(45) DEFAULT NULL,
  `browser` varchar(128) DEFAULT NULL,
  `operatingsystem` varchar(128) DEFAULT NULL,
  `login` int(11) unsigned DEFAULT NULL,
  `logout` int(11) unsigned DEFAULT NULL,
  `usertypeID` int(11) DEFAULT NULL,
  `userID` int(11) DEFAULT NULL,
  PRIMARY KEY (`loginlogID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loginlog`
--

LOCK TABLES `loginlog` WRITE;
/*!40000 ALTER TABLE `loginlog` DISABLE KEYS */;
INSERT INTO `loginlog` VALUES (1,'192.168.1.15','Google Chrome','windows',1575844431,1575844507,1,1),(2,'103.53.43.45','Google Chrome','windows',1575846392,1575847213,1,1),(3,'103.53.43.45','Google Chrome','windows',1575847230,1575847530,1,1),(4,'103.53.43.45','Google Chrome','windows',1580144296,1580148792,1,1),(5,'103.53.43.45','Google Chrome','windows',1580148816,1580148824,1,1),(6,'103.53.43.45','Google Chrome','windows',1580148873,1580148948,1,1),(7,'103.53.43.45','Google Chrome','windows',1580149658,1580149706,1,1),(8,'103.53.43.45','Google Chrome','windows',1580149729,1580149775,1,1),(9,'103.53.43.45','Google Chrome','windows',1580581950,1580581957,1,1),(10,'103.53.43.45','Google Chrome','windows',1580582036,1580582115,1,1);
/*!40000 ALTER TABLE `loginlog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mailandsms`
--

DROP TABLE IF EXISTS `mailandsms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `mailandsms` (
  `mailandsmsID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `usertypeID` int(11) NOT NULL,
  `users` text NOT NULL,
  `type` varchar(16) NOT NULL,
  `senderusertypeID` int(11) NOT NULL,
  `senderID` int(11) NOT NULL,
  `message` text NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `year` year(4) NOT NULL,
  PRIMARY KEY (`mailandsmsID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mailandsms`
--

LOCK TABLES `mailandsms` WRITE;
/*!40000 ALTER TABLE `mailandsms` DISABLE KEYS */;
/*!40000 ALTER TABLE `mailandsms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mailandsmstemplate`
--

DROP TABLE IF EXISTS `mailandsmstemplate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `mailandsmstemplate` (
  `mailandsmstemplateID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `usertypeID` int(11) NOT NULL,
  `type` varchar(10) NOT NULL,
  `template` text NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`mailandsmstemplateID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mailandsmstemplate`
--

LOCK TABLES `mailandsmstemplate` WRITE;
/*!40000 ALTER TABLE `mailandsmstemplate` DISABLE KEYS */;
/*!40000 ALTER TABLE `mailandsmstemplate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mailandsmstemplatetag`
--

DROP TABLE IF EXISTS `mailandsmstemplatetag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `mailandsmstemplatetag` (
  `mailandsmstemplatetagID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `usertypeID` int(11) NOT NULL,
  `tagname` varchar(128) NOT NULL,
  `mailandsmstemplatetag_extra` varchar(255) DEFAULT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`mailandsmstemplatetagID`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mailandsmstemplatetag`
--

LOCK TABLES `mailandsmstemplatetag` WRITE;
/*!40000 ALTER TABLE `mailandsmstemplatetag` DISABLE KEYS */;
INSERT INTO `mailandsmstemplatetag` VALUES (1,1,'[name]',NULL,'2016-12-10 14:36:33'),(2,1,'[dob]',NULL,'2016-12-10 14:37:31'),(3,1,'[gender]',NULL,'2016-12-10 14:37:31'),(4,1,'[religion]',NULL,'2016-12-10 14:39:51'),(5,1,'[email]',NULL,'2016-12-10 14:39:51'),(6,1,'[phone]',NULL,'2016-12-10 14:39:51'),(7,1,'[address]',NULL,'2016-12-10 14:39:51'),(8,1,'[jod]',NULL,'2016-12-10 14:39:51'),(9,1,'[username]',NULL,'2016-12-10 14:39:51'),(10,2,'[name]',NULL,'2016-12-10 14:40:50'),(11,2,'[designation]',NULL,'2016-12-10 14:43:27'),(12,2,'[dob]',NULL,'2016-12-10 14:46:21'),(13,2,'[gender]',NULL,'2016-12-10 14:46:21'),(14,2,'[religion]',NULL,'2016-12-10 14:46:21'),(15,2,'[email]',NULL,'2016-12-10 14:46:21'),(16,2,'[phone]',NULL,'2016-12-10 14:46:21'),(17,2,'[address]',NULL,'2016-12-10 14:46:21'),(18,2,'[jod]',NULL,'2016-12-10 14:46:21'),(19,2,'[username]',NULL,'2016-12-10 14:46:21'),(20,3,'[name]',NULL,'2016-12-10 14:47:09'),(21,3,'[dob]',NULL,'2016-12-10 14:55:54'),(22,3,'[gender]',NULL,'2016-12-10 14:55:54'),(23,3,'[blood_group]',NULL,'2016-12-10 14:55:54'),(24,3,'[religion]',NULL,'2016-12-10 14:55:54'),(25,3,'[email]',NULL,'2016-12-10 14:55:54'),(26,3,'[phone]',NULL,'2016-12-10 14:55:54'),(27,3,'[address]',NULL,'2016-12-10 14:55:54'),(28,3,'[state]',NULL,'2017-02-11 12:21:49'),(29,3,'[country]',NULL,'2017-02-11 12:21:27'),(30,3,'[class]',NULL,'2016-12-18 15:34:20'),(31,3,'[section]',NULL,'2016-12-10 14:55:54'),(32,3,'[group]',NULL,'2016-12-10 14:55:54'),(33,3,'[optional_subject]',NULL,'2016-12-10 14:55:54'),(34,3,'[register_no]',NULL,'2017-02-11 12:21:27'),(35,3,'[roll]',NULL,'2017-02-11 12:22:56'),(36,3,'[extra_curricular_activities]',NULL,'2017-02-11 12:22:56'),(37,3,'[remarks]',NULL,'2017-02-11 12:22:56'),(38,3,'[username]',NULL,'2016-12-10 14:55:54'),(39,3,'[result_table]',NULL,'2016-12-10 14:55:54'),(40,4,'[name]',NULL,'2016-12-10 14:57:31'),(41,4,'[father\'s_name]',NULL,'2016-12-10 15:04:19'),(42,4,'[mother\'s_name]',NULL,'2016-12-10 15:04:19'),(43,4,'[father\'s_profession]',NULL,'2016-12-10 15:04:19'),(44,4,'[mother\'s_profession]',NULL,'2016-12-10 15:04:19'),(45,4,'[email]',NULL,'2016-12-10 15:04:19'),(46,4,'[phone]',NULL,'2016-12-10 15:04:19'),(47,4,'[address]',NULL,'2016-12-10 15:04:19'),(48,4,'[username]',NULL,'2016-12-10 15:04:19'),(49,1,'[date]',NULL,'2018-05-11 04:12:12'),(50,2,'[date]',NULL,'2018-05-11 04:12:27'),(51,3,'[date]',NULL,'2018-05-11 04:12:36'),(52,4,'[date]',NULL,'2018-05-11 04:12:49');
/*!40000 ALTER TABLE `mailandsmstemplatetag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `maininvoice`
--

DROP TABLE IF EXISTS `maininvoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `maininvoice` (
  `maininvoiceID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `maininvoiceschoolyearID` int(11) NOT NULL,
  `maininvoiceclassesID` int(11) NOT NULL,
  `maininvoicestudentID` int(11) NOT NULL,
  `maininvoiceuserID` int(11) DEFAULT NULL,
  `maininvoiceusertypeID` int(11) DEFAULT NULL,
  `maininvoiceuname` varchar(60) DEFAULT NULL,
  `maininvoicedate` date NOT NULL,
  `maininvoicecreate_date` date NOT NULL,
  `maininvoiceday` varchar(20) DEFAULT NULL,
  `maininvoicemonth` varchar(20) DEFAULT NULL,
  `maininvoiceyear` year(4) NOT NULL,
  `maininvoicestatus` int(11) DEFAULT NULL,
  `maininvoicedeleted_at` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`maininvoiceID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `maininvoice`
--

LOCK TABLES `maininvoice` WRITE;
/*!40000 ALTER TABLE `maininvoice` DISABLE KEYS */;
/*!40000 ALTER TABLE `maininvoice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `make_payment`
--

DROP TABLE IF EXISTS `make_payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `make_payment` (
  `make_paymentID` int(11) NOT NULL AUTO_INCREMENT,
  `month` text NOT NULL,
  `gross_salary` text NOT NULL,
  `total_deduction` text NOT NULL,
  `net_salary` text NOT NULL,
  `payment_amount` text NOT NULL,
  `payment_method` int(11) NOT NULL,
  `comments` text,
  `templateID` int(11) NOT NULL,
  `salaryID` int(11) NOT NULL,
  `usertypeID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `schoolyearID` int(11) DEFAULT NULL,
  `create_date` datetime NOT NULL,
  `modify_date` datetime NOT NULL,
  `create_userID` int(11) NOT NULL,
  `create_username` varchar(60) NOT NULL,
  `create_usertype` varchar(60) NOT NULL,
  `total_hours` text,
  PRIMARY KEY (`make_paymentID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `make_payment`
--

LOCK TABLES `make_payment` WRITE;
/*!40000 ALTER TABLE `make_payment` DISABLE KEYS */;
/*!40000 ALTER TABLE `make_payment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `manage_salary`
--

DROP TABLE IF EXISTS `manage_salary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `manage_salary` (
  `manage_salaryID` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `usertypeID` int(11) NOT NULL,
  `salary` int(11) NOT NULL,
  `template` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `modify_date` datetime NOT NULL,
  `create_userID` int(11) NOT NULL,
  `create_username` varchar(60) NOT NULL,
  `create_usertype` varchar(60) NOT NULL,
  PRIMARY KEY (`manage_salaryID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `manage_salary`
--

LOCK TABLES `manage_salary` WRITE;
/*!40000 ALTER TABLE `manage_salary` DISABLE KEYS */;
/*!40000 ALTER TABLE `manage_salary` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mark`
--

DROP TABLE IF EXISTS `mark`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `mark` (
  `markID` int(200) unsigned NOT NULL AUTO_INCREMENT,
  `schoolyearID` int(11) NOT NULL,
  `examID` int(11) NOT NULL,
  `exam` varchar(60) NOT NULL,
  `studentID` int(11) NOT NULL,
  `classesID` int(11) NOT NULL,
  `subjectID` int(11) NOT NULL,
  `subject` varchar(60) NOT NULL,
  `year` year(4) NOT NULL,
  `create_date` datetime NOT NULL,
  `create_userID` int(11) NOT NULL DEFAULT '0',
  `create_usertypeID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`markID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mark`
--

LOCK TABLES `mark` WRITE;
/*!40000 ALTER TABLE `mark` DISABLE KEYS */;
/*!40000 ALTER TABLE `mark` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `markpercentage`
--

DROP TABLE IF EXISTS `markpercentage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `markpercentage` (
  `markpercentageID` int(11) NOT NULL AUTO_INCREMENT,
  `markpercentagetype` varchar(100) NOT NULL,
  `percentage` double NOT NULL,
  `examID` int(11) DEFAULT NULL,
  `classesID` int(11) DEFAULT NULL,
  `subjectID` int(11) DEFAULT NULL,
  `create_date` datetime NOT NULL,
  `modify_date` datetime NOT NULL,
  `create_userID` int(11) NOT NULL,
  `create_username` varchar(60) NOT NULL,
  `create_usertype` varchar(60) NOT NULL,
  PRIMARY KEY (`markpercentageID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `markpercentage`
--

LOCK TABLES `markpercentage` WRITE;
/*!40000 ALTER TABLE `markpercentage` DISABLE KEYS */;
INSERT INTO `markpercentage` VALUES (1,'Exam',70,NULL,NULL,NULL,'2017-01-05 06:11:54','2019-01-23 08:07:37',1,'admin','Admin'),(2,'Attendance',10,NULL,NULL,NULL,'2019-01-23 08:07:10','2019-01-23 08:07:10',1,'admin','Admin'),(3,'Class Test',10,NULL,NULL,NULL,'2019-01-23 08:07:20','2019-01-23 08:07:20',1,'admin','Admin'),(4,'Assignment',10,NULL,NULL,NULL,'2019-01-23 08:07:32','2019-01-23 08:07:32',1,'admin','Admin'),(5,'Practical',10,NULL,NULL,NULL,'2019-01-23 08:08:16','2019-01-23 08:08:16',1,'admin','Admin'),(6,'Quiz Test',10,NULL,NULL,NULL,'2019-01-23 08:08:40','2019-01-23 08:08:40',1,'admin','Admin'),(7,'Lab Report',10,NULL,NULL,NULL,'2019-01-23 08:08:49','2019-01-23 08:08:49',1,'admin','Admin');
/*!40000 ALTER TABLE `markpercentage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `markrelation`
--

DROP TABLE IF EXISTS `markrelation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `markrelation` (
  `markrelationID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `markID` int(11) NOT NULL,
  `markpercentageID` int(11) NOT NULL,
  `mark` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`markrelationID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `markrelation`
--

LOCK TABLES `markrelation` WRITE;
/*!40000 ALTER TABLE `markrelation` DISABLE KEYS */;
/*!40000 ALTER TABLE `markrelation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `marksetting`
--

DROP TABLE IF EXISTS `marksetting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `marksetting` (
  `marksettingID` int(11) NOT NULL AUTO_INCREMENT,
  `examID` int(11) NOT NULL DEFAULT '0',
  `classesID` int(11) NOT NULL DEFAULT '0',
  `subjectID` int(11) DEFAULT '0',
  `marktypeID` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`marksettingID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marksetting`
--

LOCK TABLES `marksetting` WRITE;
/*!40000 ALTER TABLE `marksetting` DISABLE KEYS */;
INSERT INTO `marksetting` VALUES (1,1,0,0,0);
/*!40000 ALTER TABLE `marksetting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `marksettingrelation`
--

DROP TABLE IF EXISTS `marksettingrelation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `marksettingrelation` (
  `marksettingrelationID` int(11) NOT NULL AUTO_INCREMENT,
  `marktypeID` int(11) NOT NULL DEFAULT '0',
  `marksettingID` int(11) NOT NULL DEFAULT '0',
  `markpercentageID` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`marksettingrelationID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marksettingrelation`
--

LOCK TABLES `marksettingrelation` WRITE;
/*!40000 ALTER TABLE `marksettingrelation` DISABLE KEYS */;
INSERT INTO `marksettingrelation` VALUES (1,0,1,1),(2,0,1,2),(3,0,1,3),(4,0,1,4);
/*!40000 ALTER TABLE `marksettingrelation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media`
--

DROP TABLE IF EXISTS `media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `media` (
  `mediaID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `usertypeID` int(11) NOT NULL,
  `mcategoryID` int(11) NOT NULL DEFAULT '0',
  `file_name` varchar(255) NOT NULL,
  `file_name_display` varchar(255) NOT NULL,
  PRIMARY KEY (`mediaID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media`
--

LOCK TABLES `media` WRITE;
/*!40000 ALTER TABLE `media` DISABLE KEYS */;
/*!40000 ALTER TABLE `media` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media_category`
--

DROP TABLE IF EXISTS `media_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `media_category` (
  `mcategoryID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `usertypeID` int(11) NOT NULL,
  `folder_name` varchar(255) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`mcategoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media_category`
--

LOCK TABLES `media_category` WRITE;
/*!40000 ALTER TABLE `media_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `media_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media_gallery`
--

DROP TABLE IF EXISTS `media_gallery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `media_gallery` (
  `media_galleryID` int(11) NOT NULL AUTO_INCREMENT,
  `media_gallery_type` int(11) NOT NULL,
  `file_type` varchar(40) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `file_original_name` varchar(255) DEFAULT NULL,
  `file_title` text NOT NULL,
  `file_size` varchar(40) DEFAULT NULL,
  `file_width_height` varchar(40) DEFAULT NULL,
  `file_upload_date` datetime DEFAULT NULL,
  `file_caption` text,
  `file_alt_text` varchar(255) DEFAULT NULL,
  `file_description` text,
  `file_length` varchar(128) DEFAULT NULL,
  `file_artist` varchar(128) DEFAULT NULL,
  `file_album` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`media_galleryID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media_gallery`
--

LOCK TABLES `media_gallery` WRITE;
/*!40000 ALTER TABLE `media_gallery` DISABLE KEYS */;
/*!40000 ALTER TABLE `media_gallery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media_share`
--

DROP TABLE IF EXISTS `media_share`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `media_share` (
  `shareID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `classesID` int(11) NOT NULL DEFAULT '0',
  `public` int(11) NOT NULL,
  `file_or_folder` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`shareID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media_share`
--

LOCK TABLES `media_share` WRITE;
/*!40000 ALTER TABLE `media_share` DISABLE KEYS */;
/*!40000 ALTER TABLE `media_share` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `menu` (
  `menuID` int(11) NOT NULL AUTO_INCREMENT,
  `menuName` varchar(128) NOT NULL,
  `link` varchar(512) NOT NULL,
  `icon` varchar(128) DEFAULT NULL,
  `pullRight` text,
  `status` int(11) NOT NULL DEFAULT '1',
  `parentID` int(11) NOT NULL DEFAULT '0',
  `priority` int(11) NOT NULL DEFAULT '1000',
  PRIMARY KEY (`menuID`)
) ENGINE=InnoDB AUTO_INCREMENT=152 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu`
--

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` VALUES (1,'dashboard','dashboard','fa-laptop','',1,0,10000),(2,'student','student','icon-student',NULL,1,0,1000),(3,'parents','parents','fa-user',NULL,1,0,1000),(4,'teacher','teacher','icon-teacher',NULL,1,0,1000),(5,'user','user','fa-users',NULL,1,0,1000),(6,'main_academic','#','icon-academicmain','',1,0,1000),(7,'main_attendance','#','icon-attendance',NULL,1,0,1000),(8,'main_exam','#','icon-exam',NULL,1,0,1000),(9,'main_mark','#','icon-markmain',NULL,1,0,1000),(10,'conversation','conversation','fa-envelope',NULL,1,0,1000),(11,'media','media','fa-film',NULL,1,0,1000),(12,'mailandsms','mailandsms','icon-mailandsms',NULL,1,0,1000),(13,'main_library','#','icon-library','',1,0,390),(14,'main_transport','#','icon-bus','',1,0,350),(15,'main_hostel','#','icon-hhostel','',1,0,320),(16,'main_account','#','icon-account','',1,0,280),(17,'main_announcement','#','icon-noticemain','',1,0,230),(18,'main_report','#','fa-clipboard','',1,0,190),(19,'visitorinfo','visitorinfo','icon-visitorinfo','',1,0,150),(20,'main_administrator','#','icon-administrator','',1,0,140),(21,'main_settings','#','fa-gavel','',1,0,30),(22,'classes','classes','fa-sitemap',NULL,1,6,5000),(23,'section','section','fa-star','',1,6,4500),(24,'subject','subject','icon-subject','',1,6,4000),(25,'routine','routine','icon-routine',NULL,1,6,1000),(26,'syllabus','syllabus','icon-syllabus',NULL,1,6,3500),(27,'assignment','assignment','icon-assignment',NULL,1,6,3000),(28,'sattendance','sattendance','icon-sattendance',NULL,1,7,1000),(29,'tattendance','tattendance','icon-tattendance',NULL,1,7,1000),(30,'exam','exam','fa-pencil',NULL,1,8,1000),(31,'examschedule','examschedule','fa-puzzle-piece',NULL,1,8,1000),(32,'grade','grade','fa-signal',NULL,1,8,1000),(33,'eattendance','eattendance','icon-eattendance',NULL,1,8,1000),(34,'mark','mark','fa-flask',NULL,1,9,1000),(35,'markpercentage','markpercentage','icon-markpercentage',NULL,1,9,1000),(36,'promotion','promotion','icon-promotion',NULL,1,9,1000),(37,'notice','notice','fa-calendar','',1,17,220),(38,'event','event','fa-calendar-check-o','',1,17,210),(39,'holiday','holiday','icon-holiday','',1,17,200),(40,'classreport','classesreport','icon-classreport','',1,18,1000),(41,'attendancereport','attendancereport','icon-attendancereport','',1,18,940),(42,'studentreport','studentreport','icon-studentreport','',1,18,990),(43,'schoolyear','schoolyear','fa fa-calendar-plus-o','',1,20,130),(44,'mailandsmstemplate','mailandsmstemplate','icon-template','',1,20,100),(46,'backup','backup','fa-download','',1,20,80),(47,'systemadmin','systemadmin','icon-systemadmin','',1,20,120),(48,'resetpassword','resetpassword','icon-reset_password','',1,20,110),(49,'permission','permission','icon-permission','',1,20,60),(50,'usertype','usertype','icon-role','',1,20,70),(51,'setting','setting','fa-gears','',1,21,30),(52,'paymentsettings','paymentsettings','icon-paymentsettings','',1,21,20),(53,'smssettings','smssettings','fa-wrench','',1,21,10),(54,'invoice','invoice','icon-invoice','',1,16,260),(55,'paymenthistory','paymenthistory','icon-payment','',1,16,250),(56,'transport','transport','icon-sbus','',1,14,340),(57,'member','tmember','icon-member','',1,14,330),(58,'hostel','hostel','icon-hostel','',1,15,310),(59,'category','category','fa-leaf','',1,15,300),(61,'member','hmember','icon-member','',1,15,290),(62,'feetypes','feetypes','icon-feetypes','',1,16,270),(63,'expense','expense','icon-expense','',1,16,240),(64,'member','lmember','icon-member','',1,13,380),(65,'books','book','icon-lbooks','',1,13,370),(66,'issue','issue','icon-issue','',1,13,360),(69,'import','bulkimport','fa-upload','',1,20,90),(70,'update','update','fa-refresh','',1,20,50),(71,'main_child','#','fa-child','',1,0,430),(72,'activitiescategory','activitiescategory','fa-pagelines','',1,71,420),(73,'activities','activities','fa-fighter-jet','',1,71,410),(74,'childcare','childcare','fa-wheelchair','',1,71,400),(75,'uattendance','uattendance','fa-user-secret',NULL,1,7,1000),(76,'studentgroup','studentgroup','fa-object-group','',1,20,129),(77,'vendor','vendor','fa-rss','',1,96,1000),(78,'location','location','fa-newspaper-o','',1,96,1000),(79,'asset_category','asset_category','fa-life-ring','',1,96,1000),(80,'asset','asset','fa-fax','',1,96,1000),(81,'complain','complain','fa-commenting','',1,20,128),(82,'question_group','question_group','fa-question-circle','',1,88,1000),(83,'question_level','question_level','fa-level-up','',1,88,1000),(84,'question_bank','question_bank','fa-qrcode','',1,88,1000),(85,'online_exam','online_exam','fa-slideshare','',1,88,1000),(86,'instruction','instruction','fa-map-signs','',1,88,1000),(87,'take_exam','take_exam','fa-user-secret','',1,88,1000),(88,'online_exam','#','fa-graduation-cap','',1,0,1000),(89,'certificatereport','certificatereport','fa-diamond','',1,18,860),(90,'certificate_template','certificate_template','fa-certificate','',1,20,128),(91,'main_payroll','#','fa-usd',NULL,1,0,1000),(92,'salary_template','salary_template','fa-calculator','',1,91,1000),(93,'hourly_template','hourly_template','fa fa-clock-o','',1,91,1000),(94,'manage_salary','manage_salary','fa-beer','',1,91,1000),(95,'make_payment','make_payment','fa-money',NULL,1,91,1000),(96,'main_asset_management','#','fa-archive',NULL,1,0,1000),(97,'asset_assignment','asset_assignment','fa-plug',NULL,1,96,1000),(98,'purchase','purchase','fa-cart-plus',NULL,1,96,1000),(99,'main_frontend','#','fa-home','',1,0,40),(100,'pages','pages','fa-connectdevelop','',1,99,1000),(101,'frontend_setting','frontend_setting','fa-asterisk','',1,21,25),(102,'routinereport','routinereport','iniicon-routinereport','',1,18,960),(103,'examschedulereport','examschedulereport','iniicon-examschedulereport','',1,18,950),(104,'feesreport','feesreport','iniicon-feesreport','',1,18,850),(105,'duefeesreport','duefeesreport','iniicon-duefeesreport','',1,18,840),(106,'balancefeesreport','balancefeesreport','iniicon-balancefeesreport','',1,18,830),(107,'transactionreport','transactionreport','iniicon-transactionreport','',1,18,820),(108,'sociallink','sociallink','iniicon-sociallink','',1,20,109),(109,'idcardreport','idcardreport','iniicon-idcardreport','',1,18,980),(110,'admitcardreport','admitcardreport','iniicon-admitcardreport','',1,18,970),(111,'studentfinereport','studentfinereport','iniicon-studentfinereport','',1,18,810),(112,'attendanceoverviewreport','attendanceoverviewreport','iniicon-attendanceoverviewreport','',1,18,930),(113,'income','income','iniicon-income','',1,16,239),(114,'global_payment','global_payment','fa-balance-scale','',1,16,238),(115,'terminalreport','terminalreport','iniicon-terminalreport','',1,18,920),(116,'tabulationsheetreport','tabulationsheetreport','iniicon-tabulationsheetreport','',1,18,900),(117,'marksheetreport','marksheetreport','iniicon-marksheetreport','',1,18,890),(118,'meritstagereport','meritstagereport','iniicon-meritstagereport','',1,18,910),(119,'progresscardreport','progresscardreport','iniicon-progresscardreport','',1,18,880),(120,'onlineexamreport','onlineexamreport','iniicon-onlineexamreport','',1,18,870),(121,'main_inventory','#','iniicon-maininventory','',1,0,1000),(122,'productcategory','productcategory','iniicon-productcategory','',1,121,1000),(123,'product','product','iniicon-product','',1,121,1000),(124,'productwarehouse','productwarehouse','iniicon-productwarehouse','',1,121,1000),(125,'productsupplier','productsupplier','iniicon-productsupplier','',1,121,1000),(126,'productpurchase','productpurchase','iniicon-productpurchase','',1,121,1000),(127,'productsale','productsale','iniicon-productsale','',1,121,1000),(128,'main_leaveapplication','#','iniicon-mainleaveapplication','',1,0,1000),(129,'leavecategory','leavecategory','iniicon-leavecategory','',1,128,1000),(130,'leaveassign','leaveassign','iniicon-leaveassign','',1,128,1000),(131,'leaveapply','leaveapply','iniicon-leaveapply','',1,128,1000),(132,'leaveapplication','leaveapplication','iniicon-leaveapplication','',1,128,1000),(133,'librarybooksreport','librarybooksreport','iniicon-librarybooksreport','',1,18,925),(134,'searchpaymentfeesreport','searchpaymentfeesreport','iniicon-searchpaymentfeesreport','',1,18,852),(135,'salaryreport','salaryreport','iniicon-salaryreport','',1,18,805),(136,'productpurchasereport','productpurchasereport','iniicon-productpurchasereport','',1,18,854),(137,'productsalereport','productsalereport','iniicon-productsalereport','',1,18,853),(138,'leaveapplicationreport','leaveapplicationreport','iniicon-leaveapplicationreport','',1,18,855),(139,'posts','posts','fa-thumb-tack','',1,99,1005),(140,'posts_categories','posts_categories','fa-anchor',NULL,1,99,1010),(141,'menu','frontendmenu','iniicon-fmenu','',1,99,1000),(142,'librarycardreport','librarycardreport','iniicon-librarycardreport','',1,18,924),(143,'librarybookissuereport','librarybookissuereport','iniicon-librarybookissuereport','',1,18,923),(144,'onlineexamquestionreport','onlineexamquestionreport','iniicon-onlineexamquestionreport','',1,18,865),(145,'ebooks','ebooks','iniicon-ebook','',1,13,350),(146,'accountledgerreport','accountledgerreport','iniicon-accountledgerreport','',1,18,800),(147,'onlineadmission','onlineadmission','iniicon-onlineadmission','',1,0,160),(148,'emailsetting','emailsetting','iniicon-ini-emailsetting','',1,21,5),(149,'onlineadmissionreport','onlineadmissionreport','iniicon-onlineadmissionreport','',1,18,863),(150,'marksetting','marksetting','fa-futbol-o','',1,21,4),(151,'studentsessionreport','studentsessionreport','fa-recycle','',1,18,876);
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `migrations` (
  `version` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notice`
--

DROP TABLE IF EXISTS `notice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `notice` (
  `noticeID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `notice` text NOT NULL,
  `schoolyearID` int(11) NOT NULL,
  `date` date NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `create_userID` int(11) NOT NULL DEFAULT '0',
  `create_usertypeID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`noticeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notice`
--

LOCK TABLES `notice` WRITE;
/*!40000 ALTER TABLE `notice` DISABLE KEYS */;
/*!40000 ALTER TABLE `notice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `online_exam`
--

DROP TABLE IF EXISTS `online_exam`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `online_exam` (
  `onlineExamID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(512) NOT NULL,
  `description` text,
  `classID` int(11) DEFAULT '0',
  `sectionID` int(11) DEFAULT '0',
  `studentGroupID` int(11) DEFAULT '0',
  `subjectID` int(11) DEFAULT '0',
  `userTypeID` int(11) DEFAULT '0',
  `instructionID` int(11) DEFAULT '0',
  `examStatus` varchar(11) NOT NULL,
  `schoolYearID` int(11) NOT NULL,
  `examTypeNumber` int(11) DEFAULT NULL,
  `startDateTime` datetime DEFAULT NULL,
  `endDateTime` datetime DEFAULT NULL,
  `duration` int(11) DEFAULT '0',
  `random` int(11) DEFAULT '0',
  `public` int(11) DEFAULT '0',
  `status` int(11) DEFAULT '1',
  `markType` int(11) NOT NULL,
  `negativeMark` int(11) DEFAULT '0',
  `bonusMark` int(11) DEFAULT '0',
  `point` int(11) DEFAULT '0',
  `percentage` int(11) DEFAULT '0',
  `showMarkAfterExam` int(11) DEFAULT '0',
  `judge` int(11) DEFAULT '1' COMMENT 'Auto Judge = 1, Manually Judge = 0',
  `paid` int(11) DEFAULT '0' COMMENT '0 = Unpaid, 1 = Paid',
  `validDays` int(11) DEFAULT '0',
  `cost` int(11) DEFAULT '0',
  `img` varchar(512) DEFAULT NULL,
  `create_date` datetime NOT NULL,
  `modify_date` datetime NOT NULL,
  `create_userID` int(11) NOT NULL,
  `create_usertypeID` int(11) NOT NULL,
  `published` int(11) NOT NULL,
  PRIMARY KEY (`onlineExamID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `online_exam`
--

LOCK TABLES `online_exam` WRITE;
/*!40000 ALTER TABLE `online_exam` DISABLE KEYS */;
/*!40000 ALTER TABLE `online_exam` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `online_exam_question`
--

DROP TABLE IF EXISTS `online_exam_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `online_exam_question` (
  `onlineExamQuestionID` int(11) NOT NULL AUTO_INCREMENT,
  `onlineExamID` int(11) NOT NULL,
  `questionID` int(11) DEFAULT NULL,
  PRIMARY KEY (`onlineExamQuestionID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `online_exam_question`
--

LOCK TABLES `online_exam_question` WRITE;
/*!40000 ALTER TABLE `online_exam_question` DISABLE KEYS */;
/*!40000 ALTER TABLE `online_exam_question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `online_exam_type`
--

DROP TABLE IF EXISTS `online_exam_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `online_exam_type` (
  `onlineExamTypeID` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(512) DEFAULT NULL,
  `examTypeNumber` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  PRIMARY KEY (`onlineExamTypeID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `online_exam_type`
--

LOCK TABLES `online_exam_type` WRITE;
/*!40000 ALTER TABLE `online_exam_type` DISABLE KEYS */;
INSERT INTO `online_exam_type` VALUES (1,'Date , Time and Duration',5,1),(2,'Date and Duration',4,1),(3,'Only Date',3,0),(4,'Only Duration',2,1),(5,'None',1,0);
/*!40000 ALTER TABLE `online_exam_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `online_exam_user_answer`
--

DROP TABLE IF EXISTS `online_exam_user_answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `online_exam_user_answer` (
  `onlineExamUserAnswerID` int(11) NOT NULL AUTO_INCREMENT,
  `onlineExamQuestionID` int(11) NOT NULL,
  `onlineExamRegisteredUserID` int(11) DEFAULT NULL,
  `userID` int(11) DEFAULT NULL,
  PRIMARY KEY (`onlineExamUserAnswerID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `online_exam_user_answer`
--

LOCK TABLES `online_exam_user_answer` WRITE;
/*!40000 ALTER TABLE `online_exam_user_answer` DISABLE KEYS */;
/*!40000 ALTER TABLE `online_exam_user_answer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `online_exam_user_answer_option`
--

DROP TABLE IF EXISTS `online_exam_user_answer_option`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `online_exam_user_answer_option` (
  `onlineExamUserAnswerOptionID` int(11) NOT NULL AUTO_INCREMENT,
  `questionID` int(11) NOT NULL,
  `optionID` int(11) DEFAULT NULL,
  `typeID` int(11) NOT NULL,
  `text` text,
  `time` datetime NOT NULL,
  PRIMARY KEY (`onlineExamUserAnswerOptionID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `online_exam_user_answer_option`
--

LOCK TABLES `online_exam_user_answer_option` WRITE;
/*!40000 ALTER TABLE `online_exam_user_answer_option` DISABLE KEYS */;
/*!40000 ALTER TABLE `online_exam_user_answer_option` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `online_exam_user_status`
--

DROP TABLE IF EXISTS `online_exam_user_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `online_exam_user_status` (
  `onlineExamUserStatus` int(11) NOT NULL AUTO_INCREMENT,
  `onlineExamID` int(11) NOT NULL,
  `duration` int(11) NOT NULL,
  `score` int(11) NOT NULL,
  `totalQuestion` int(11) NOT NULL,
  `totalAnswer` int(11) NOT NULL,
  `nagetiveMark` int(11) NOT NULL,
  `time` datetime NOT NULL,
  `userID` int(11) NOT NULL,
  `classesID` int(11) DEFAULT NULL,
  `sectionID` int(11) DEFAULT NULL,
  `examtimeID` int(11) DEFAULT NULL,
  `totalCurrectAnswer` int(11) DEFAULT NULL,
  `totalMark` varchar(40) DEFAULT NULL,
  `totalObtainedMark` int(11) DEFAULT NULL,
  `totalPercentage` double DEFAULT NULL,
  `statusID` int(11) DEFAULT NULL,
  PRIMARY KEY (`onlineExamUserStatus`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `online_exam_user_status`
--

LOCK TABLES `online_exam_user_status` WRITE;
/*!40000 ALTER TABLE `online_exam_user_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `online_exam_user_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `onlineadmission`
--

DROP TABLE IF EXISTS `onlineadmission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `onlineadmission` (
  `onlineadmissionID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `sex` varchar(10) DEFAULT NULL,
  `religion` varchar(25) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `phone` tinytext,
  `address` varchar(200) DEFAULT NULL,
  `classesID` int(11) DEFAULT NULL,
  `bloodgroup` varchar(5) DEFAULT NULL,
  `country` varchar(128) DEFAULT NULL,
  `photo` varchar(200) DEFAULT NULL,
  `schoolyearID` int(11) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `modify_date` datetime DEFAULT NULL,
  `studentID` int(11) NOT NULL DEFAULT '0',
  `status` int(11) DEFAULT '0' COMMENT '0 = New, 1=Approved, 2 = Waiting, 3 = Declined',
  PRIMARY KEY (`onlineadmissionID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `onlineadmission`
--

LOCK TABLES `onlineadmission` WRITE;
/*!40000 ALTER TABLE `onlineadmission` DISABLE KEYS */;
/*!40000 ALTER TABLE `onlineadmission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `pages` (
  `pagesID` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) DEFAULT NULL,
  `url` varchar(250) DEFAULT NULL,
  `content` text,
  `status` int(11) DEFAULT NULL COMMENT '1 => active, 2 => draft, 3 => trash, 4 => review  ',
  `visibility` int(11) DEFAULT NULL COMMENT '1 => public 2 => protected 3 => private ',
  `publish_date` datetime DEFAULT NULL,
  `parentID` int(11) NOT NULL DEFAULT '0',
  `pageorder` int(11) NOT NULL DEFAULT '0',
  `template` varchar(250) DEFAULT NULL,
  `featured_image` varchar(11) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `modify_date` datetime DEFAULT NULL,
  `create_userID` int(11) DEFAULT NULL,
  `create_username` varchar(60) DEFAULT NULL,
  `create_usertypeID` int(11) DEFAULT NULL,
  `password` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`pagesID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (1,'Admission','admission','',1,1,'2019-12-09 04:52:01',0,0,'admission','','2019-12-09 04:53:14','2019-12-09 04:53:25',1,'vishal',1,NULL),(2,'About','about','',1,1,'2019-12-09 04:54:01',0,0,'about','','2019-12-09 04:54:52','2019-12-09 04:54:52',1,'vishal',1,NULL),(3,'Event','event','',1,1,'2019-12-09 04:54:01',0,0,'event','','2019-12-09 04:55:11','2019-12-09 04:55:11',1,'vishal',1,NULL),(4,'Teachers','teachers','',1,1,'2019-12-09 04:55:01',0,0,'teacher','','2019-12-09 04:55:32','2019-12-09 04:55:32',1,'vishal',1,NULL),(5,'Gallery','gallery','',1,1,'2019-12-09 04:55:01',0,0,'gallery','','2019-12-09 04:55:53','2019-12-09 04:55:53',1,'vishal',1,NULL),(6,'Notice','notice','',1,1,'2019-12-09 04:55:01',0,0,'notice','','2019-12-09 04:56:19','2019-12-09 04:56:19',1,'vishal',1,NULL),(7,'Blog','blog','',1,1,'2019-12-09 04:56:01',0,0,'blog','','2019-12-09 04:56:42','2019-12-09 04:56:42',1,'vishal',1,NULL),(8,'Contact','contact','',1,1,'2019-12-09 04:56:01',0,0,'contact','','2019-12-09 04:57:13','2019-12-09 04:57:13',1,'vishal',1,NULL),(9,'Home','home','',1,1,'2019-12-09 04:57:01',0,0,'home','','2019-12-09 04:57:30','2019-12-09 04:57:30',1,'vishal',1,NULL);
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parents`
--

DROP TABLE IF EXISTS `parents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `parents` (
  `parentsID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `father_name` varchar(60) NOT NULL,
  `mother_name` varchar(60) NOT NULL,
  `father_profession` varchar(40) NOT NULL,
  `mother_profession` varchar(40) NOT NULL,
  `email` varchar(40) DEFAULT NULL,
  `phone` tinytext,
  `address` text,
  `photo` varchar(200) DEFAULT NULL,
  `username` varchar(40) NOT NULL,
  `password` varchar(128) NOT NULL,
  `usertypeID` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `modify_date` datetime NOT NULL,
  `create_userID` int(11) NOT NULL,
  `create_username` varchar(60) NOT NULL,
  `create_usertype` varchar(60) NOT NULL,
  `active` int(11) NOT NULL,
  PRIMARY KEY (`parentsID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parents`
--

LOCK TABLES `parents` WRITE;
/*!40000 ALTER TABLE `parents` DISABLE KEYS */;
/*!40000 ALTER TABLE `parents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment`
--

DROP TABLE IF EXISTS `payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `payment` (
  `paymentID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `schoolyearID` int(11) NOT NULL,
  `invoiceID` int(11) NOT NULL,
  `studentID` int(11) NOT NULL,
  `paymentamount` double DEFAULT NULL,
  `paymenttype` varchar(128) NOT NULL,
  `paymentdate` date NOT NULL,
  `paymentday` varchar(11) NOT NULL,
  `paymentmonth` varchar(10) NOT NULL,
  `paymentyear` year(4) NOT NULL,
  `userID` int(11) NOT NULL,
  `usertypeID` int(11) NOT NULL,
  `uname` varchar(60) NOT NULL,
  `transactionID` text,
  `globalpaymentID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`paymentID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment`
--

LOCK TABLES `payment` WRITE;
/*!40000 ALTER TABLE `payment` DISABLE KEYS */;
/*!40000 ALTER TABLE `payment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_relationships`
--

DROP TABLE IF EXISTS `permission_relationships`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `permission_relationships` (
  `permission_id` int(11) NOT NULL,
  `usertype_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_relationships`
--

LOCK TABLES `permission_relationships` WRITE;
/*!40000 ALTER TABLE `permission_relationships` DISABLE KEYS */;
INSERT INTO `permission_relationships` VALUES (501,1),(502,1),(503,1),(504,1),(505,1),(506,1),(507,1),(508,1),(509,1),(510,1),(511,1),(512,1),(513,1),(514,1),(515,1),(516,1),(517,1),(518,1),(519,1),(520,1),(521,1),(522,1),(523,1),(524,1),(525,1),(526,1),(527,1),(528,1),(530,1),(531,1),(532,1),(533,1),(534,1),(535,1),(536,1),(537,1),(538,1),(539,1),(540,1),(541,1),(542,1),(543,1),(544,1),(545,1),(546,1),(547,1),(548,1),(549,1),(550,1),(551,1),(552,1),(553,1),(554,1),(555,1),(556,1),(557,1),(558,1),(559,1),(560,1),(561,1),(562,1),(563,1),(564,1),(565,1),(566,1),(567,1),(568,1),(569,1),(570,1),(571,1),(572,1),(573,1),(574,1),(575,1),(576,1),(577,1),(578,1),(579,1),(580,1),(581,1),(582,1),(583,1),(584,1),(585,1),(586,1),(587,1),(588,1),(589,1),(590,1),(591,1),(592,1),(593,1),(594,1),(595,1),(596,1),(597,1),(598,1),(599,1),(600,1),(601,1),(602,1),(603,1),(604,1),(605,1),(606,1),(607,1),(608,1),(609,1),(610,1),(611,1),(612,1),(613,1),(614,1),(615,1),(616,1),(617,1),(618,1),(619,1),(620,1),(621,1),(622,1),(623,1),(624,1),(625,1),(626,1),(627,1),(628,1),(629,1),(630,1),(631,1),(632,1),(633,1),(634,1),(635,1),(636,1),(637,1),(638,1),(639,1),(640,1),(641,1),(642,1),(643,1),(644,1),(645,1),(646,1),(647,1),(648,1),(649,1),(650,1),(651,1),(652,1),(653,1),(654,1),(655,1),(656,1),(657,1),(658,1),(659,1),(660,1),(661,1),(662,1),(663,1),(664,1),(665,1),(666,1),(667,1),(668,1),(669,1),(670,1),(671,1),(672,1),(673,1),(674,1),(675,1),(676,1),(677,1),(678,1),(679,1),(680,1),(681,1),(682,1),(683,1),(684,1),(685,1),(686,1),(687,1),(688,1),(689,1),(690,1),(691,1),(692,1),(693,1),(694,1),(695,1),(696,1),(697,1),(698,1),(699,1),(700,1),(701,1),(702,1),(703,1),(704,1),(705,1),(706,1),(707,1),(708,1),(709,1),(710,1),(711,1),(712,1),(713,1),(714,1),(715,1),(716,1),(717,1),(718,1),(719,1),(720,1),(721,1),(722,1),(723,1),(724,1),(725,1),(726,1),(727,1),(728,1),(729,1),(730,1),(731,1),(732,1),(733,1),(734,1),(735,1),(736,1),(737,1),(738,1),(739,1),(740,1),(741,1),(742,1),(743,1),(744,1),(745,1),(746,1),(747,1),(748,1),(749,1),(750,1),(751,1),(752,1),(753,1),(754,1),(755,1),(756,1),(757,1),(758,1),(759,1),(760,1),(761,1),(762,1),(763,1),(764,1),(765,1),(766,1),(767,1),(768,1),(769,1),(770,1),(771,1),(772,1),(773,1),(774,1),(775,1),(776,1),(777,1),(778,1),(779,1),(780,1),(781,1),(782,1),(783,1),(784,1),(785,1),(786,1),(787,1),(788,1),(789,1),(790,1),(791,1),(792,1),(793,1),(794,1),(795,1),(796,1),(797,1),(798,1),(799,1),(800,1),(801,1),(802,1),(803,1),(804,1),(805,1),(806,1),(807,1),(808,1),(809,1),(810,1),(811,1),(812,1),(813,1),(814,1),(815,1),(816,1),(817,1),(818,1),(819,1),(820,1),(821,1),(822,1),(823,1),(824,1),(825,1),(826,1),(827,1),(828,1),(829,1),(830,1),(831,1),(832,1),(833,1),(834,1),(835,1),(836,1),(837,1),(838,1),(839,1),(840,1),(841,1),(842,1),(843,1),(844,1),(845,1),(846,1),(847,1),(848,1),(849,1),(850,1),(851,1),(852,1),(853,1),(854,1),(855,1),(856,1),(857,1),(858,1),(859,1),(860,1),(861,1),(862,1),(863,1),(864,1),(865,1),(866,1),(867,1),(868,1),(869,1),(870,1),(871,1),(501,2),(502,2),(506,2),(507,2),(511,2),(512,2),(516,2),(531,2),(535,2),(536,2),(537,2),(538,2),(539,2),(540,2),(541,2),(542,2),(543,2),(544,2),(548,2),(549,2),(550,2),(551,2),(553,2),(554,2),(556,2),(561,2),(569,2),(570,2),(571,2),(572,2),(573,2),(579,2),(580,2),(581,2),(582,2),(586,2),(587,2),(588,2),(590,2),(591,2),(592,2),(594,2),(595,2),(596,2),(598,2),(599,2),(600,2),(601,2),(603,2),(604,2),(605,2),(607,2),(683,2),(684,2),(685,2),(686,2),(687,2),(688,2),(693,2),(694,2),(695,2),(705,2),(713,2),(717,2),(718,2),(727,2),(731,2),(761,2),(765,2),(766,2),(770,2),(771,2),(775,2),(777,2),(780,2),(781,2),(782,2),(783,2),(787,2),(788,2),(789,2),(790,2),(791,2),(793,2),(794,2),(820,2),(821,2),(824,2),(501,3),(502,3),(512,3),(516,3),(531,3),(539,3),(543,3),(544,3),(548,3),(561,3),(571,3),(579,3),(580,3),(683,3),(684,3),(685,3),(686,3),(687,3),(693,3),(700,3),(705,3),(709,3),(712,3),(713,3),(717,3),(718,3),(722,3),(727,3),(731,3),(744,3),(748,3),(749,3),(761,3),(765,3),(766,3),(770,3),(771,3),(775,3),(820,3),(821,3),(824,3),(501,4),(502,4),(506,4),(512,4),(516,4),(531,4),(535,4),(544,4),(548,4),(550,4),(561,4),(571,4),(573,4),(579,4),(580,4),(693,4),(696,4),(700,4),(704,4),(705,4),(709,4),(712,4),(718,4),(722,4),(726,4),(727,4),(731,4),(735,4),(739,4),(744,4),(748,4),(749,4),(761,4),(765,4),(766,4),(770,4),(771,4),(775,4),(820,4),(821,4),(824,4),(501,5),(512,5),(516,5),(554,5),(556,5),(579,5),(580,5),(608,5),(609,5),(610,5),(611,5),(612,5),(613,5),(614,5),(615,5),(616,5),(617,5),(618,5),(619,5),(620,5),(621,5),(622,5),(649,5),(650,5),(651,5),(652,5),(653,5),(654,5),(655,5),(656,5),(657,5),(658,5),(659,5),(660,5),(661,5),(662,5),(663,5),(664,5),(665,5),(666,5),(667,5),(668,5),(669,5),(670,5),(671,5),(672,5),(673,5),(674,5),(683,5),(684,5),(685,5),(686,5),(687,5),(718,5),(722,5),(723,5),(724,5),(725,5),(726,5),(727,5),(731,5),(735,5),(736,5),(737,5),(738,5),(739,5),(740,5),(741,5),(742,5),(743,5),(744,5),(745,5),(746,5),(747,5),(748,5),(749,5),(750,5),(751,5),(752,5),(753,5),(754,5),(755,5),(756,5),(757,5),(758,5),(759,5),(760,5),(761,5),(765,5),(766,5),(770,5),(771,5),(775,5),(798,5),(799,5),(800,5),(801,5),(802,5),(803,5),(804,5),(805,5),(806,5),(820,5),(821,5),(824,5),(501,6),(512,6),(516,6),(531,6),(554,6),(556,6),(579,6),(580,6),(683,6),(684,6),(685,6),(686,6),(687,6),(700,6),(701,6),(702,6),(703,6),(704,6),(705,6),(706,6),(707,6),(708,6),(709,6),(710,6),(711,6),(712,6),(713,6),(714,6),(715,6),(716,6),(717,6),(718,6),(727,6),(731,6),(761,6),(765,6),(766,6),(770,6),(771,6),(775,6),(777,6),(784,6),(785,6),(786,6),(820,6),(821,6),(824,6),(501,7),(502,7),(506,7),(507,7),(511,7),(512,7),(516,7),(517,7),(521,7),(548,7),(550,7),(551,7),(553,7),(554,7),(556,7),(579,7),(580,7),(683,7),(684,7),(685,7),(686,7),(687,7),(727,7),(731,7),(761,7),(765,7),(766,7),(770,7),(771,7),(775,7),(809,7),(810,7),(811,7),(820,7),(821,7),(824,7);
/*!40000 ALTER TABLE `permission_relationships` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `permissions` (
  `permissionID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'In most cases, this should be the name of the module (e.g. news)',
  `active` enum('yes','no') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`permissionID`)
) ENGINE=InnoDB AUTO_INCREMENT=872 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (501,'Dashboard','dashboard','yes'),(502,'Student','student','yes'),(503,'Student Add','student_add','yes'),(504,'Student Edit','student_edit','yes'),(505,'Student Delete','student_delete','yes'),(506,'Student View','student_view','yes'),(507,'Parents','parents','yes'),(508,'Parents Add','parents_add','yes'),(509,'Parents Edit','parents_edit','yes'),(510,'Parents Delete','parents_delete','yes'),(511,'Parents View','parents_view','yes'),(512,'Teacher','teacher','yes'),(513,'Teacher Add','teacher_add','yes'),(514,'Teacher Edit','teacher_edit','yes'),(515,'Teacher Delete','teacher_delete','yes'),(516,'Teacher View','teacher_view','yes'),(517,'User','user','yes'),(518,'User Add','user_add','yes'),(519,'User Edit','user_edit','yes'),(520,'User Delete','user_delete','yes'),(521,'User View','user_view','yes'),(522,'Class','classes','yes'),(523,'Class Add','classes_add','yes'),(524,'Class Edit','classes_edit','yes'),(525,'Class Delete','classes_delete','yes'),(526,'Section','section','yes'),(527,'Section Add','section_add','yes'),(528,'Section Edit','section_edit','yes'),(529,'Semester Delete','semester_delete','yes'),(530,'Section Delete','section_delete','yes'),(531,'Subject','subject','yes'),(532,'Subject Add','subject_add','yes'),(533,'Subject Edit','subject_edit','yes'),(534,'Subject Delete','subject_delete','yes'),(535,'Syllabus','syllabus','yes'),(536,'Syllabus Add','syllabus_add','yes'),(537,'Syllabus Edit','syllabus_edit','yes'),(538,'Syllabus Delete','syllabus_delete','yes'),(539,'Assignment','assignment','yes'),(540,'Assignment Add','assignment_add','yes'),(541,'Assignment Edit','assignment_edit','yes'),(542,'Assignment Delete','assignment_delete','yes'),(543,'Assignment View','assignment_view','yes'),(544,'Routine','routine','yes'),(545,'Routine Add','routine_add','yes'),(546,'Routine Edit','routine_edit','yes'),(547,'Routine Delete','routine_delete','yes'),(548,'Student Attendance','sattendance','yes'),(549,'Student Attendance Add','sattendance_add','yes'),(550,'Student Attendance View','sattendance_view','yes'),(551,'Teacher Attendance','tattendance','yes'),(552,'Teacher Attendance Add','tattendance_add','yes'),(553,'Teacher Attendance View','tattendance_view','yes'),(554,'User Attendance','uattendance','yes'),(555,'User Attendance Add','uattendance_add','yes'),(556,'User Attendance View','uattendance_view','yes'),(557,'Exam','exam','yes'),(558,'Exam Add','exam_add','yes'),(559,'Exam Edit','exam_edit','yes'),(560,'Exam Delete','exam_delete','yes'),(561,'Examschedule','examschedule','yes'),(562,'Examschedule Add','examschedule_add','yes'),(563,'Examschedule Edit','examschedule_edit','yes'),(564,'Examschedule Delete','examschedule_delete','yes'),(565,'Grade','grade','yes'),(566,'Grade Add','grade_add','yes'),(567,'Grade Edit','grade_edit','yes'),(568,'Grade Delete','grade_delete','yes'),(569,'Exam Attendance','eattendance','yes'),(570,'Exam Attendance Add','eattendance_add','yes'),(571,'Mark','mark','yes'),(572,'Mark Add','mark_add','yes'),(573,'Mark View','mark_view','yes'),(574,'Mark Distribution','markpercentage','yes'),(575,'Mark Distribution Add','markpercentage_add','yes'),(576,'Mark Distribution Edit','markpercentage_edit','yes'),(577,'Mark Distribution Delete','markpercentage_delete','yes'),(578,'Promotion','promotion','yes'),(579,'Message','conversation','yes'),(580,'Media','media','yes'),(581,'Media Add','media_add','yes'),(582,'Media Delete','media_delete','yes'),(583,'Mail / SMS','mailandsms','yes'),(584,'Mail / SMS Add','mailandsms_add','yes'),(585,'Mail / SMS View','mailandsms_view','yes'),(586,'Question Group','question_group','yes'),(587,'Question Group Add','question_group_add','yes'),(588,'Question Group Edit','question_group_edit','yes'),(589,'Question Group Delete','question_group_delete','yes'),(590,'Question Level','question_level','yes'),(591,'Question Level Add','question_level_add','yes'),(592,'Question Level Edit','question_level_edit','yes'),(593,'Question Level Delete','question_level_delete','yes'),(594,'Question Bank','question_bank','yes'),(595,'Question Bank Add','question_bank_add','yes'),(596,'Question Bank Edit','question_bank_edit','yes'),(597,'Question Bank Delete','question_bank_delete','yes'),(598,'Question Bank View','question_bank_view','yes'),(599,'Online Exam','online_exam','yes'),(600,'Online Exam Add','online_exam_add','yes'),(601,'Online Exam Edit','online_exam_edit','yes'),(602,'Online Exam Delete','online_exam_delete','yes'),(603,'Instruction','instruction','yes'),(604,'Instruction Add','instruction_add','yes'),(605,'Instruction Edit','instruction_edit','yes'),(606,'Instruction Delete','instruction_delete','yes'),(607,'Instruction View','instruction_view','yes'),(608,'Salary Template','salary_template','yes'),(609,'Salary Template Add','salary_template_add','yes'),(610,'Salary Template Edit','salary_template_edit','yes'),(611,'Salary Template Delete','salary_template_delete','yes'),(612,'Salary Template View','salary_template_view','yes'),(613,'Hourly Template','hourly_template','yes'),(614,'Hourly Template Add','hourly_template_add','yes'),(615,'Hourly Template Edit','hourly_template_edit','yes'),(616,'Hourly Template Delete','hourly_template_delete','yes'),(617,'Manage Salary','manage_salary','yes'),(618,'Manage Salary Add','manage_salary_add','yes'),(619,'Manage Salary Edit','manage_salary_edit','yes'),(620,'Manage Salary Delete','manage_salary_delete','yes'),(621,'Manage Salary View','manage_salary_view','yes'),(622,'Make Payment','make_payment','yes'),(623,'Vendor','vendor','yes'),(624,'Vendor Add','vendor_add','yes'),(625,'Vendor Edit','vendor_edit','yes'),(626,'Vendor Delete','vendor_delete','yes'),(627,'Location','location','yes'),(628,'Location Add','location_add','yes'),(629,'Location Edit','location_edit','yes'),(630,'Location Delete','location_delete','yes'),(631,'Asset Category','asset_category','yes'),(632,'Asset Category Add','asset_category_add','yes'),(633,'Asset Category Edit','asset_category_edit','yes'),(634,'Asset Category Delete','asset_category_delete','yes'),(635,'Asset','asset','yes'),(636,'Asset Add','asset_add','yes'),(637,'Asset Edit','asset_edit','yes'),(638,'Asset Delete','asset_delete','yes'),(639,'Asset View','asset_view','yes'),(640,'Asset Assignment','asset_assignment','yes'),(641,'Asset Assignment Add','asset_assignment_add','yes'),(642,'Asset Assignment Edit','asset_assignment_edit','yes'),(643,'Asset Assignment Delete','asset_assignment_delete','yes'),(644,'Asset Assignment View','asset_assignment_view','yes'),(645,'Purchase','purchase','yes'),(646,'Purchase Add','purchase_add','yes'),(647,'Purchase Edit','purchase_edit','yes'),(648,'Purchase Delete','purchase_delete','yes'),(649,'Product Category','productcategory','yes'),(650,'Product Category Add','productcategory_add','yes'),(651,'Product Category Edit','productcategory_edit','yes'),(652,'Product Category Delete','productcategory_delete','yes'),(653,'Product','product','yes'),(654,'Product Add','product_add','yes'),(655,'Product Edit','product_edit','yes'),(656,'Product Delete','product_delete','yes'),(657,'Warehouse','productwarehouse','yes'),(658,'Warehouse Add','productwarehouse_add','yes'),(659,'Warehouse Edit','productwarehouse_edit','yes'),(660,'Warehouse Delete','productwarehouse_delete','yes'),(661,'Supplier','productsupplier','yes'),(662,'Supplier Add','productsupplier_add','yes'),(663,'Supplier Edit','productsupplier_edit','yes'),(664,'Supplier Delete','productsupplier_delete','yes'),(665,'Purchase','productpurchase','yes'),(666,'Purchase Add','productpurchase_add','yes'),(667,'Purchase Edit','productpurchase_edit','yes'),(668,'Purchase Delete','productpurchase_delete','yes'),(669,'Purchase View','productpurchase_view','yes'),(670,'Sale','productsale','yes'),(671,'Sale Add','productsale_add','yes'),(672,'Sale Edit','productsale_edit','yes'),(673,'Sale Delete','productsale_delete','yes'),(674,'Sale View','productsale_view','yes'),(675,'Leave Category','leavecategory','yes'),(676,'Leave Category Add','leavecategory_add','yes'),(677,'Leave Category Edit','leavecategory_edit','yes'),(678,'Leave Category Delete','leavecategory_delete','yes'),(679,'Leave Assign','leaveassign','yes'),(680,'Leave Assign Add','leaveassign_add','yes'),(681,'Leave Assign Edit','leaveassign_edit','yes'),(682,'Leave Assign Delete','leaveassign_delete','yes'),(683,'Leave Apply','leaveapply','yes'),(684,'Leave Apply Add','leaveapply_add','yes'),(685,'Leave Apply Edit','leaveapply_edit','yes'),(686,'Leave Apply Delete','leaveapply_delete','yes'),(687,'Leave Apply View','leaveapply_view','yes'),(688,'Leave Application','leaveapplication','yes'),(689,'Activities Category','activitiescategory','yes'),(690,'Activities Category Add','activitiescategory_add','yes'),(691,'Activities Category Edit','activitiescategory_edit','yes'),(692,'Activities Category Delete','activitiescategory_delete','yes'),(693,'Activities','activities','yes'),(694,'Activities Add','activities_add','yes'),(695,'Activities Delete','activities_delete','yes'),(696,'Child Care','childcare','yes'),(697,'Child Care Add','childcare_add','yes'),(698,'Child Care Edit','childcare_edit','yes'),(699,'Child Care Delete','childcare_delete','yes'),(700,'Library Member','lmember','yes'),(701,'Library Member Add','lmember_add','yes'),(702,'Library Member Edit','lmember_edit','yes'),(703,'Library Member Delete','lmember_delete','yes'),(704,'Library Member View','lmember_view','yes'),(705,'Books','book','yes'),(706,'Books Add','book_add','yes'),(707,'Books Edit','book_edit','yes'),(708,'Books Delete','book_delete','yes'),(709,'Issue Book','issue','yes'),(710,'Issue Book Add','issue_add','yes'),(711,'Issue Book Edit','issue_edit','yes'),(712,'Issue Book View','issue_view','yes'),(713,'E-Books','ebooks','yes'),(714,'E-Books Add','ebooks_add','yes'),(715,'E-Books Edit','ebooks_edit','yes'),(716,'E-Books Delete','ebooks_delete','yes'),(717,'E-Books View','ebooks_view','yes'),(718,'Transport','transport','yes'),(719,'Transport Add','transport_add','yes'),(720,'Transport Edit','transport_edit','yes'),(721,'Transport Delete','transport_delete','yes'),(722,'Transport Member','tmember','yes'),(723,'Transport Member Add','tmember_add','yes'),(724,'Transport Member Edit','tmember_edit','yes'),(725,'Transport Member Delete','tmember_delete','yes'),(726,'Transport Member View','tmember_view','yes'),(727,'Hostel','hostel','yes'),(728,'Hostel Add','hostel_add','yes'),(729,'Hostel Edit','hostel_edit','yes'),(730,'Hostel Delete','hostel_delete','yes'),(731,'Hostel Category','category','yes'),(732,'Hostel Category Add','category_add','yes'),(733,'Hostel Category Edit','category_edit','yes'),(734,'Hostel Category Delete','category_delete','yes'),(735,'Hostel Member','hmember','yes'),(736,'Hostel Member Add','hmember_add','yes'),(737,'Hostel Member Edit','hmember_edit','yes'),(738,'Hostel Member Delete','hmember_delete','yes'),(739,'Hostel Member View','hmember_view','yes'),(740,'Fee Types','feetypes','yes'),(741,'Fee Types Add','feetypes_add','yes'),(742,'Fee Types Edit','feetypes_edit','yes'),(743,'Fee Types Delete','feetypes_delete','yes'),(744,'Invoice','invoice','yes'),(745,'Invoice Add','invoice_add','yes'),(746,'Invoice Edit','invoice_edit','yes'),(747,'Invoice Delete','invoice_delete','yes'),(748,'Invoice View','invoice_view','yes'),(749,'Payment History','paymenthistory','yes'),(750,'Payment History Edit','paymenthistory_edit','yes'),(751,'Payment History Delete','paymenthistory_delete','yes'),(752,'Expense','expense','yes'),(753,'Expense Add','expense_add','yes'),(754,'Expense Edit','expense_edit','yes'),(755,'Expense Delete','expense_delete','yes'),(756,'Income','income','yes'),(757,'Income Add','income_add','yes'),(758,'Income Edit','income_edit','yes'),(759,'Income Delete','income_delete','yes'),(760,'Global Payment','global_payment','yes'),(761,'Notice','notice','yes'),(762,'Notice Add','notice_add','yes'),(763,'Notice Edit','notice_edit','yes'),(764,'Notice Delete','notice_delete','yes'),(765,'Notice View','notice_view','yes'),(766,'Event','event','yes'),(767,'Event Add','event_add','yes'),(768,'Event Edit','event_edit','yes'),(769,'Event Delete','event_delete','yes'),(770,'Event View','event_view','yes'),(771,'Holiday','holiday','yes'),(772,'Holiday Add','holiday_add','yes'),(773,'Holiday Edit','holiday_edit','yes'),(774,'Holiday Delete','holiday_delete','yes'),(775,'Holiday View','holiday_view','yes'),(776,'Classes Report','classesreport','yes'),(777,'Student Report','studentreport','yes'),(778,'ID Card Report','idcardreport','yes'),(779,'Admit Card Report','admitcardreport','yes'),(780,'Routine Report','routinereport','yes'),(781,'Exam Schedule Report','examschedulereport','yes'),(782,'Attendance Report','attendancereport','yes'),(783,'Attendance Overview Report','attendanceoverviewreport','yes'),(784,'Library Books Report','librarybooksreport','yes'),(785,'Library Card Report','librarycardreport','yes'),(786,'Library Book Issue Report','librarybookissuereport','yes'),(787,'Terminal Report','terminalreport','yes'),(788,'Merit Stage Report','meritstagereport','yes'),(789,'Tabulation Sheet Report','tabulationsheetreport','yes'),(790,'Mark Sheet Report','marksheetreport','yes'),(791,'Progress Card Report','progresscardreport','yes'),(792,'Student Session Report','studentsessionreport','yes'),(793,'Online Exam Report','onlineexamreport','yes'),(794,'Online Exam Question Report','onlineexamquestionreport','yes'),(795,'Online Admission Report','onlineadmissionreport','yes'),(796,'Certificate Report','certificatereport','yes'),(797,'Leave Application Report','leaveapplicationreport','yes'),(798,'Product Purchase Report','productpurchasereport','yes'),(799,'Product Sale Report','productsalereport','yes'),(800,'Search Payment Fees Report','searchpaymentfeesreport','yes'),(801,'Fees Report','feesreport','yes'),(802,'Due Fees Report','duefeesreport','yes'),(803,'Balance Fees Report','balancefeesreport','yes'),(804,'Transaction','transactionreport','yes'),(805,'Student Fine Report','studentfinereport','yes'),(806,'Salary Report','salaryreport','yes'),(807,'Account Ledger Report','accountledgerreport','yes'),(808,'Online Admission','onlineadmission','yes'),(809,'Visitor Information','visitorinfo','yes'),(810,'Visitor Information Delete','visitorinfo_delete','yes'),(811,'Visitor Infomation View','visitorinfo_view','yes'),(812,'Academic Year','schoolyear','yes'),(813,'Academic Year Add','schoolyear_add','yes'),(814,'Academic Year Edit','schoolyear_edit','yes'),(815,'Academic Year Delete','schoolyear_delete','yes'),(816,'Student Group','studentgroup','yes'),(817,'Student Group Add','studentgroup_add','yes'),(818,'Student Group Edit','studentgroup_edit','yes'),(819,'Student Group Delete','studentgroup_delete','yes'),(820,'Complain','complain','yes'),(821,'Complain Add','complain_add','yes'),(822,'Complain Edit','complain_edit','yes'),(823,'Complain Delete','complain_delete','yes'),(824,'Complain View','complain_view','yes'),(825,'Certificate Template','certificate_template','yes'),(826,'Certificate Template Add','certificate_template_add','yes'),(827,'Certificate Template Edit','certificate_template_edit','yes'),(828,'Certificate Template Delete','certificate_template_delete','yes'),(829,'Certificate Template View','certificate_template_view','yes'),(830,'System Admin','systemadmin','yes'),(831,'System Admin Add','systemadmin_add','yes'),(832,'System Admin Edit','systemadmin_edit','yes'),(833,'System Admin Delete','systemadmin_delete','yes'),(834,'System Admin View','systemadmin_view','yes'),(835,'Reset Password','resetpassword','yes'),(836,'Social Link','sociallink','yes'),(837,'Social Link Add','sociallink_add','yes'),(838,'Social Link Edit','sociallink_edit','yes'),(839,'Social Link Delete','sociallink_delete','yes'),(840,'Mail / SMS Template','mailandsmstemplate','yes'),(841,'Mail / SMS Template Add','mailandsmstemplate_add','yes'),(842,'Mail / SMS Template Edit','mailandsmstemplate_edit','yes'),(843,'Mail / SMS Template Delete','mailandsmstemplate_delete','yes'),(844,'Mail / SMS Template View','mailandsmstemplate_view','yes'),(845,'Import','bulkimport ','yes'),(846,'Backup','backup','yes'),(847,'Role','usertype','yes'),(848,'Role Add','usertype_add','yes'),(849,'Role Edit','usertype_edit','yes'),(850,'Role Delete','usertype_delete','yes'),(851,'Permission','permission','yes'),(852,'Auto Update','update','yes'),(853,'Posts Categories','posts_categories','yes'),(854,'Posts Categories Add','posts_categories_add','yes'),(855,'Posts Categories Edit','posts_categories_edit','yes'),(856,'Posts Categories Delete','posts_categories_delete','yes'),(857,'Posts','posts','yes'),(858,'Posts Add','posts_add','yes'),(859,'Posts Edit','posts_edit','yes'),(860,'Posts Delete','posts_delete','yes'),(861,'Pages','pages','yes'),(862,'Pages Add','pages_add','yes'),(863,'Pages Edit','pages_edit','yes'),(864,'Pages Delete','pages_delete','yes'),(865,'Menu','frontendmenu','yes'),(866,'General Setting','setting','yes'),(867,'Frontend Setting','frontend_setting','yes'),(868,'Payment Settings','paymentsettings','yes'),(869,'SMS Settings','smssettings','yes'),(870,'Email Setting','emailsetting','yes'),(871,'Mark Settings','marksetting','yes');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `posts` (
  `postsID` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) DEFAULT NULL,
  `url` varchar(250) DEFAULT NULL,
  `content` text,
  `status` int(11) DEFAULT NULL COMMENT '1 => active, 2 => draft, 3 => trash, 4 => review  ',
  `visibility` int(11) DEFAULT NULL COMMENT '1 => public 2 => protected 3 => private ',
  `publish_date` datetime DEFAULT NULL,
  `parentID` int(11) NOT NULL DEFAULT '0',
  `postorder` int(11) NOT NULL DEFAULT '0',
  `featured_image` varchar(11) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `modify_date` datetime DEFAULT NULL,
  `create_userID` int(11) DEFAULT NULL,
  `create_username` varchar(60) DEFAULT NULL,
  `create_usertypeID` int(11) DEFAULT NULL,
  `password` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`postsID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts_categories`
--

DROP TABLE IF EXISTS `posts_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `posts_categories` (
  `posts_categoriesID` int(11) NOT NULL AUTO_INCREMENT,
  `posts_categories` varchar(40) DEFAULT NULL,
  `posts_slug` varchar(250) DEFAULT NULL,
  `posts_parent` int(11) DEFAULT '0',
  `posts_description` text,
  PRIMARY KEY (`posts_categoriesID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts_categories`
--

LOCK TABLES `posts_categories` WRITE;
/*!40000 ALTER TABLE `posts_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `posts_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts_category`
--

DROP TABLE IF EXISTS `posts_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `posts_category` (
  `posts_categoryID` int(11) NOT NULL AUTO_INCREMENT,
  `postsID` int(11) NOT NULL,
  `posts_categoriesID` int(11) NOT NULL,
  PRIMARY KEY (`posts_categoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts_category`
--

LOCK TABLES `posts_category` WRITE;
/*!40000 ALTER TABLE `posts_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `posts_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `product` (
  `productID` int(11) NOT NULL AUTO_INCREMENT,
  `productcategoryID` int(11) NOT NULL,
  `productname` varchar(128) NOT NULL,
  `productbuyingprice` double NOT NULL,
  `productsellingprice` double NOT NULL,
  `productdesc` text NOT NULL,
  `create_date` datetime NOT NULL,
  `modify_date` datetime NOT NULL,
  `create_userID` int(11) NOT NULL,
  `create_usertypeID` int(11) NOT NULL,
  PRIMARY KEY (`productID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productcategory`
--

DROP TABLE IF EXISTS `productcategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `productcategory` (
  `productcategoryID` int(11) NOT NULL AUTO_INCREMENT,
  `productcategoryname` varchar(128) NOT NULL,
  `productcategorydesc` text NOT NULL,
  `create_date` datetime NOT NULL,
  `modify_date` datetime NOT NULL,
  `create_userID` int(11) NOT NULL,
  `create_usertypeID` int(11) NOT NULL,
  PRIMARY KEY (`productcategoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productcategory`
--

LOCK TABLES `productcategory` WRITE;
/*!40000 ALTER TABLE `productcategory` DISABLE KEYS */;
/*!40000 ALTER TABLE `productcategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productpurchase`
--

DROP TABLE IF EXISTS `productpurchase`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `productpurchase` (
  `productpurchaseID` int(11) NOT NULL AUTO_INCREMENT,
  `schoolyearID` int(11) NOT NULL,
  `productsupplierID` int(11) NOT NULL,
  `productwarehouseID` int(11) NOT NULL,
  `productpurchasereferenceno` varchar(100) NOT NULL,
  `productpurchasedate` date NOT NULL,
  `productpurchasefile` varchar(200) DEFAULT NULL,
  `productpurchasefileorginalname` varchar(200) DEFAULT NULL,
  `productpurchasedescription` text,
  `productpurchasestatus` int(11) NOT NULL COMMENT '0 = pending, 1 = partial_paid,  2 = fully_paid',
  `productpurchaserefund` int(11) NOT NULL DEFAULT '0' COMMENT '0 = not refund, 1 = refund ',
  `productpurchasetaxID` int(11) NOT NULL DEFAULT '0',
  `productpurchasetaxamount` double NOT NULL DEFAULT '0',
  `productpurchasediscount` double NOT NULL DEFAULT '0',
  `productpurchaseshipping` double NOT NULL DEFAULT '0',
  `productpurchasepaymentterm` int(11) NOT NULL DEFAULT '0',
  `create_date` datetime NOT NULL,
  `modify_date` datetime NOT NULL,
  `create_userID` int(11) NOT NULL,
  `create_usertypeID` int(11) NOT NULL,
  PRIMARY KEY (`productpurchaseID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productpurchase`
--

LOCK TABLES `productpurchase` WRITE;
/*!40000 ALTER TABLE `productpurchase` DISABLE KEYS */;
/*!40000 ALTER TABLE `productpurchase` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productpurchaseitem`
--

DROP TABLE IF EXISTS `productpurchaseitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `productpurchaseitem` (
  `productpurchaseitemID` int(11) NOT NULL AUTO_INCREMENT,
  `schoolyearID` int(11) NOT NULL,
  `productpurchaseID` int(11) NOT NULL,
  `productID` int(11) NOT NULL,
  `productpurchaseunitprice` double NOT NULL,
  `productpurchasequantity` double NOT NULL,
  PRIMARY KEY (`productpurchaseitemID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productpurchaseitem`
--

LOCK TABLES `productpurchaseitem` WRITE;
/*!40000 ALTER TABLE `productpurchaseitem` DISABLE KEYS */;
/*!40000 ALTER TABLE `productpurchaseitem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productpurchasepaid`
--

DROP TABLE IF EXISTS `productpurchasepaid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `productpurchasepaid` (
  `productpurchasepaidID` int(11) NOT NULL AUTO_INCREMENT,
  `productpurchasepaidschoolyearID` int(11) NOT NULL,
  `schoolyearID` int(11) NOT NULL,
  `productpurchaseID` int(11) NOT NULL,
  `productpurchasepaiddate` date NOT NULL,
  `productpurchasepaidreferenceno` varchar(100) NOT NULL,
  `productpurchasepaidamount` double NOT NULL,
  `productpurchasepaidpaymentmethod` int(11) NOT NULL COMMENT '1 = cash, 2 = cheque, 3 = crediit card, 4 = other',
  `productpurchasepaidfile` varchar(200) DEFAULT NULL,
  `productpurchasepaidorginalname` varchar(200) DEFAULT NULL,
  `productpurchasepaiddescription` text,
  `create_date` datetime NOT NULL,
  `modify_date` datetime NOT NULL,
  `create_userID` int(11) NOT NULL,
  `create_usertypeID` int(11) NOT NULL,
  PRIMARY KEY (`productpurchasepaidID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productpurchasepaid`
--

LOCK TABLES `productpurchasepaid` WRITE;
/*!40000 ALTER TABLE `productpurchasepaid` DISABLE KEYS */;
/*!40000 ALTER TABLE `productpurchasepaid` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productsale`
--

DROP TABLE IF EXISTS `productsale`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `productsale` (
  `productsaleID` int(11) NOT NULL AUTO_INCREMENT,
  `schoolyearID` int(11) NOT NULL,
  `productsalecustomertypeID` int(11) NOT NULL,
  `productsalecustomerID` int(11) NOT NULL,
  `productsalereferenceno` varchar(100) NOT NULL,
  `productsaledate` date NOT NULL,
  `productsalefile` varchar(200) DEFAULT NULL,
  `productsalefileorginalname` varchar(200) DEFAULT NULL,
  `productsaledescription` text,
  `productsalestatus` int(11) NOT NULL COMMENT '0 = select_payment_status, 1 = due,  2 = partial, 3 = Paid',
  `productsalerefund` int(11) NOT NULL DEFAULT '0' COMMENT '0 = not refund, 1 = refund ',
  `productsaletaxID` int(11) NOT NULL DEFAULT '0',
  `productsaletaxamount` double NOT NULL DEFAULT '0',
  `productsalediscount` double NOT NULL DEFAULT '0',
  `productsaleshipping` double NOT NULL DEFAULT '0',
  `productsalepaymentterm` int(11) NOT NULL DEFAULT '0',
  `create_date` datetime NOT NULL,
  `modify_date` datetime NOT NULL,
  `create_userID` int(11) NOT NULL,
  `create_usertypeID` int(11) NOT NULL,
  PRIMARY KEY (`productsaleID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productsale`
--

LOCK TABLES `productsale` WRITE;
/*!40000 ALTER TABLE `productsale` DISABLE KEYS */;
/*!40000 ALTER TABLE `productsale` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productsaleitem`
--

DROP TABLE IF EXISTS `productsaleitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `productsaleitem` (
  `productsaleitemID` int(11) NOT NULL AUTO_INCREMENT,
  `schoolyearID` int(11) NOT NULL,
  `productsaleID` int(11) NOT NULL,
  `productID` int(11) NOT NULL,
  `productsaleserialno` varchar(100) DEFAULT '0',
  `productsaleunitprice` double NOT NULL,
  `productsalequantity` double NOT NULL,
  PRIMARY KEY (`productsaleitemID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productsaleitem`
--

LOCK TABLES `productsaleitem` WRITE;
/*!40000 ALTER TABLE `productsaleitem` DISABLE KEYS */;
/*!40000 ALTER TABLE `productsaleitem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productsalepaid`
--

DROP TABLE IF EXISTS `productsalepaid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `productsalepaid` (
  `productsalepaidID` int(11) NOT NULL AUTO_INCREMENT,
  `productsalepaidschoolyearID` int(11) NOT NULL,
  `schoolyearID` int(11) NOT NULL,
  `productsaleID` int(11) NOT NULL,
  `productsalepaiddate` date NOT NULL,
  `productsalepaidreferenceno` varchar(100) NOT NULL,
  `productsalepaidamount` double NOT NULL,
  `productsalepaidpaymentmethod` int(11) NOT NULL COMMENT '1 = cash, 2 = cheque, 3 = crediit card, 4 = other',
  `productsalepaidfile` varchar(200) DEFAULT NULL,
  `productsalepaidorginalname` varchar(200) DEFAULT NULL,
  `productsalepaiddescription` text,
  `create_date` datetime NOT NULL,
  `modify_date` datetime NOT NULL,
  `create_userID` int(11) NOT NULL,
  `create_usertypeID` int(11) NOT NULL,
  PRIMARY KEY (`productsalepaidID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productsalepaid`
--

LOCK TABLES `productsalepaid` WRITE;
/*!40000 ALTER TABLE `productsalepaid` DISABLE KEYS */;
/*!40000 ALTER TABLE `productsalepaid` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productsupplier`
--

DROP TABLE IF EXISTS `productsupplier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `productsupplier` (
  `productsupplierID` int(11) NOT NULL AUTO_INCREMENT,
  `productsuppliercompanyname` varchar(128) NOT NULL,
  `productsuppliername` varchar(40) NOT NULL,
  `productsupplieremail` varchar(40) DEFAULT NULL,
  `productsupplierphone` varchar(20) DEFAULT NULL,
  `productsupplieraddress` text,
  `create_date` datetime NOT NULL,
  `modify_date` datetime NOT NULL,
  `create_userID` int(11) NOT NULL,
  `create_usertypeID` int(11) NOT NULL,
  PRIMARY KEY (`productsupplierID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productsupplier`
--

LOCK TABLES `productsupplier` WRITE;
/*!40000 ALTER TABLE `productsupplier` DISABLE KEYS */;
/*!40000 ALTER TABLE `productsupplier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productwarehouse`
--

DROP TABLE IF EXISTS `productwarehouse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `productwarehouse` (
  `productwarehouseID` int(11) NOT NULL AUTO_INCREMENT,
  `productwarehousename` varchar(128) NOT NULL,
  `productwarehousecode` varchar(128) NOT NULL,
  `productwarehouseemail` varchar(40) DEFAULT NULL,
  `productwarehousephone` varchar(20) DEFAULT NULL,
  `productwarehouseaddress` text,
  `create_date` datetime NOT NULL,
  `modify_date` datetime NOT NULL,
  `create_userID` int(11) NOT NULL,
  `create_usertypeID` int(11) NOT NULL,
  PRIMARY KEY (`productwarehouseID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productwarehouse`
--

LOCK TABLES `productwarehouse` WRITE;
/*!40000 ALTER TABLE `productwarehouse` DISABLE KEYS */;
/*!40000 ALTER TABLE `productwarehouse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `promotionlog`
--

DROP TABLE IF EXISTS `promotionlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `promotionlog` (
  `promotionLogID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `promotionType` varchar(50) DEFAULT NULL,
  `classesID` int(11) NOT NULL,
  `jumpClassID` int(11) NOT NULL,
  `schoolYearID` int(11) NOT NULL,
  `jumpSchoolYearID` int(11) NOT NULL,
  `subjectandsubjectcodeandmark` longtext,
  `exams` longtext,
  `markpercentages` longtext,
  `promoteStudents` longtext,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `create_userID` int(11) NOT NULL,
  PRIMARY KEY (`promotionLogID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `promotionlog`
--

LOCK TABLES `promotionlog` WRITE;
/*!40000 ALTER TABLE `promotionlog` DISABLE KEYS */;
/*!40000 ALTER TABLE `promotionlog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `purchase`
--

DROP TABLE IF EXISTS `purchase`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `purchase` (
  `purchaseID` int(11) NOT NULL AUTO_INCREMENT,
  `assetID` int(11) NOT NULL,
  `vendorID` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `unit` int(11) DEFAULT NULL,
  `purchase_date` date DEFAULT NULL,
  `service_date` date DEFAULT NULL,
  `purchase_price` double NOT NULL,
  `purchased_by` int(11) NOT NULL,
  `usertypeID` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `expire_date` date DEFAULT NULL,
  `create_date` date NOT NULL,
  `modify_date` date NOT NULL,
  `create_userID` int(11) NOT NULL,
  `create_usertypeID` int(11) NOT NULL,
  PRIMARY KEY (`purchaseID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `purchase`
--

LOCK TABLES `purchase` WRITE;
/*!40000 ALTER TABLE `purchase` DISABLE KEYS */;
/*!40000 ALTER TABLE `purchase` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question_answer`
--

DROP TABLE IF EXISTS `question_answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `question_answer` (
  `answerID` int(11) NOT NULL AUTO_INCREMENT,
  `questionID` int(11) NOT NULL,
  `optionID` int(11) DEFAULT NULL,
  `typeNumber` int(11) NOT NULL,
  `text` text,
  PRIMARY KEY (`answerID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question_answer`
--

LOCK TABLES `question_answer` WRITE;
/*!40000 ALTER TABLE `question_answer` DISABLE KEYS */;
/*!40000 ALTER TABLE `question_answer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question_bank`
--

DROP TABLE IF EXISTS `question_bank`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `question_bank` (
  `questionBankID` int(11) NOT NULL AUTO_INCREMENT,
  `question` text NOT NULL,
  `explanation` text,
  `levelID` int(11) DEFAULT NULL,
  `groupID` int(11) DEFAULT NULL,
  `totalQuestion` int(11) DEFAULT '0',
  `totalOption` int(11) DEFAULT NULL,
  `typeNumber` int(11) DEFAULT NULL,
  `parentID` int(11) DEFAULT '0',
  `time` int(11) DEFAULT '0',
  `mark` int(11) DEFAULT '0',
  `hints` text,
  `upload` varchar(512) DEFAULT NULL,
  `subjectID` int(11) DEFAULT NULL,
  `create_date` datetime NOT NULL,
  `modify_date` datetime NOT NULL,
  `create_userID` int(11) NOT NULL,
  `create_usertypeID` int(11) NOT NULL,
  PRIMARY KEY (`questionBankID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question_bank`
--

LOCK TABLES `question_bank` WRITE;
/*!40000 ALTER TABLE `question_bank` DISABLE KEYS */;
/*!40000 ALTER TABLE `question_bank` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question_group`
--

DROP TABLE IF EXISTS `question_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `question_group` (
  `questionGroupID` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  PRIMARY KEY (`questionGroupID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question_group`
--

LOCK TABLES `question_group` WRITE;
/*!40000 ALTER TABLE `question_group` DISABLE KEYS */;
INSERT INTO `question_group` VALUES (1,'Reasoning'),(2,'Computer Knowledge'),(3,'General'),(4,'Math'),(5,'GRE'),(6,'Sports');
/*!40000 ALTER TABLE `question_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question_level`
--

DROP TABLE IF EXISTS `question_level`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `question_level` (
  `questionLevelID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  PRIMARY KEY (`questionLevelID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question_level`
--

LOCK TABLES `question_level` WRITE;
/*!40000 ALTER TABLE `question_level` DISABLE KEYS */;
INSERT INTO `question_level` VALUES (1,'Easy'),(2,'Medium'),(3,'Hard');
/*!40000 ALTER TABLE `question_level` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question_option`
--

DROP TABLE IF EXISTS `question_option`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `question_option` (
  `optionID` int(11) NOT NULL AUTO_INCREMENT,
  `questionID` int(11) NOT NULL,
  `name` varchar(512) NOT NULL,
  `img` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`optionID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question_option`
--

LOCK TABLES `question_option` WRITE;
/*!40000 ALTER TABLE `question_option` DISABLE KEYS */;
/*!40000 ALTER TABLE `question_option` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question_type`
--

DROP TABLE IF EXISTS `question_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `question_type` (
  `questionTypeID` int(11) NOT NULL AUTO_INCREMENT,
  `typeNumber` int(11) NOT NULL,
  `name` varchar(512) NOT NULL,
  PRIMARY KEY (`questionTypeID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question_type`
--

LOCK TABLES `question_type` WRITE;
/*!40000 ALTER TABLE `question_type` DISABLE KEYS */;
INSERT INTO `question_type` VALUES (1,1,'Single Answer'),(2,2,'Multi Answer'),(3,3,'Fill in the blanks');
/*!40000 ALTER TABLE `question_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reset`
--

DROP TABLE IF EXISTS `reset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `reset` (
  `resetID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `keyID` varchar(128) NOT NULL,
  `email` varchar(60) NOT NULL,
  PRIMARY KEY (`resetID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reset`
--

LOCK TABLES `reset` WRITE;
/*!40000 ALTER TABLE `reset` DISABLE KEYS */;
/*!40000 ALTER TABLE `reset` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `routine`
--

DROP TABLE IF EXISTS `routine`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `routine` (
  `routineID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `classesID` int(11) NOT NULL,
  `sectionID` int(11) NOT NULL,
  `subjectID` int(11) NOT NULL,
  `schoolyearID` int(11) NOT NULL,
  `teacherID` int(11) NOT NULL,
  `day` varchar(60) NOT NULL,
  `start_time` varchar(10) NOT NULL,
  `end_time` varchar(10) NOT NULL,
  `room` tinytext NOT NULL,
  PRIMARY KEY (`routineID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `routine`
--

LOCK TABLES `routine` WRITE;
/*!40000 ALTER TABLE `routine` DISABLE KEYS */;
/*!40000 ALTER TABLE `routine` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `salary_option`
--

DROP TABLE IF EXISTS `salary_option`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `salary_option` (
  `salary_optionID` int(11) NOT NULL AUTO_INCREMENT,
  `salary_templateID` int(11) NOT NULL,
  `option_type` int(11) NOT NULL COMMENT 'Allowances =1, Dllowances = 2, Increment = 3',
  `label_name` varchar(128) DEFAULT NULL,
  `label_amount` double NOT NULL,
  PRIMARY KEY (`salary_optionID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `salary_option`
--

LOCK TABLES `salary_option` WRITE;
/*!40000 ALTER TABLE `salary_option` DISABLE KEYS */;
/*!40000 ALTER TABLE `salary_option` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `salary_template`
--

DROP TABLE IF EXISTS `salary_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `salary_template` (
  `salary_templateID` int(11) NOT NULL AUTO_INCREMENT,
  `salary_grades` varchar(128) NOT NULL,
  `basic_salary` text NOT NULL,
  `overtime_rate` text NOT NULL,
  PRIMARY KEY (`salary_templateID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `salary_template`
--

LOCK TABLES `salary_template` WRITE;
/*!40000 ALTER TABLE `salary_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `salary_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `school_sessions`
--

DROP TABLE IF EXISTS `school_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `school_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `school_sessions`
--

LOCK TABLES `school_sessions` WRITE;
/*!40000 ALTER TABLE `school_sessions` DISABLE KEYS */;
INSERT INTO `school_sessions` VALUES ('114kj72kdoaboku3s0heu2d1mteeih38','192.168.1.15',1575844507,_binary '__ci_last_regenerate|i:1575844507;'),('c04c57055aceaf079949e72462a42be7d41661c6','159.203.196.79',1575846376,_binary '__ci_last_regenerate|i:1575846376;'),('c0e6b84ec9ac348f63c6af15c517eb508a46d0f8','116.74.199.42',1575847213,_binary '__ci_last_regenerate|i:1575847213;english|N;loginuserID|s:1:\"1\";name|s:6:\"vishal\";email|s:26:\"support@blitzsoftwares.com\";usertypeID|s:1:\"1\";usertype|s:5:\"Admin\";username|s:6:\"vishal\";photo|s:11:\"default.png\";lang|s:7:\"english\";defaultschoolyearID|s:1:\"1\";varifyvaliduser|b:1;loggedin|b:1;get_permission|b:1;master_permission_set|a:372:{s:9:\"dashboard\";s:3:\"yes\";s:7:\"student\";s:3:\"yes\";s:11:\"student_add\";s:3:\"yes\";s:12:\"student_edit\";s:3:\"yes\";s:14:\"student_delete\";s:3:\"yes\";s:12:\"student_view\";s:3:\"yes\";s:7:\"parents\";s:3:\"yes\";s:11:\"parents_add\";s:3:\"yes\";s:12:\"parents_edit\";s:3:\"yes\";s:14:\"parents_delete\";s:3:\"yes\";s:12:\"parents_view\";s:3:\"yes\";s:7:\"teacher\";s:3:\"yes\";s:11:\"teacher_add\";s:3:\"yes\";s:12:\"teacher_edit\";s:3:\"yes\";s:14:\"teacher_delete\";s:3:\"yes\";s:12:\"teacher_view\";s:3:\"yes\";s:4:\"user\";s:3:\"yes\";s:8:\"user_add\";s:3:\"yes\";s:9:\"user_edit\";s:3:\"yes\";s:11:\"user_delete\";s:3:\"yes\";s:9:\"user_view\";s:3:\"yes\";s:7:\"classes\";s:3:\"yes\";s:11:\"classes_add\";s:3:\"yes\";s:12:\"classes_edit\";s:3:\"yes\";s:14:\"classes_delete\";s:3:\"yes\";s:7:\"section\";s:3:\"yes\";s:11:\"section_add\";s:3:\"yes\";s:12:\"section_edit\";s:3:\"yes\";s:15:\"semester_delete\";s:3:\"yes\";s:14:\"section_delete\";s:3:\"yes\";s:7:\"subject\";s:3:\"yes\";s:11:\"subject_add\";s:3:\"yes\";s:12:\"subject_edit\";s:3:\"yes\";s:14:\"subject_delete\";s:3:\"yes\";s:8:\"syllabus\";s:3:\"yes\";s:12:\"syllabus_add\";s:3:\"yes\";s:13:\"syllabus_edit\";s:3:\"yes\";s:15:\"syllabus_delete\";s:3:\"yes\";s:10:\"assignment\";s:3:\"yes\";s:14:\"assignment_add\";s:3:\"yes\";s:15:\"assignment_edit\";s:3:\"yes\";s:17:\"assignment_delete\";s:3:\"yes\";s:15:\"assignment_view\";s:3:\"yes\";s:7:\"routine\";s:3:\"yes\";s:11:\"routine_add\";s:3:\"yes\";s:12:\"routine_edit\";s:3:\"yes\";s:14:\"routine_delete\";s:3:\"yes\";s:11:\"sattendance\";s:3:\"yes\";s:15:\"sattendance_add\";s:3:\"yes\";s:16:\"sattendance_view\";s:3:\"yes\";s:11:\"tattendance\";s:3:\"yes\";s:15:\"tattendance_add\";s:3:\"yes\";s:16:\"tattendance_view\";s:3:\"yes\";s:11:\"uattendance\";s:3:\"yes\";s:15:\"uattendance_add\";s:3:\"yes\";s:16:\"uattendance_view\";s:3:\"yes\";s:4:\"exam\";s:3:\"yes\";s:8:\"exam_add\";s:3:\"yes\";s:9:\"exam_edit\";s:3:\"yes\";s:11:\"exam_delete\";s:3:\"yes\";s:12:\"examschedule\";s:3:\"yes\";s:16:\"examschedule_add\";s:3:\"yes\";s:17:\"examschedule_edit\";s:3:\"yes\";s:19:\"examschedule_delete\";s:3:\"yes\";s:5:\"grade\";s:3:\"yes\";s:9:\"grade_add\";s:3:\"yes\";s:10:\"grade_edit\";s:3:\"yes\";s:12:\"grade_delete\";s:3:\"yes\";s:11:\"eattendance\";s:3:\"yes\";s:15:\"eattendance_add\";s:3:\"yes\";s:4:\"mark\";s:3:\"yes\";s:8:\"mark_add\";s:3:\"yes\";s:9:\"mark_view\";s:3:\"yes\";s:14:\"markpercentage\";s:3:\"yes\";s:18:\"markpercentage_add\";s:3:\"yes\";s:19:\"markpercentage_edit\";s:3:\"yes\";s:21:\"markpercentage_delete\";s:3:\"yes\";s:9:\"promotion\";s:3:\"yes\";s:12:\"conversation\";s:3:\"yes\";s:5:\"media\";s:3:\"yes\";s:9:\"media_add\";s:3:\"yes\";s:12:\"media_delete\";s:3:\"yes\";s:10:\"mailandsms\";s:3:\"yes\";s:14:\"mailandsms_add\";s:3:\"yes\";s:15:\"mailandsms_view\";s:3:\"yes\";s:14:\"question_group\";s:3:\"yes\";s:18:\"question_group_add\";s:3:\"yes\";s:19:\"question_group_edit\";s:3:\"yes\";s:21:\"question_group_delete\";s:3:\"yes\";s:14:\"question_level\";s:3:\"yes\";s:18:\"question_level_add\";s:3:\"yes\";s:19:\"question_level_edit\";s:3:\"yes\";s:21:\"question_level_delete\";s:3:\"yes\";s:13:\"question_bank\";s:3:\"yes\";s:17:\"question_bank_add\";s:3:\"yes\";s:18:\"question_bank_edit\";s:3:\"yes\";s:20:\"question_bank_delete\";s:3:\"yes\";s:18:\"question_bank_view\";s:3:\"yes\";s:11:\"online_exam\";s:3:\"yes\";s:15:\"online_exam_add\";s:3:\"yes\";s:16:\"online_exam_edit\";s:3:\"yes\";s:18:\"online_exam_delete\";s:3:\"yes\";s:11:\"instruction\";s:3:\"yes\";s:15:\"instruction_add\";s:3:\"yes\";s:16:\"instruction_edit\";s:3:\"yes\";s:18:\"instruction_delete\";s:3:\"yes\";s:16:\"instruction_view\";s:3:\"yes\";s:15:\"salary_template\";s:3:\"yes\";s:19:\"salary_template_add\";s:3:\"yes\";s:20:\"salary_template_edit\";s:3:\"yes\";s:22:\"salary_template_delete\";s:3:\"yes\";s:20:\"salary_template_view\";s:3:\"yes\";s:15:\"hourly_template\";s:3:\"yes\";s:19:\"hourly_template_add\";s:3:\"yes\";s:20:\"hourly_template_edit\";s:3:\"yes\";s:22:\"hourly_template_delete\";s:3:\"yes\";s:13:\"manage_salary\";s:3:\"yes\";s:17:\"manage_salary_add\";s:3:\"yes\";s:18:\"manage_salary_edit\";s:3:\"yes\";s:20:\"manage_salary_delete\";s:3:\"yes\";s:18:\"manage_salary_view\";s:3:\"yes\";s:12:\"make_payment\";s:3:\"yes\";s:6:\"vendor\";s:3:\"yes\";s:10:\"vendor_add\";s:3:\"yes\";s:11:\"vendor_edit\";s:3:\"yes\";s:13:\"vendor_delete\";s:3:\"yes\";s:8:\"location\";s:3:\"yes\";s:12:\"location_add\";s:3:\"yes\";s:13:\"location_edit\";s:3:\"yes\";s:15:\"location_delete\";s:3:\"yes\";s:14:\"asset_category\";s:3:\"yes\";s:18:\"asset_category_add\";s:3:\"yes\";s:19:\"asset_category_edit\";s:3:\"yes\";s:21:\"asset_category_delete\";s:3:\"yes\";s:5:\"asset\";s:3:\"yes\";s:9:\"asset_add\";s:3:\"yes\";s:10:\"asset_edit\";s:3:\"yes\";s:12:\"asset_delete\";s:3:\"yes\";s:10:\"asset_view\";s:3:\"yes\";s:16:\"asset_assignment\";s:3:\"yes\";s:20:\"asset_assignment_add\";s:3:\"yes\";s:21:\"asset_assignment_edit\";s:3:\"yes\";s:23:\"asset_assignment_delete\";s:3:\"yes\";s:21:\"asset_assignment_view\";s:3:\"yes\";s:8:\"purchase\";s:3:\"yes\";s:12:\"purchase_add\";s:3:\"yes\";s:13:\"purchase_edit\";s:3:\"yes\";s:15:\"purchase_delete\";s:3:\"yes\";s:15:\"productcategory\";s:3:\"yes\";s:19:\"productcategory_add\";s:3:\"yes\";s:20:\"productcategory_edit\";s:3:\"yes\";s:22:\"productcategory_delete\";s:3:\"yes\";s:7:\"product\";s:3:\"yes\";s:11:\"product_add\";s:3:\"yes\";s:12:\"product_edit\";s:3:\"yes\";s:14:\"product_delete\";s:3:\"yes\";s:16:\"productwarehouse\";s:3:\"yes\";s:20:\"productwarehouse_add\";s:3:\"yes\";s:21:\"productwarehouse_edit\";s:3:\"yes\";s:23:\"productwarehouse_delete\";s:3:\"yes\";s:15:\"productsupplier\";s:3:\"yes\";s:19:\"productsupplier_add\";s:3:\"yes\";s:20:\"productsupplier_edit\";s:3:\"yes\";s:22:\"productsupplier_delete\";s:3:\"yes\";s:15:\"productpurchase\";s:3:\"yes\";s:19:\"productpurchase_add\";s:3:\"yes\";s:20:\"productpurchase_edit\";s:3:\"yes\";s:22:\"productpurchase_delete\";s:3:\"yes\";s:20:\"productpurchase_view\";s:3:\"yes\";s:11:\"productsale\";s:3:\"yes\";s:15:\"productsale_add\";s:3:\"yes\";s:16:\"productsale_edit\";s:3:\"yes\";s:18:\"productsale_delete\";s:3:\"yes\";s:16:\"productsale_view\";s:3:\"yes\";s:13:\"leavecategory\";s:3:\"yes\";s:17:\"leavecategory_add\";s:3:\"yes\";s:18:\"leavecategory_edit\";s:3:\"yes\";s:20:\"leavecategory_delete\";s:3:\"yes\";s:11:\"leaveassign\";s:3:\"yes\";s:15:\"leaveassign_add\";s:3:\"yes\";s:16:\"leaveassign_edit\";s:3:\"yes\";s:18:\"leaveassign_delete\";s:3:\"yes\";s:10:\"leaveapply\";s:3:\"yes\";s:14:\"leaveapply_add\";s:3:\"yes\";s:15:\"leaveapply_edit\";s:3:\"yes\";s:17:\"leaveapply_delete\";s:3:\"yes\";s:15:\"leaveapply_view\";s:3:\"yes\";s:16:\"leaveapplication\";s:3:\"yes\";s:18:\"activitiescategory\";s:3:\"yes\";s:22:\"activitiescategory_add\";s:3:\"yes\";s:23:\"activitiescategory_edit\";s:3:\"yes\";s:25:\"activitiescategory_delete\";s:3:\"yes\";s:10:\"activities\";s:3:\"yes\";s:14:\"activities_add\";s:3:\"yes\";s:17:\"activities_delete\";s:3:\"yes\";s:9:\"childcare\";s:3:\"yes\";s:13:\"childcare_add\";s:3:\"yes\";s:14:\"childcare_edit\";s:3:\"yes\";s:16:\"childcare_delete\";s:3:\"yes\";s:7:\"lmember\";s:3:\"yes\";s:11:\"lmember_add\";s:3:\"yes\";s:12:\"lmember_edit\";s:3:\"yes\";s:14:\"lmember_delete\";s:3:\"yes\";s:12:\"lmember_view\";s:3:\"yes\";s:4:\"book\";s:3:\"yes\";s:8:\"book_add\";s:3:\"yes\";s:9:\"book_edit\";s:3:\"yes\";s:11:\"book_delete\";s:3:\"yes\";s:5:\"issue\";s:3:\"yes\";s:9:\"issue_add\";s:3:\"yes\";s:10:\"issue_edit\";s:3:\"yes\";s:10:\"issue_view\";s:3:\"yes\";s:6:\"ebooks\";s:3:\"yes\";s:10:\"ebooks_add\";s:3:\"yes\";s:11:\"ebooks_edit\";s:3:\"yes\";s:13:\"ebooks_delete\";s:3:\"yes\";s:11:\"ebooks_view\";s:3:\"yes\";s:9:\"transport\";s:3:\"yes\";s:13:\"transport_add\";s:3:\"yes\";s:14:\"transport_edit\";s:3:\"yes\";s:16:\"transport_delete\";s:3:\"yes\";s:7:\"tmember\";s:3:\"yes\";s:11:\"tmember_add\";s:3:\"yes\";s:12:\"tmember_edit\";s:3:\"yes\";s:14:\"tmember_delete\";s:3:\"yes\";s:12:\"tmember_view\";s:3:\"yes\";s:6:\"hostel\";s:3:\"yes\";s:10:\"hostel_add\";s:3:\"yes\";s:11:\"hostel_edit\";s:3:\"yes\";s:13:\"hostel_delete\";s:3:\"yes\";s:8:\"category\";s:3:\"yes\";s:12:\"category_add\";s:3:\"yes\";s:13:\"category_edit\";s:3:\"yes\";s:15:\"category_delete\";s:3:\"yes\";s:7:\"hmember\";s:3:\"yes\";s:11:\"hmember_add\";s:3:\"yes\";s:12:\"hmember_edit\";s:3:\"yes\";s:14:\"hmember_delete\";s:3:\"yes\";s:12:\"hmember_view\";s:3:\"yes\";s:8:\"feetypes\";s:3:\"yes\";s:12:\"feetypes_add\";s:3:\"yes\";s:13:\"feetypes_edit\";s:3:\"yes\";s:15:\"feetypes_delete\";s:3:\"yes\";s:7:\"invoice\";s:3:\"yes\";s:11:\"invoice_add\";s:3:\"yes\";s:12:\"invoice_edit\";s:3:\"yes\";s:14:\"invoice_delete\";s:3:\"yes\";s:12:\"invoice_view\";s:3:\"yes\";s:14:\"paymenthistory\";s:3:\"yes\";s:19:\"paymenthistory_edit\";s:3:\"yes\";s:21:\"paymenthistory_delete\";s:3:\"yes\";s:7:\"expense\";s:3:\"yes\";s:11:\"expense_add\";s:3:\"yes\";s:12:\"expense_edit\";s:3:\"yes\";s:14:\"expense_delete\";s:3:\"yes\";s:6:\"income\";s:3:\"yes\";s:10:\"income_add\";s:3:\"yes\";s:11:\"income_edit\";s:3:\"yes\";s:13:\"income_delete\";s:3:\"yes\";s:14:\"global_payment\";s:3:\"yes\";s:6:\"notice\";s:3:\"yes\";s:10:\"notice_add\";s:3:\"yes\";s:11:\"notice_edit\";s:3:\"yes\";s:13:\"notice_delete\";s:3:\"yes\";s:11:\"notice_view\";s:3:\"yes\";s:5:\"event\";s:3:\"yes\";s:9:\"event_add\";s:3:\"yes\";s:10:\"event_edit\";s:3:\"yes\";s:12:\"event_delete\";s:3:\"yes\";s:10:\"event_view\";s:3:\"yes\";s:7:\"holiday\";s:3:\"yes\";s:11:\"holiday_add\";s:3:\"yes\";s:12:\"holiday_edit\";s:3:\"yes\";s:14:\"holiday_delete\";s:3:\"yes\";s:12:\"holiday_view\";s:3:\"yes\";s:13:\"classesreport\";s:3:\"yes\";s:13:\"studentreport\";s:3:\"yes\";s:12:\"idcardreport\";s:3:\"yes\";s:15:\"admitcardreport\";s:3:\"yes\";s:13:\"routinereport\";s:3:\"yes\";s:18:\"examschedulereport\";s:3:\"yes\";s:16:\"attendancereport\";s:3:\"yes\";s:24:\"attendanceoverviewreport\";s:3:\"yes\";s:18:\"librarybooksreport\";s:3:\"yes\";s:17:\"librarycardreport\";s:3:\"yes\";s:22:\"librarybookissuereport\";s:3:\"yes\";s:14:\"terminalreport\";s:3:\"yes\";s:16:\"meritstagereport\";s:3:\"yes\";s:21:\"tabulationsheetreport\";s:3:\"yes\";s:15:\"marksheetreport\";s:3:\"yes\";s:18:\"progresscardreport\";s:3:\"yes\";s:20:\"studentsessionreport\";s:3:\"yes\";s:16:\"onlineexamreport\";s:3:\"yes\";s:24:\"onlineexamquestionreport\";s:3:\"yes\";s:21:\"onlineadmissionreport\";s:3:\"yes\";s:17:\"certificatereport\";s:3:\"yes\";s:22:\"leaveapplicationreport\";s:3:\"yes\";s:21:\"productpurchasereport\";s:3:\"yes\";s:17:\"productsalereport\";s:3:\"yes\";s:23:\"searchpaymentfeesreport\";s:3:\"yes\";s:10:\"feesreport\";s:3:\"yes\";s:13:\"duefeesreport\";s:3:\"yes\";s:17:\"balancefeesreport\";s:3:\"yes\";s:17:\"transactionreport\";s:3:\"yes\";s:17:\"studentfinereport\";s:3:\"yes\";s:12:\"salaryreport\";s:3:\"yes\";s:19:\"accountledgerreport\";s:3:\"yes\";s:15:\"onlineadmission\";s:3:\"yes\";s:11:\"visitorinfo\";s:3:\"yes\";s:18:\"visitorinfo_delete\";s:3:\"yes\";s:16:\"visitorinfo_view\";s:3:\"yes\";s:10:\"schoolyear\";s:3:\"yes\";s:14:\"schoolyear_add\";s:3:\"yes\";s:15:\"schoolyear_edit\";s:3:\"yes\";s:17:\"schoolyear_delete\";s:3:\"yes\";s:12:\"studentgroup\";s:3:\"yes\";s:16:\"studentgroup_add\";s:3:\"yes\";s:17:\"studentgroup_edit\";s:3:\"yes\";s:19:\"studentgroup_delete\";s:3:\"yes\";s:8:\"complain\";s:3:\"yes\";s:12:\"complain_add\";s:3:\"yes\";s:13:\"complain_edit\";s:3:\"yes\";s:15:\"complain_delete\";s:3:\"yes\";s:13:\"complain_view\";s:3:\"yes\";s:20:\"certificate_template\";s:3:\"yes\";s:24:\"certificate_template_add\";s:3:\"yes\";s:25:\"certificate_template_edit\";s:3:\"yes\";s:27:\"certificate_template_delete\";s:3:\"yes\";s:25:\"certificate_template_view\";s:3:\"yes\";s:11:\"systemadmin\";s:3:\"yes\";s:15:\"systemadmin_add\";s:3:\"yes\";s:16:\"systemadmin_edit\";s:3:\"yes\";s:18:\"systemadmin_delete\";s:3:\"yes\";s:16:\"systemadmin_view\";s:3:\"yes\";s:13:\"resetpassword\";s:3:\"yes\";s:10:\"sociallink\";s:3:\"yes\";s:14:\"sociallink_add\";s:3:\"yes\";s:15:\"sociallink_edit\";s:3:\"yes\";s:17:\"sociallink_delete\";s:3:\"yes\";s:18:\"mailandsmstemplate\";s:3:\"yes\";s:22:\"mailandsmstemplate_add\";s:3:\"yes\";s:23:\"mailandsmstemplate_edit\";s:3:\"yes\";s:25:\"mailandsmstemplate_delete\";s:3:\"yes\";s:23:\"mailandsmstemplate_view\";s:3:\"yes\";s:10:\"bulkimport\";s:3:\"yes\";s:6:\"backup\";s:3:\"yes\";s:8:\"usertype\";s:3:\"yes\";s:12:\"usertype_add\";s:3:\"yes\";s:13:\"usertype_edit\";s:3:\"yes\";s:15:\"usertype_delete\";s:3:\"yes\";s:10:\"permission\";s:3:\"yes\";s:6:\"update\";s:3:\"yes\";s:16:\"posts_categories\";s:3:\"yes\";s:20:\"posts_categories_add\";s:3:\"yes\";s:21:\"posts_categories_edit\";s:3:\"yes\";s:23:\"posts_categories_delete\";s:3:\"yes\";s:5:\"posts\";s:3:\"yes\";s:9:\"posts_add\";s:3:\"yes\";s:10:\"posts_edit\";s:3:\"yes\";s:12:\"posts_delete\";s:3:\"yes\";s:5:\"pages\";s:3:\"yes\";s:9:\"pages_add\";s:3:\"yes\";s:10:\"pages_edit\";s:3:\"yes\";s:12:\"pages_delete\";s:3:\"yes\";s:12:\"frontendmenu\";s:3:\"yes\";s:7:\"setting\";s:3:\"yes\";s:16:\"frontend_setting\";s:3:\"yes\";s:15:\"paymentsettings\";s:3:\"yes\";s:11:\"smssettings\";s:3:\"yes\";s:12:\"emailsetting\";s:3:\"yes\";s:11:\"marksetting\";s:3:\"yes\";s:9:\"take_exam\";s:3:\"yes\";}'),('0457c9f2408d57dc8ea67d9ccacd3fbab914f376','159.203.196.79',1575846393,_binary '__ci_last_regenerate|i:1575846393;'),('232bf7bc53f1ccdb225cd3a7c289286c1c07747b','162.243.69.215',1575846400,_binary '__ci_last_regenerate|i:1575846400;'),('4e317bb86137c5eec27ebeb6b54c8f0407a2c2cb','159.203.42.143',1575846453,_binary '__ci_last_regenerate|i:1575846453;'),('c913e096633bd8e9fea657741e65a50a33d2e0d0','159.203.196.79',1575846456,_binary '__ci_last_regenerate|i:1575846456;'),('8f8f17908fab117cd688bffd540851862f53a761','138.197.202.197',1575846576,_binary '__ci_last_regenerate|i:1575846576;'),('a22ab5a9a81b653da87fe03a132f2c31c4c7f412','107.170.96.6',1575847214,_binary '__ci_last_regenerate|i:1575847214;'),('36c75c2fe4708d55a10cd701e27bfbb3b743752c','116.74.199.42',1575847532,_binary '__ci_last_regenerate|i:1575847532;english|N;loginuserID|s:1:\"1\";name|s:6:\"vishal\";email|s:26:\"support@blitzsoftwares.com\";usertypeID|s:1:\"1\";usertype|s:5:\"Admin\";username|s:6:\"vishal\";photo|s:11:\"default.png\";lang|s:7:\"english\";defaultschoolyearID|s:1:\"1\";varifyvaliduser|b:1;loggedin|b:1;get_permission|b:1;master_permission_set|a:372:{s:9:\"dashboard\";s:3:\"yes\";s:7:\"student\";s:3:\"yes\";s:11:\"student_add\";s:3:\"yes\";s:12:\"student_edit\";s:3:\"yes\";s:14:\"student_delete\";s:3:\"yes\";s:12:\"student_view\";s:3:\"yes\";s:7:\"parents\";s:3:\"yes\";s:11:\"parents_add\";s:3:\"yes\";s:12:\"parents_edit\";s:3:\"yes\";s:14:\"parents_delete\";s:3:\"yes\";s:12:\"parents_view\";s:3:\"yes\";s:7:\"teacher\";s:3:\"yes\";s:11:\"teacher_add\";s:3:\"yes\";s:12:\"teacher_edit\";s:3:\"yes\";s:14:\"teacher_delete\";s:3:\"yes\";s:12:\"teacher_view\";s:3:\"yes\";s:4:\"user\";s:3:\"yes\";s:8:\"user_add\";s:3:\"yes\";s:9:\"user_edit\";s:3:\"yes\";s:11:\"user_delete\";s:3:\"yes\";s:9:\"user_view\";s:3:\"yes\";s:7:\"classes\";s:3:\"yes\";s:11:\"classes_add\";s:3:\"yes\";s:12:\"classes_edit\";s:3:\"yes\";s:14:\"classes_delete\";s:3:\"yes\";s:7:\"section\";s:3:\"yes\";s:11:\"section_add\";s:3:\"yes\";s:12:\"section_edit\";s:3:\"yes\";s:15:\"semester_delete\";s:3:\"yes\";s:14:\"section_delete\";s:3:\"yes\";s:7:\"subject\";s:3:\"yes\";s:11:\"subject_add\";s:3:\"yes\";s:12:\"subject_edit\";s:3:\"yes\";s:14:\"subject_delete\";s:3:\"yes\";s:8:\"syllabus\";s:3:\"yes\";s:12:\"syllabus_add\";s:3:\"yes\";s:13:\"syllabus_edit\";s:3:\"yes\";s:15:\"syllabus_delete\";s:3:\"yes\";s:10:\"assignment\";s:3:\"yes\";s:14:\"assignment_add\";s:3:\"yes\";s:15:\"assignment_edit\";s:3:\"yes\";s:17:\"assignment_delete\";s:3:\"yes\";s:15:\"assignment_view\";s:3:\"yes\";s:7:\"routine\";s:3:\"yes\";s:11:\"routine_add\";s:3:\"yes\";s:12:\"routine_edit\";s:3:\"yes\";s:14:\"routine_delete\";s:3:\"yes\";s:11:\"sattendance\";s:3:\"yes\";s:15:\"sattendance_add\";s:3:\"yes\";s:16:\"sattendance_view\";s:3:\"yes\";s:11:\"tattendance\";s:3:\"yes\";s:15:\"tattendance_add\";s:3:\"yes\";s:16:\"tattendance_view\";s:3:\"yes\";s:11:\"uattendance\";s:3:\"yes\";s:15:\"uattendance_add\";s:3:\"yes\";s:16:\"uattendance_view\";s:3:\"yes\";s:4:\"exam\";s:3:\"yes\";s:8:\"exam_add\";s:3:\"yes\";s:9:\"exam_edit\";s:3:\"yes\";s:11:\"exam_delete\";s:3:\"yes\";s:12:\"examschedule\";s:3:\"yes\";s:16:\"examschedule_add\";s:3:\"yes\";s:17:\"examschedule_edit\";s:3:\"yes\";s:19:\"examschedule_delete\";s:3:\"yes\";s:5:\"grade\";s:3:\"yes\";s:9:\"grade_add\";s:3:\"yes\";s:10:\"grade_edit\";s:3:\"yes\";s:12:\"grade_delete\";s:3:\"yes\";s:11:\"eattendance\";s:3:\"yes\";s:15:\"eattendance_add\";s:3:\"yes\";s:4:\"mark\";s:3:\"yes\";s:8:\"mark_add\";s:3:\"yes\";s:9:\"mark_view\";s:3:\"yes\";s:14:\"markpercentage\";s:3:\"yes\";s:18:\"markpercentage_add\";s:3:\"yes\";s:19:\"markpercentage_edit\";s:3:\"yes\";s:21:\"markpercentage_delete\";s:3:\"yes\";s:9:\"promotion\";s:3:\"yes\";s:12:\"conversation\";s:3:\"yes\";s:5:\"media\";s:3:\"yes\";s:9:\"media_add\";s:3:\"yes\";s:12:\"media_delete\";s:3:\"yes\";s:10:\"mailandsms\";s:3:\"yes\";s:14:\"mailandsms_add\";s:3:\"yes\";s:15:\"mailandsms_view\";s:3:\"yes\";s:14:\"question_group\";s:3:\"yes\";s:18:\"question_group_add\";s:3:\"yes\";s:19:\"question_group_edit\";s:3:\"yes\";s:21:\"question_group_delete\";s:3:\"yes\";s:14:\"question_level\";s:3:\"yes\";s:18:\"question_level_add\";s:3:\"yes\";s:19:\"question_level_edit\";s:3:\"yes\";s:21:\"question_level_delete\";s:3:\"yes\";s:13:\"question_bank\";s:3:\"yes\";s:17:\"question_bank_add\";s:3:\"yes\";s:18:\"question_bank_edit\";s:3:\"yes\";s:20:\"question_bank_delete\";s:3:\"yes\";s:18:\"question_bank_view\";s:3:\"yes\";s:11:\"online_exam\";s:3:\"yes\";s:15:\"online_exam_add\";s:3:\"yes\";s:16:\"online_exam_edit\";s:3:\"yes\";s:18:\"online_exam_delete\";s:3:\"yes\";s:11:\"instruction\";s:3:\"yes\";s:15:\"instruction_add\";s:3:\"yes\";s:16:\"instruction_edit\";s:3:\"yes\";s:18:\"instruction_delete\";s:3:\"yes\";s:16:\"instruction_view\";s:3:\"yes\";s:15:\"salary_template\";s:3:\"yes\";s:19:\"salary_template_add\";s:3:\"yes\";s:20:\"salary_template_edit\";s:3:\"yes\";s:22:\"salary_template_delete\";s:3:\"yes\";s:20:\"salary_template_view\";s:3:\"yes\";s:15:\"hourly_template\";s:3:\"yes\";s:19:\"hourly_template_add\";s:3:\"yes\";s:20:\"hourly_template_edit\";s:3:\"yes\";s:22:\"hourly_template_delete\";s:3:\"yes\";s:13:\"manage_salary\";s:3:\"yes\";s:17:\"manage_salary_add\";s:3:\"yes\";s:18:\"manage_salary_edit\";s:3:\"yes\";s:20:\"manage_salary_delete\";s:3:\"yes\";s:18:\"manage_salary_view\";s:3:\"yes\";s:12:\"make_payment\";s:3:\"yes\";s:6:\"vendor\";s:3:\"yes\";s:10:\"vendor_add\";s:3:\"yes\";s:11:\"vendor_edit\";s:3:\"yes\";s:13:\"vendor_delete\";s:3:\"yes\";s:8:\"location\";s:3:\"yes\";s:12:\"location_add\";s:3:\"yes\";s:13:\"location_edit\";s:3:\"yes\";s:15:\"location_delete\";s:3:\"yes\";s:14:\"asset_category\";s:3:\"yes\";s:18:\"asset_category_add\";s:3:\"yes\";s:19:\"asset_category_edit\";s:3:\"yes\";s:21:\"asset_category_delete\";s:3:\"yes\";s:5:\"asset\";s:3:\"yes\";s:9:\"asset_add\";s:3:\"yes\";s:10:\"asset_edit\";s:3:\"yes\";s:12:\"asset_delete\";s:3:\"yes\";s:10:\"asset_view\";s:3:\"yes\";s:16:\"asset_assignment\";s:3:\"yes\";s:20:\"asset_assignment_add\";s:3:\"yes\";s:21:\"asset_assignment_edit\";s:3:\"yes\";s:23:\"asset_assignment_delete\";s:3:\"yes\";s:21:\"asset_assignment_view\";s:3:\"yes\";s:8:\"purchase\";s:3:\"yes\";s:12:\"purchase_add\";s:3:\"yes\";s:13:\"purchase_edit\";s:3:\"yes\";s:15:\"purchase_delete\";s:3:\"yes\";s:15:\"productcategory\";s:3:\"yes\";s:19:\"productcategory_add\";s:3:\"yes\";s:20:\"productcategory_edit\";s:3:\"yes\";s:22:\"productcategory_delete\";s:3:\"yes\";s:7:\"product\";s:3:\"yes\";s:11:\"product_add\";s:3:\"yes\";s:12:\"product_edit\";s:3:\"yes\";s:14:\"product_delete\";s:3:\"yes\";s:16:\"productwarehouse\";s:3:\"yes\";s:20:\"productwarehouse_add\";s:3:\"yes\";s:21:\"productwarehouse_edit\";s:3:\"yes\";s:23:\"productwarehouse_delete\";s:3:\"yes\";s:15:\"productsupplier\";s:3:\"yes\";s:19:\"productsupplier_add\";s:3:\"yes\";s:20:\"productsupplier_edit\";s:3:\"yes\";s:22:\"productsupplier_delete\";s:3:\"yes\";s:15:\"productpurchase\";s:3:\"yes\";s:19:\"productpurchase_add\";s:3:\"yes\";s:20:\"productpurchase_edit\";s:3:\"yes\";s:22:\"productpurchase_delete\";s:3:\"yes\";s:20:\"productpurchase_view\";s:3:\"yes\";s:11:\"productsale\";s:3:\"yes\";s:15:\"productsale_add\";s:3:\"yes\";s:16:\"productsale_edit\";s:3:\"yes\";s:18:\"productsale_delete\";s:3:\"yes\";s:16:\"productsale_view\";s:3:\"yes\";s:13:\"leavecategory\";s:3:\"yes\";s:17:\"leavecategory_add\";s:3:\"yes\";s:18:\"leavecategory_edit\";s:3:\"yes\";s:20:\"leavecategory_delete\";s:3:\"yes\";s:11:\"leaveassign\";s:3:\"yes\";s:15:\"leaveassign_add\";s:3:\"yes\";s:16:\"leaveassign_edit\";s:3:\"yes\";s:18:\"leaveassign_delete\";s:3:\"yes\";s:10:\"leaveapply\";s:3:\"yes\";s:14:\"leaveapply_add\";s:3:\"yes\";s:15:\"leaveapply_edit\";s:3:\"yes\";s:17:\"leaveapply_delete\";s:3:\"yes\";s:15:\"leaveapply_view\";s:3:\"yes\";s:16:\"leaveapplication\";s:3:\"yes\";s:18:\"activitiescategory\";s:3:\"yes\";s:22:\"activitiescategory_add\";s:3:\"yes\";s:23:\"activitiescategory_edit\";s:3:\"yes\";s:25:\"activitiescategory_delete\";s:3:\"yes\";s:10:\"activities\";s:3:\"yes\";s:14:\"activities_add\";s:3:\"yes\";s:17:\"activities_delete\";s:3:\"yes\";s:9:\"childcare\";s:3:\"yes\";s:13:\"childcare_add\";s:3:\"yes\";s:14:\"childcare_edit\";s:3:\"yes\";s:16:\"childcare_delete\";s:3:\"yes\";s:7:\"lmember\";s:3:\"yes\";s:11:\"lmember_add\";s:3:\"yes\";s:12:\"lmember_edit\";s:3:\"yes\";s:14:\"lmember_delete\";s:3:\"yes\";s:12:\"lmember_view\";s:3:\"yes\";s:4:\"book\";s:3:\"yes\";s:8:\"book_add\";s:3:\"yes\";s:9:\"book_edit\";s:3:\"yes\";s:11:\"book_delete\";s:3:\"yes\";s:5:\"issue\";s:3:\"yes\";s:9:\"issue_add\";s:3:\"yes\";s:10:\"issue_edit\";s:3:\"yes\";s:10:\"issue_view\";s:3:\"yes\";s:6:\"ebooks\";s:3:\"yes\";s:10:\"ebooks_add\";s:3:\"yes\";s:11:\"ebooks_edit\";s:3:\"yes\";s:13:\"ebooks_delete\";s:3:\"yes\";s:11:\"ebooks_view\";s:3:\"yes\";s:9:\"transport\";s:3:\"yes\";s:13:\"transport_add\";s:3:\"yes\";s:14:\"transport_edit\";s:3:\"yes\";s:16:\"transport_delete\";s:3:\"yes\";s:7:\"tmember\";s:3:\"yes\";s:11:\"tmember_add\";s:3:\"yes\";s:12:\"tmember_edit\";s:3:\"yes\";s:14:\"tmember_delete\";s:3:\"yes\";s:12:\"tmember_view\";s:3:\"yes\";s:6:\"hostel\";s:3:\"yes\";s:10:\"hostel_add\";s:3:\"yes\";s:11:\"hostel_edit\";s:3:\"yes\";s:13:\"hostel_delete\";s:3:\"yes\";s:8:\"category\";s:3:\"yes\";s:12:\"category_add\";s:3:\"yes\";s:13:\"category_edit\";s:3:\"yes\";s:15:\"category_delete\";s:3:\"yes\";s:7:\"hmember\";s:3:\"yes\";s:11:\"hmember_add\";s:3:\"yes\";s:12:\"hmember_edit\";s:3:\"yes\";s:14:\"hmember_delete\";s:3:\"yes\";s:12:\"hmember_view\";s:3:\"yes\";s:8:\"feetypes\";s:3:\"yes\";s:12:\"feetypes_add\";s:3:\"yes\";s:13:\"feetypes_edit\";s:3:\"yes\";s:15:\"feetypes_delete\";s:3:\"yes\";s:7:\"invoice\";s:3:\"yes\";s:11:\"invoice_add\";s:3:\"yes\";s:12:\"invoice_edit\";s:3:\"yes\";s:14:\"invoice_delete\";s:3:\"yes\";s:12:\"invoice_view\";s:3:\"yes\";s:14:\"paymenthistory\";s:3:\"yes\";s:19:\"paymenthistory_edit\";s:3:\"yes\";s:21:\"paymenthistory_delete\";s:3:\"yes\";s:7:\"expense\";s:3:\"yes\";s:11:\"expense_add\";s:3:\"yes\";s:12:\"expense_edit\";s:3:\"yes\";s:14:\"expense_delete\";s:3:\"yes\";s:6:\"income\";s:3:\"yes\";s:10:\"income_add\";s:3:\"yes\";s:11:\"income_edit\";s:3:\"yes\";s:13:\"income_delete\";s:3:\"yes\";s:14:\"global_payment\";s:3:\"yes\";s:6:\"notice\";s:3:\"yes\";s:10:\"notice_add\";s:3:\"yes\";s:11:\"notice_edit\";s:3:\"yes\";s:13:\"notice_delete\";s:3:\"yes\";s:11:\"notice_view\";s:3:\"yes\";s:5:\"event\";s:3:\"yes\";s:9:\"event_add\";s:3:\"yes\";s:10:\"event_edit\";s:3:\"yes\";s:12:\"event_delete\";s:3:\"yes\";s:10:\"event_view\";s:3:\"yes\";s:7:\"holiday\";s:3:\"yes\";s:11:\"holiday_add\";s:3:\"yes\";s:12:\"holiday_edit\";s:3:\"yes\";s:14:\"holiday_delete\";s:3:\"yes\";s:12:\"holiday_view\";s:3:\"yes\";s:13:\"classesreport\";s:3:\"yes\";s:13:\"studentreport\";s:3:\"yes\";s:12:\"idcardreport\";s:3:\"yes\";s:15:\"admitcardreport\";s:3:\"yes\";s:13:\"routinereport\";s:3:\"yes\";s:18:\"examschedulereport\";s:3:\"yes\";s:16:\"attendancereport\";s:3:\"yes\";s:24:\"attendanceoverviewreport\";s:3:\"yes\";s:18:\"librarybooksreport\";s:3:\"yes\";s:17:\"librarycardreport\";s:3:\"yes\";s:22:\"librarybookissuereport\";s:3:\"yes\";s:14:\"terminalreport\";s:3:\"yes\";s:16:\"meritstagereport\";s:3:\"yes\";s:21:\"tabulationsheetreport\";s:3:\"yes\";s:15:\"marksheetreport\";s:3:\"yes\";s:18:\"progresscardreport\";s:3:\"yes\";s:20:\"studentsessionreport\";s:3:\"yes\";s:16:\"onlineexamreport\";s:3:\"yes\";s:24:\"onlineexamquestionreport\";s:3:\"yes\";s:21:\"onlineadmissionreport\";s:3:\"yes\";s:17:\"certificatereport\";s:3:\"yes\";s:22:\"leaveapplicationreport\";s:3:\"yes\";s:21:\"productpurchasereport\";s:3:\"yes\";s:17:\"productsalereport\";s:3:\"yes\";s:23:\"searchpaymentfeesreport\";s:3:\"yes\";s:10:\"feesreport\";s:3:\"yes\";s:13:\"duefeesreport\";s:3:\"yes\";s:17:\"balancefeesreport\";s:3:\"yes\";s:17:\"transactionreport\";s:3:\"yes\";s:17:\"studentfinereport\";s:3:\"yes\";s:12:\"salaryreport\";s:3:\"yes\";s:19:\"accountledgerreport\";s:3:\"yes\";s:15:\"onlineadmission\";s:3:\"yes\";s:11:\"visitorinfo\";s:3:\"yes\";s:18:\"visitorinfo_delete\";s:3:\"yes\";s:16:\"visitorinfo_view\";s:3:\"yes\";s:10:\"schoolyear\";s:3:\"yes\";s:14:\"schoolyear_add\";s:3:\"yes\";s:15:\"schoolyear_edit\";s:3:\"yes\";s:17:\"schoolyear_delete\";s:3:\"yes\";s:12:\"studentgroup\";s:3:\"yes\";s:16:\"studentgroup_add\";s:3:\"yes\";s:17:\"studentgroup_edit\";s:3:\"yes\";s:19:\"studentgroup_delete\";s:3:\"yes\";s:8:\"complain\";s:3:\"yes\";s:12:\"complain_add\";s:3:\"yes\";s:13:\"complain_edit\";s:3:\"yes\";s:15:\"complain_delete\";s:3:\"yes\";s:13:\"complain_view\";s:3:\"yes\";s:20:\"certificate_template\";s:3:\"yes\";s:24:\"certificate_template_add\";s:3:\"yes\";s:25:\"certificate_template_edit\";s:3:\"yes\";s:27:\"certificate_template_delete\";s:3:\"yes\";s:25:\"certificate_template_view\";s:3:\"yes\";s:11:\"systemadmin\";s:3:\"yes\";s:15:\"systemadmin_add\";s:3:\"yes\";s:16:\"systemadmin_edit\";s:3:\"yes\";s:18:\"systemadmin_delete\";s:3:\"yes\";s:16:\"systemadmin_view\";s:3:\"yes\";s:13:\"resetpassword\";s:3:\"yes\";s:10:\"sociallink\";s:3:\"yes\";s:14:\"sociallink_add\";s:3:\"yes\";s:15:\"sociallink_edit\";s:3:\"yes\";s:17:\"sociallink_delete\";s:3:\"yes\";s:18:\"mailandsmstemplate\";s:3:\"yes\";s:22:\"mailandsmstemplate_add\";s:3:\"yes\";s:23:\"mailandsmstemplate_edit\";s:3:\"yes\";s:25:\"mailandsmstemplate_delete\";s:3:\"yes\";s:23:\"mailandsmstemplate_view\";s:3:\"yes\";s:10:\"bulkimport\";s:3:\"yes\";s:6:\"backup\";s:3:\"yes\";s:8:\"usertype\";s:3:\"yes\";s:12:\"usertype_add\";s:3:\"yes\";s:13:\"usertype_edit\";s:3:\"yes\";s:15:\"usertype_delete\";s:3:\"yes\";s:10:\"permission\";s:3:\"yes\";s:6:\"update\";s:3:\"yes\";s:16:\"posts_categories\";s:3:\"yes\";s:20:\"posts_categories_add\";s:3:\"yes\";s:21:\"posts_categories_edit\";s:3:\"yes\";s:23:\"posts_categories_delete\";s:3:\"yes\";s:5:\"posts\";s:3:\"yes\";s:9:\"posts_add\";s:3:\"yes\";s:10:\"posts_edit\";s:3:\"yes\";s:12:\"posts_delete\";s:3:\"yes\";s:5:\"pages\";s:3:\"yes\";s:9:\"pages_add\";s:3:\"yes\";s:10:\"pages_edit\";s:3:\"yes\";s:12:\"pages_delete\";s:3:\"yes\";s:12:\"frontendmenu\";s:3:\"yes\";s:7:\"setting\";s:3:\"yes\";s:16:\"frontend_setting\";s:3:\"yes\";s:15:\"paymentsettings\";s:3:\"yes\";s:11:\"smssettings\";s:3:\"yes\";s:12:\"emailsetting\";s:3:\"yes\";s:11:\"marksetting\";s:3:\"yes\";s:9:\"take_exam\";s:3:\"yes\";}'),('c087181c1ba1a09460c46da5f96c2500676f4495','162.243.69.215',1575847245,_binary '__ci_last_regenerate|i:1575847245;'),('c8a511efb3a12ee1d6578fa4970cabb23b4aeb55','159.203.42.143',1575847270,_binary '__ci_last_regenerate|i:1575847270;'),('b5b0304898ef64b54b4c993995aeea3ba95622a3','107.170.96.6',1575847323,_binary '__ci_last_regenerate|i:1575847323;'),('22bb04d0c796954507c869ab13b391588838b19e','107.170.96.6',1575847335,_binary '__ci_last_regenerate|i:1575847335;'),('8f385c0297ca5145e00540cb3d073741bafbe726','138.197.202.197',1575847339,_binary '__ci_last_regenerate|i:1575847339;'),('d7001f942376abdf97b734474591a6f437fb4be3','159.203.81.93',1575847395,_binary '__ci_last_regenerate|i:1575847395;'),('f9acd20812553555235c1c58b628cf0a99bc9d1a','159.203.42.143',1575847403,_binary '__ci_last_regenerate|i:1575847403;'),('209c4659dc38ab05307de358e287b24fec42be3b','159.203.42.143',1575847406,_binary '__ci_last_regenerate|i:1575847406;'),('89ea9120a5ccb7022b44c357a39531af845e1eb3','116.74.199.42',1575847837,_binary '__ci_last_regenerate|i:1575847837;english|N;loginuserID|s:1:\"1\";name|s:6:\"vishal\";email|s:26:\"support@blitzsoftwares.com\";usertypeID|s:1:\"1\";usertype|s:5:\"Admin\";username|s:6:\"vishal\";photo|s:11:\"default.png\";lang|s:7:\"english\";defaultschoolyearID|s:1:\"1\";varifyvaliduser|b:1;loggedin|b:1;get_permission|b:1;master_permission_set|a:372:{s:9:\"dashboard\";s:3:\"yes\";s:7:\"student\";s:3:\"yes\";s:11:\"student_add\";s:3:\"yes\";s:12:\"student_edit\";s:3:\"yes\";s:14:\"student_delete\";s:3:\"yes\";s:12:\"student_view\";s:3:\"yes\";s:7:\"parents\";s:3:\"yes\";s:11:\"parents_add\";s:3:\"yes\";s:12:\"parents_edit\";s:3:\"yes\";s:14:\"parents_delete\";s:3:\"yes\";s:12:\"parents_view\";s:3:\"yes\";s:7:\"teacher\";s:3:\"yes\";s:11:\"teacher_add\";s:3:\"yes\";s:12:\"teacher_edit\";s:3:\"yes\";s:14:\"teacher_delete\";s:3:\"yes\";s:12:\"teacher_view\";s:3:\"yes\";s:4:\"user\";s:3:\"yes\";s:8:\"user_add\";s:3:\"yes\";s:9:\"user_edit\";s:3:\"yes\";s:11:\"user_delete\";s:3:\"yes\";s:9:\"user_view\";s:3:\"yes\";s:7:\"classes\";s:3:\"yes\";s:11:\"classes_add\";s:3:\"yes\";s:12:\"classes_edit\";s:3:\"yes\";s:14:\"classes_delete\";s:3:\"yes\";s:7:\"section\";s:3:\"yes\";s:11:\"section_add\";s:3:\"yes\";s:12:\"section_edit\";s:3:\"yes\";s:15:\"semester_delete\";s:3:\"yes\";s:14:\"section_delete\";s:3:\"yes\";s:7:\"subject\";s:3:\"yes\";s:11:\"subject_add\";s:3:\"yes\";s:12:\"subject_edit\";s:3:\"yes\";s:14:\"subject_delete\";s:3:\"yes\";s:8:\"syllabus\";s:3:\"yes\";s:12:\"syllabus_add\";s:3:\"yes\";s:13:\"syllabus_edit\";s:3:\"yes\";s:15:\"syllabus_delete\";s:3:\"yes\";s:10:\"assignment\";s:3:\"yes\";s:14:\"assignment_add\";s:3:\"yes\";s:15:\"assignment_edit\";s:3:\"yes\";s:17:\"assignment_delete\";s:3:\"yes\";s:15:\"assignment_view\";s:3:\"yes\";s:7:\"routine\";s:3:\"yes\";s:11:\"routine_add\";s:3:\"yes\";s:12:\"routine_edit\";s:3:\"yes\";s:14:\"routine_delete\";s:3:\"yes\";s:11:\"sattendance\";s:3:\"yes\";s:15:\"sattendance_add\";s:3:\"yes\";s:16:\"sattendance_view\";s:3:\"yes\";s:11:\"tattendance\";s:3:\"yes\";s:15:\"tattendance_add\";s:3:\"yes\";s:16:\"tattendance_view\";s:3:\"yes\";s:11:\"uattendance\";s:3:\"yes\";s:15:\"uattendance_add\";s:3:\"yes\";s:16:\"uattendance_view\";s:3:\"yes\";s:4:\"exam\";s:3:\"yes\";s:8:\"exam_add\";s:3:\"yes\";s:9:\"exam_edit\";s:3:\"yes\";s:11:\"exam_delete\";s:3:\"yes\";s:12:\"examschedule\";s:3:\"yes\";s:16:\"examschedule_add\";s:3:\"yes\";s:17:\"examschedule_edit\";s:3:\"yes\";s:19:\"examschedule_delete\";s:3:\"yes\";s:5:\"grade\";s:3:\"yes\";s:9:\"grade_add\";s:3:\"yes\";s:10:\"grade_edit\";s:3:\"yes\";s:12:\"grade_delete\";s:3:\"yes\";s:11:\"eattendance\";s:3:\"yes\";s:15:\"eattendance_add\";s:3:\"yes\";s:4:\"mark\";s:3:\"yes\";s:8:\"mark_add\";s:3:\"yes\";s:9:\"mark_view\";s:3:\"yes\";s:14:\"markpercentage\";s:3:\"yes\";s:18:\"markpercentage_add\";s:3:\"yes\";s:19:\"markpercentage_edit\";s:3:\"yes\";s:21:\"markpercentage_delete\";s:3:\"yes\";s:9:\"promotion\";s:3:\"yes\";s:12:\"conversation\";s:3:\"yes\";s:5:\"media\";s:3:\"yes\";s:9:\"media_add\";s:3:\"yes\";s:12:\"media_delete\";s:3:\"yes\";s:10:\"mailandsms\";s:3:\"yes\";s:14:\"mailandsms_add\";s:3:\"yes\";s:15:\"mailandsms_view\";s:3:\"yes\";s:14:\"question_group\";s:3:\"yes\";s:18:\"question_group_add\";s:3:\"yes\";s:19:\"question_group_edit\";s:3:\"yes\";s:21:\"question_group_delete\";s:3:\"yes\";s:14:\"question_level\";s:3:\"yes\";s:18:\"question_level_add\";s:3:\"yes\";s:19:\"question_level_edit\";s:3:\"yes\";s:21:\"question_level_delete\";s:3:\"yes\";s:13:\"question_bank\";s:3:\"yes\";s:17:\"question_bank_add\";s:3:\"yes\";s:18:\"question_bank_edit\";s:3:\"yes\";s:20:\"question_bank_delete\";s:3:\"yes\";s:18:\"question_bank_view\";s:3:\"yes\";s:11:\"online_exam\";s:3:\"yes\";s:15:\"online_exam_add\";s:3:\"yes\";s:16:\"online_exam_edit\";s:3:\"yes\";s:18:\"online_exam_delete\";s:3:\"yes\";s:11:\"instruction\";s:3:\"yes\";s:15:\"instruction_add\";s:3:\"yes\";s:16:\"instruction_edit\";s:3:\"yes\";s:18:\"instruction_delete\";s:3:\"yes\";s:16:\"instruction_view\";s:3:\"yes\";s:15:\"salary_template\";s:3:\"yes\";s:19:\"salary_template_add\";s:3:\"yes\";s:20:\"salary_template_edit\";s:3:\"yes\";s:22:\"salary_template_delete\";s:3:\"yes\";s:20:\"salary_template_view\";s:3:\"yes\";s:15:\"hourly_template\";s:3:\"yes\";s:19:\"hourly_template_add\";s:3:\"yes\";s:20:\"hourly_template_edit\";s:3:\"yes\";s:22:\"hourly_template_delete\";s:3:\"yes\";s:13:\"manage_salary\";s:3:\"yes\";s:17:\"manage_salary_add\";s:3:\"yes\";s:18:\"manage_salary_edit\";s:3:\"yes\";s:20:\"manage_salary_delete\";s:3:\"yes\";s:18:\"manage_salary_view\";s:3:\"yes\";s:12:\"make_payment\";s:3:\"yes\";s:6:\"vendor\";s:3:\"yes\";s:10:\"vendor_add\";s:3:\"yes\";s:11:\"vendor_edit\";s:3:\"yes\";s:13:\"vendor_delete\";s:3:\"yes\";s:8:\"location\";s:3:\"yes\";s:12:\"location_add\";s:3:\"yes\";s:13:\"location_edit\";s:3:\"yes\";s:15:\"location_delete\";s:3:\"yes\";s:14:\"asset_category\";s:3:\"yes\";s:18:\"asset_category_add\";s:3:\"yes\";s:19:\"asset_category_edit\";s:3:\"yes\";s:21:\"asset_category_delete\";s:3:\"yes\";s:5:\"asset\";s:3:\"yes\";s:9:\"asset_add\";s:3:\"yes\";s:10:\"asset_edit\";s:3:\"yes\";s:12:\"asset_delete\";s:3:\"yes\";s:10:\"asset_view\";s:3:\"yes\";s:16:\"asset_assignment\";s:3:\"yes\";s:20:\"asset_assignment_add\";s:3:\"yes\";s:21:\"asset_assignment_edit\";s:3:\"yes\";s:23:\"asset_assignment_delete\";s:3:\"yes\";s:21:\"asset_assignment_view\";s:3:\"yes\";s:8:\"purchase\";s:3:\"yes\";s:12:\"purchase_add\";s:3:\"yes\";s:13:\"purchase_edit\";s:3:\"yes\";s:15:\"purchase_delete\";s:3:\"yes\";s:15:\"productcategory\";s:3:\"yes\";s:19:\"productcategory_add\";s:3:\"yes\";s:20:\"productcategory_edit\";s:3:\"yes\";s:22:\"productcategory_delete\";s:3:\"yes\";s:7:\"product\";s:3:\"yes\";s:11:\"product_add\";s:3:\"yes\";s:12:\"product_edit\";s:3:\"yes\";s:14:\"product_delete\";s:3:\"yes\";s:16:\"productwarehouse\";s:3:\"yes\";s:20:\"productwarehouse_add\";s:3:\"yes\";s:21:\"productwarehouse_edit\";s:3:\"yes\";s:23:\"productwarehouse_delete\";s:3:\"yes\";s:15:\"productsupplier\";s:3:\"yes\";s:19:\"productsupplier_add\";s:3:\"yes\";s:20:\"productsupplier_edit\";s:3:\"yes\";s:22:\"productsupplier_delete\";s:3:\"yes\";s:15:\"productpurchase\";s:3:\"yes\";s:19:\"productpurchase_add\";s:3:\"yes\";s:20:\"productpurchase_edit\";s:3:\"yes\";s:22:\"productpurchase_delete\";s:3:\"yes\";s:20:\"productpurchase_view\";s:3:\"yes\";s:11:\"productsale\";s:3:\"yes\";s:15:\"productsale_add\";s:3:\"yes\";s:16:\"productsale_edit\";s:3:\"yes\";s:18:\"productsale_delete\";s:3:\"yes\";s:16:\"productsale_view\";s:3:\"yes\";s:13:\"leavecategory\";s:3:\"yes\";s:17:\"leavecategory_add\";s:3:\"yes\";s:18:\"leavecategory_edit\";s:3:\"yes\";s:20:\"leavecategory_delete\";s:3:\"yes\";s:11:\"leaveassign\";s:3:\"yes\";s:15:\"leaveassign_add\";s:3:\"yes\";s:16:\"leaveassign_edit\";s:3:\"yes\";s:18:\"leaveassign_delete\";s:3:\"yes\";s:10:\"leaveapply\";s:3:\"yes\";s:14:\"leaveapply_add\";s:3:\"yes\";s:15:\"leaveapply_edit\";s:3:\"yes\";s:17:\"leaveapply_delete\";s:3:\"yes\";s:15:\"leaveapply_view\";s:3:\"yes\";s:16:\"leaveapplication\";s:3:\"yes\";s:18:\"activitiescategory\";s:3:\"yes\";s:22:\"activitiescategory_add\";s:3:\"yes\";s:23:\"activitiescategory_edit\";s:3:\"yes\";s:25:\"activitiescategory_delete\";s:3:\"yes\";s:10:\"activities\";s:3:\"yes\";s:14:\"activities_add\";s:3:\"yes\";s:17:\"activities_delete\";s:3:\"yes\";s:9:\"childcare\";s:3:\"yes\";s:13:\"childcare_add\";s:3:\"yes\";s:14:\"childcare_edit\";s:3:\"yes\";s:16:\"childcare_delete\";s:3:\"yes\";s:7:\"lmember\";s:3:\"yes\";s:11:\"lmember_add\";s:3:\"yes\";s:12:\"lmember_edit\";s:3:\"yes\";s:14:\"lmember_delete\";s:3:\"yes\";s:12:\"lmember_view\";s:3:\"yes\";s:4:\"book\";s:3:\"yes\";s:8:\"book_add\";s:3:\"yes\";s:9:\"book_edit\";s:3:\"yes\";s:11:\"book_delete\";s:3:\"yes\";s:5:\"issue\";s:3:\"yes\";s:9:\"issue_add\";s:3:\"yes\";s:10:\"issue_edit\";s:3:\"yes\";s:10:\"issue_view\";s:3:\"yes\";s:6:\"ebooks\";s:3:\"yes\";s:10:\"ebooks_add\";s:3:\"yes\";s:11:\"ebooks_edit\";s:3:\"yes\";s:13:\"ebooks_delete\";s:3:\"yes\";s:11:\"ebooks_view\";s:3:\"yes\";s:9:\"transport\";s:3:\"yes\";s:13:\"transport_add\";s:3:\"yes\";s:14:\"transport_edit\";s:3:\"yes\";s:16:\"transport_delete\";s:3:\"yes\";s:7:\"tmember\";s:3:\"yes\";s:11:\"tmember_add\";s:3:\"yes\";s:12:\"tmember_edit\";s:3:\"yes\";s:14:\"tmember_delete\";s:3:\"yes\";s:12:\"tmember_view\";s:3:\"yes\";s:6:\"hostel\";s:3:\"yes\";s:10:\"hostel_add\";s:3:\"yes\";s:11:\"hostel_edit\";s:3:\"yes\";s:13:\"hostel_delete\";s:3:\"yes\";s:8:\"category\";s:3:\"yes\";s:12:\"category_add\";s:3:\"yes\";s:13:\"category_edit\";s:3:\"yes\";s:15:\"category_delete\";s:3:\"yes\";s:7:\"hmember\";s:3:\"yes\";s:11:\"hmember_add\";s:3:\"yes\";s:12:\"hmember_edit\";s:3:\"yes\";s:14:\"hmember_delete\";s:3:\"yes\";s:12:\"hmember_view\";s:3:\"yes\";s:8:\"feetypes\";s:3:\"yes\";s:12:\"feetypes_add\";s:3:\"yes\";s:13:\"feetypes_edit\";s:3:\"yes\";s:15:\"feetypes_delete\";s:3:\"yes\";s:7:\"invoice\";s:3:\"yes\";s:11:\"invoice_add\";s:3:\"yes\";s:12:\"invoice_edit\";s:3:\"yes\";s:14:\"invoice_delete\";s:3:\"yes\";s:12:\"invoice_view\";s:3:\"yes\";s:14:\"paymenthistory\";s:3:\"yes\";s:19:\"paymenthistory_edit\";s:3:\"yes\";s:21:\"paymenthistory_delete\";s:3:\"yes\";s:7:\"expense\";s:3:\"yes\";s:11:\"expense_add\";s:3:\"yes\";s:12:\"expense_edit\";s:3:\"yes\";s:14:\"expense_delete\";s:3:\"yes\";s:6:\"income\";s:3:\"yes\";s:10:\"income_add\";s:3:\"yes\";s:11:\"income_edit\";s:3:\"yes\";s:13:\"income_delete\";s:3:\"yes\";s:14:\"global_payment\";s:3:\"yes\";s:6:\"notice\";s:3:\"yes\";s:10:\"notice_add\";s:3:\"yes\";s:11:\"notice_edit\";s:3:\"yes\";s:13:\"notice_delete\";s:3:\"yes\";s:11:\"notice_view\";s:3:\"yes\";s:5:\"event\";s:3:\"yes\";s:9:\"event_add\";s:3:\"yes\";s:10:\"event_edit\";s:3:\"yes\";s:12:\"event_delete\";s:3:\"yes\";s:10:\"event_view\";s:3:\"yes\";s:7:\"holiday\";s:3:\"yes\";s:11:\"holiday_add\";s:3:\"yes\";s:12:\"holiday_edit\";s:3:\"yes\";s:14:\"holiday_delete\";s:3:\"yes\";s:12:\"holiday_view\";s:3:\"yes\";s:13:\"classesreport\";s:3:\"yes\";s:13:\"studentreport\";s:3:\"yes\";s:12:\"idcardreport\";s:3:\"yes\";s:15:\"admitcardreport\";s:3:\"yes\";s:13:\"routinereport\";s:3:\"yes\";s:18:\"examschedulereport\";s:3:\"yes\";s:16:\"attendancereport\";s:3:\"yes\";s:24:\"attendanceoverviewreport\";s:3:\"yes\";s:18:\"librarybooksreport\";s:3:\"yes\";s:17:\"librarycardreport\";s:3:\"yes\";s:22:\"librarybookissuereport\";s:3:\"yes\";s:14:\"terminalreport\";s:3:\"yes\";s:16:\"meritstagereport\";s:3:\"yes\";s:21:\"tabulationsheetreport\";s:3:\"yes\";s:15:\"marksheetreport\";s:3:\"yes\";s:18:\"progresscardreport\";s:3:\"yes\";s:20:\"studentsessionreport\";s:3:\"yes\";s:16:\"onlineexamreport\";s:3:\"yes\";s:24:\"onlineexamquestionreport\";s:3:\"yes\";s:21:\"onlineadmissionreport\";s:3:\"yes\";s:17:\"certificatereport\";s:3:\"yes\";s:22:\"leaveapplicationreport\";s:3:\"yes\";s:21:\"productpurchasereport\";s:3:\"yes\";s:17:\"productsalereport\";s:3:\"yes\";s:23:\"searchpaymentfeesreport\";s:3:\"yes\";s:10:\"feesreport\";s:3:\"yes\";s:13:\"duefeesreport\";s:3:\"yes\";s:17:\"balancefeesreport\";s:3:\"yes\";s:17:\"transactionreport\";s:3:\"yes\";s:17:\"studentfinereport\";s:3:\"yes\";s:12:\"salaryreport\";s:3:\"yes\";s:19:\"accountledgerreport\";s:3:\"yes\";s:15:\"onlineadmission\";s:3:\"yes\";s:11:\"visitorinfo\";s:3:\"yes\";s:18:\"visitorinfo_delete\";s:3:\"yes\";s:16:\"visitorinfo_view\";s:3:\"yes\";s:10:\"schoolyear\";s:3:\"yes\";s:14:\"schoolyear_add\";s:3:\"yes\";s:15:\"schoolyear_edit\";s:3:\"yes\";s:17:\"schoolyear_delete\";s:3:\"yes\";s:12:\"studentgroup\";s:3:\"yes\";s:16:\"studentgroup_add\";s:3:\"yes\";s:17:\"studentgroup_edit\";s:3:\"yes\";s:19:\"studentgroup_delete\";s:3:\"yes\";s:8:\"complain\";s:3:\"yes\";s:12:\"complain_add\";s:3:\"yes\";s:13:\"complain_edit\";s:3:\"yes\";s:15:\"complain_delete\";s:3:\"yes\";s:13:\"complain_view\";s:3:\"yes\";s:20:\"certificate_template\";s:3:\"yes\";s:24:\"certificate_template_add\";s:3:\"yes\";s:25:\"certificate_template_edit\";s:3:\"yes\";s:27:\"certificate_template_delete\";s:3:\"yes\";s:25:\"certificate_template_view\";s:3:\"yes\";s:11:\"systemadmin\";s:3:\"yes\";s:15:\"systemadmin_add\";s:3:\"yes\";s:16:\"systemadmin_edit\";s:3:\"yes\";s:18:\"systemadmin_delete\";s:3:\"yes\";s:16:\"systemadmin_view\";s:3:\"yes\";s:13:\"resetpassword\";s:3:\"yes\";s:10:\"sociallink\";s:3:\"yes\";s:14:\"sociallink_add\";s:3:\"yes\";s:15:\"sociallink_edit\";s:3:\"yes\";s:17:\"sociallink_delete\";s:3:\"yes\";s:18:\"mailandsmstemplate\";s:3:\"yes\";s:22:\"mailandsmstemplate_add\";s:3:\"yes\";s:23:\"mailandsmstemplate_edit\";s:3:\"yes\";s:25:\"mailandsmstemplate_delete\";s:3:\"yes\";s:23:\"mailandsmstemplate_view\";s:3:\"yes\";s:10:\"bulkimport\";s:3:\"yes\";s:6:\"backup\";s:3:\"yes\";s:8:\"usertype\";s:3:\"yes\";s:12:\"usertype_add\";s:3:\"yes\";s:13:\"usertype_edit\";s:3:\"yes\";s:15:\"usertype_delete\";s:3:\"yes\";s:10:\"permission\";s:3:\"yes\";s:6:\"update\";s:3:\"yes\";s:16:\"posts_categories\";s:3:\"yes\";s:20:\"posts_categories_add\";s:3:\"yes\";s:21:\"posts_categories_edit\";s:3:\"yes\";s:23:\"posts_categories_delete\";s:3:\"yes\";s:5:\"posts\";s:3:\"yes\";s:9:\"posts_add\";s:3:\"yes\";s:10:\"posts_edit\";s:3:\"yes\";s:12:\"posts_delete\";s:3:\"yes\";s:5:\"pages\";s:3:\"yes\";s:9:\"pages_add\";s:3:\"yes\";s:10:\"pages_edit\";s:3:\"yes\";s:12:\"pages_delete\";s:3:\"yes\";s:12:\"frontendmenu\";s:3:\"yes\";s:7:\"setting\";s:3:\"yes\";s:16:\"frontend_setting\";s:3:\"yes\";s:15:\"paymentsettings\";s:3:\"yes\";s:11:\"smssettings\";s:3:\"yes\";s:12:\"emailsetting\";s:3:\"yes\";s:11:\"marksetting\";s:3:\"yes\";s:9:\"take_exam\";s:3:\"yes\";}'),('5f8a5b34ce49115869bec1c9dd12b8f4aeb2b3a4','159.203.196.79',1575847722,_binary '__ci_last_regenerate|i:1575847722;'),('730ed3925f34cae499774ac79f41b57c483afd1f','107.170.96.6',1575847791,_binary '__ci_last_regenerate|i:1575847791;'),('c04cc85f62d7fdfaa34e7961404225966aa12aa2','162.243.69.215',1575847811,_binary '__ci_last_regenerate|i:1575847811;'),('2cfa7bbb29d2a4e06b3dda1456b2dd77d8d1bf5d','162.243.69.215',1575847815,_binary '__ci_last_regenerate|i:1575847815;'),('e72a753ac900f2104550425ceaab75f48a62f1be','162.243.69.215',1575847824,_binary '__ci_last_regenerate|i:1575847824;'),('dc38d7c7414da8643fbc65183074a7543b5e02f2','138.197.202.197',1575847828,_binary '__ci_last_regenerate|i:1575847828;'),('478ebda2721e1cdecd7f961a4d6e811c5f094589','138.197.202.197',1575847830,_binary '__ci_last_regenerate|i:1575847830;'),('8e5aea4054be0da4c3eec6f0708f9ab8065cd571','138.197.202.197',1575847830,_binary '__ci_last_regenerate|i:1575847830;'),('4af29a6968cd0569cfec833bcc8391cd0d2aeab9','159.203.81.93',1575847831,_binary '__ci_last_regenerate|i:1575847831;'),('588a8013459c11db3189bde0be152de210b8ef46','116.74.199.42',1575847847,_binary '__ci_last_regenerate|i:1575847837;english|N;loginuserID|s:1:\"1\";name|s:6:\"vishal\";email|s:26:\"support@blitzsoftwares.com\";usertypeID|s:1:\"1\";usertype|s:5:\"Admin\";username|s:6:\"vishal\";photo|s:11:\"default.png\";lang|s:7:\"english\";defaultschoolyearID|s:1:\"1\";varifyvaliduser|b:1;loggedin|b:1;get_permission|b:1;master_permission_set|a:372:{s:9:\"dashboard\";s:3:\"yes\";s:7:\"student\";s:3:\"yes\";s:11:\"student_add\";s:3:\"yes\";s:12:\"student_edit\";s:3:\"yes\";s:14:\"student_delete\";s:3:\"yes\";s:12:\"student_view\";s:3:\"yes\";s:7:\"parents\";s:3:\"yes\";s:11:\"parents_add\";s:3:\"yes\";s:12:\"parents_edit\";s:3:\"yes\";s:14:\"parents_delete\";s:3:\"yes\";s:12:\"parents_view\";s:3:\"yes\";s:7:\"teacher\";s:3:\"yes\";s:11:\"teacher_add\";s:3:\"yes\";s:12:\"teacher_edit\";s:3:\"yes\";s:14:\"teacher_delete\";s:3:\"yes\";s:12:\"teacher_view\";s:3:\"yes\";s:4:\"user\";s:3:\"yes\";s:8:\"user_add\";s:3:\"yes\";s:9:\"user_edit\";s:3:\"yes\";s:11:\"user_delete\";s:3:\"yes\";s:9:\"user_view\";s:3:\"yes\";s:7:\"classes\";s:3:\"yes\";s:11:\"classes_add\";s:3:\"yes\";s:12:\"classes_edit\";s:3:\"yes\";s:14:\"classes_delete\";s:3:\"yes\";s:7:\"section\";s:3:\"yes\";s:11:\"section_add\";s:3:\"yes\";s:12:\"section_edit\";s:3:\"yes\";s:15:\"semester_delete\";s:3:\"yes\";s:14:\"section_delete\";s:3:\"yes\";s:7:\"subject\";s:3:\"yes\";s:11:\"subject_add\";s:3:\"yes\";s:12:\"subject_edit\";s:3:\"yes\";s:14:\"subject_delete\";s:3:\"yes\";s:8:\"syllabus\";s:3:\"yes\";s:12:\"syllabus_add\";s:3:\"yes\";s:13:\"syllabus_edit\";s:3:\"yes\";s:15:\"syllabus_delete\";s:3:\"yes\";s:10:\"assignment\";s:3:\"yes\";s:14:\"assignment_add\";s:3:\"yes\";s:15:\"assignment_edit\";s:3:\"yes\";s:17:\"assignment_delete\";s:3:\"yes\";s:15:\"assignment_view\";s:3:\"yes\";s:7:\"routine\";s:3:\"yes\";s:11:\"routine_add\";s:3:\"yes\";s:12:\"routine_edit\";s:3:\"yes\";s:14:\"routine_delete\";s:3:\"yes\";s:11:\"sattendance\";s:3:\"yes\";s:15:\"sattendance_add\";s:3:\"yes\";s:16:\"sattendance_view\";s:3:\"yes\";s:11:\"tattendance\";s:3:\"yes\";s:15:\"tattendance_add\";s:3:\"yes\";s:16:\"tattendance_view\";s:3:\"yes\";s:11:\"uattendance\";s:3:\"yes\";s:15:\"uattendance_add\";s:3:\"yes\";s:16:\"uattendance_view\";s:3:\"yes\";s:4:\"exam\";s:3:\"yes\";s:8:\"exam_add\";s:3:\"yes\";s:9:\"exam_edit\";s:3:\"yes\";s:11:\"exam_delete\";s:3:\"yes\";s:12:\"examschedule\";s:3:\"yes\";s:16:\"examschedule_add\";s:3:\"yes\";s:17:\"examschedule_edit\";s:3:\"yes\";s:19:\"examschedule_delete\";s:3:\"yes\";s:5:\"grade\";s:3:\"yes\";s:9:\"grade_add\";s:3:\"yes\";s:10:\"grade_edit\";s:3:\"yes\";s:12:\"grade_delete\";s:3:\"yes\";s:11:\"eattendance\";s:3:\"yes\";s:15:\"eattendance_add\";s:3:\"yes\";s:4:\"mark\";s:3:\"yes\";s:8:\"mark_add\";s:3:\"yes\";s:9:\"mark_view\";s:3:\"yes\";s:14:\"markpercentage\";s:3:\"yes\";s:18:\"markpercentage_add\";s:3:\"yes\";s:19:\"markpercentage_edit\";s:3:\"yes\";s:21:\"markpercentage_delete\";s:3:\"yes\";s:9:\"promotion\";s:3:\"yes\";s:12:\"conversation\";s:3:\"yes\";s:5:\"media\";s:3:\"yes\";s:9:\"media_add\";s:3:\"yes\";s:12:\"media_delete\";s:3:\"yes\";s:10:\"mailandsms\";s:3:\"yes\";s:14:\"mailandsms_add\";s:3:\"yes\";s:15:\"mailandsms_view\";s:3:\"yes\";s:14:\"question_group\";s:3:\"yes\";s:18:\"question_group_add\";s:3:\"yes\";s:19:\"question_group_edit\";s:3:\"yes\";s:21:\"question_group_delete\";s:3:\"yes\";s:14:\"question_level\";s:3:\"yes\";s:18:\"question_level_add\";s:3:\"yes\";s:19:\"question_level_edit\";s:3:\"yes\";s:21:\"question_level_delete\";s:3:\"yes\";s:13:\"question_bank\";s:3:\"yes\";s:17:\"question_bank_add\";s:3:\"yes\";s:18:\"question_bank_edit\";s:3:\"yes\";s:20:\"question_bank_delete\";s:3:\"yes\";s:18:\"question_bank_view\";s:3:\"yes\";s:11:\"online_exam\";s:3:\"yes\";s:15:\"online_exam_add\";s:3:\"yes\";s:16:\"online_exam_edit\";s:3:\"yes\";s:18:\"online_exam_delete\";s:3:\"yes\";s:11:\"instruction\";s:3:\"yes\";s:15:\"instruction_add\";s:3:\"yes\";s:16:\"instruction_edit\";s:3:\"yes\";s:18:\"instruction_delete\";s:3:\"yes\";s:16:\"instruction_view\";s:3:\"yes\";s:15:\"salary_template\";s:3:\"yes\";s:19:\"salary_template_add\";s:3:\"yes\";s:20:\"salary_template_edit\";s:3:\"yes\";s:22:\"salary_template_delete\";s:3:\"yes\";s:20:\"salary_template_view\";s:3:\"yes\";s:15:\"hourly_template\";s:3:\"yes\";s:19:\"hourly_template_add\";s:3:\"yes\";s:20:\"hourly_template_edit\";s:3:\"yes\";s:22:\"hourly_template_delete\";s:3:\"yes\";s:13:\"manage_salary\";s:3:\"yes\";s:17:\"manage_salary_add\";s:3:\"yes\";s:18:\"manage_salary_edit\";s:3:\"yes\";s:20:\"manage_salary_delete\";s:3:\"yes\";s:18:\"manage_salary_view\";s:3:\"yes\";s:12:\"make_payment\";s:3:\"yes\";s:6:\"vendor\";s:3:\"yes\";s:10:\"vendor_add\";s:3:\"yes\";s:11:\"vendor_edit\";s:3:\"yes\";s:13:\"vendor_delete\";s:3:\"yes\";s:8:\"location\";s:3:\"yes\";s:12:\"location_add\";s:3:\"yes\";s:13:\"location_edit\";s:3:\"yes\";s:15:\"location_delete\";s:3:\"yes\";s:14:\"asset_category\";s:3:\"yes\";s:18:\"asset_category_add\";s:3:\"yes\";s:19:\"asset_category_edit\";s:3:\"yes\";s:21:\"asset_category_delete\";s:3:\"yes\";s:5:\"asset\";s:3:\"yes\";s:9:\"asset_add\";s:3:\"yes\";s:10:\"asset_edit\";s:3:\"yes\";s:12:\"asset_delete\";s:3:\"yes\";s:10:\"asset_view\";s:3:\"yes\";s:16:\"asset_assignment\";s:3:\"yes\";s:20:\"asset_assignment_add\";s:3:\"yes\";s:21:\"asset_assignment_edit\";s:3:\"yes\";s:23:\"asset_assignment_delete\";s:3:\"yes\";s:21:\"asset_assignment_view\";s:3:\"yes\";s:8:\"purchase\";s:3:\"yes\";s:12:\"purchase_add\";s:3:\"yes\";s:13:\"purchase_edit\";s:3:\"yes\";s:15:\"purchase_delete\";s:3:\"yes\";s:15:\"productcategory\";s:3:\"yes\";s:19:\"productcategory_add\";s:3:\"yes\";s:20:\"productcategory_edit\";s:3:\"yes\";s:22:\"productcategory_delete\";s:3:\"yes\";s:7:\"product\";s:3:\"yes\";s:11:\"product_add\";s:3:\"yes\";s:12:\"product_edit\";s:3:\"yes\";s:14:\"product_delete\";s:3:\"yes\";s:16:\"productwarehouse\";s:3:\"yes\";s:20:\"productwarehouse_add\";s:3:\"yes\";s:21:\"productwarehouse_edit\";s:3:\"yes\";s:23:\"productwarehouse_delete\";s:3:\"yes\";s:15:\"productsupplier\";s:3:\"yes\";s:19:\"productsupplier_add\";s:3:\"yes\";s:20:\"productsupplier_edit\";s:3:\"yes\";s:22:\"productsupplier_delete\";s:3:\"yes\";s:15:\"productpurchase\";s:3:\"yes\";s:19:\"productpurchase_add\";s:3:\"yes\";s:20:\"productpurchase_edit\";s:3:\"yes\";s:22:\"productpurchase_delete\";s:3:\"yes\";s:20:\"productpurchase_view\";s:3:\"yes\";s:11:\"productsale\";s:3:\"yes\";s:15:\"productsale_add\";s:3:\"yes\";s:16:\"productsale_edit\";s:3:\"yes\";s:18:\"productsale_delete\";s:3:\"yes\";s:16:\"productsale_view\";s:3:\"yes\";s:13:\"leavecategory\";s:3:\"yes\";s:17:\"leavecategory_add\";s:3:\"yes\";s:18:\"leavecategory_edit\";s:3:\"yes\";s:20:\"leavecategory_delete\";s:3:\"yes\";s:11:\"leaveassign\";s:3:\"yes\";s:15:\"leaveassign_add\";s:3:\"yes\";s:16:\"leaveassign_edit\";s:3:\"yes\";s:18:\"leaveassign_delete\";s:3:\"yes\";s:10:\"leaveapply\";s:3:\"yes\";s:14:\"leaveapply_add\";s:3:\"yes\";s:15:\"leaveapply_edit\";s:3:\"yes\";s:17:\"leaveapply_delete\";s:3:\"yes\";s:15:\"leaveapply_view\";s:3:\"yes\";s:16:\"leaveapplication\";s:3:\"yes\";s:18:\"activitiescategory\";s:3:\"yes\";s:22:\"activitiescategory_add\";s:3:\"yes\";s:23:\"activitiescategory_edit\";s:3:\"yes\";s:25:\"activitiescategory_delete\";s:3:\"yes\";s:10:\"activities\";s:3:\"yes\";s:14:\"activities_add\";s:3:\"yes\";s:17:\"activities_delete\";s:3:\"yes\";s:9:\"childcare\";s:3:\"yes\";s:13:\"childcare_add\";s:3:\"yes\";s:14:\"childcare_edit\";s:3:\"yes\";s:16:\"childcare_delete\";s:3:\"yes\";s:7:\"lmember\";s:3:\"yes\";s:11:\"lmember_add\";s:3:\"yes\";s:12:\"lmember_edit\";s:3:\"yes\";s:14:\"lmember_delete\";s:3:\"yes\";s:12:\"lmember_view\";s:3:\"yes\";s:4:\"book\";s:3:\"yes\";s:8:\"book_add\";s:3:\"yes\";s:9:\"book_edit\";s:3:\"yes\";s:11:\"book_delete\";s:3:\"yes\";s:5:\"issue\";s:3:\"yes\";s:9:\"issue_add\";s:3:\"yes\";s:10:\"issue_edit\";s:3:\"yes\";s:10:\"issue_view\";s:3:\"yes\";s:6:\"ebooks\";s:3:\"yes\";s:10:\"ebooks_add\";s:3:\"yes\";s:11:\"ebooks_edit\";s:3:\"yes\";s:13:\"ebooks_delete\";s:3:\"yes\";s:11:\"ebooks_view\";s:3:\"yes\";s:9:\"transport\";s:3:\"yes\";s:13:\"transport_add\";s:3:\"yes\";s:14:\"transport_edit\";s:3:\"yes\";s:16:\"transport_delete\";s:3:\"yes\";s:7:\"tmember\";s:3:\"yes\";s:11:\"tmember_add\";s:3:\"yes\";s:12:\"tmember_edit\";s:3:\"yes\";s:14:\"tmember_delete\";s:3:\"yes\";s:12:\"tmember_view\";s:3:\"yes\";s:6:\"hostel\";s:3:\"yes\";s:10:\"hostel_add\";s:3:\"yes\";s:11:\"hostel_edit\";s:3:\"yes\";s:13:\"hostel_delete\";s:3:\"yes\";s:8:\"category\";s:3:\"yes\";s:12:\"category_add\";s:3:\"yes\";s:13:\"category_edit\";s:3:\"yes\";s:15:\"category_delete\";s:3:\"yes\";s:7:\"hmember\";s:3:\"yes\";s:11:\"hmember_add\";s:3:\"yes\";s:12:\"hmember_edit\";s:3:\"yes\";s:14:\"hmember_delete\";s:3:\"yes\";s:12:\"hmember_view\";s:3:\"yes\";s:8:\"feetypes\";s:3:\"yes\";s:12:\"feetypes_add\";s:3:\"yes\";s:13:\"feetypes_edit\";s:3:\"yes\";s:15:\"feetypes_delete\";s:3:\"yes\";s:7:\"invoice\";s:3:\"yes\";s:11:\"invoice_add\";s:3:\"yes\";s:12:\"invoice_edit\";s:3:\"yes\";s:14:\"invoice_delete\";s:3:\"yes\";s:12:\"invoice_view\";s:3:\"yes\";s:14:\"paymenthistory\";s:3:\"yes\";s:19:\"paymenthistory_edit\";s:3:\"yes\";s:21:\"paymenthistory_delete\";s:3:\"yes\";s:7:\"expense\";s:3:\"yes\";s:11:\"expense_add\";s:3:\"yes\";s:12:\"expense_edit\";s:3:\"yes\";s:14:\"expense_delete\";s:3:\"yes\";s:6:\"income\";s:3:\"yes\";s:10:\"income_add\";s:3:\"yes\";s:11:\"income_edit\";s:3:\"yes\";s:13:\"income_delete\";s:3:\"yes\";s:14:\"global_payment\";s:3:\"yes\";s:6:\"notice\";s:3:\"yes\";s:10:\"notice_add\";s:3:\"yes\";s:11:\"notice_edit\";s:3:\"yes\";s:13:\"notice_delete\";s:3:\"yes\";s:11:\"notice_view\";s:3:\"yes\";s:5:\"event\";s:3:\"yes\";s:9:\"event_add\";s:3:\"yes\";s:10:\"event_edit\";s:3:\"yes\";s:12:\"event_delete\";s:3:\"yes\";s:10:\"event_view\";s:3:\"yes\";s:7:\"holiday\";s:3:\"yes\";s:11:\"holiday_add\";s:3:\"yes\";s:12:\"holiday_edit\";s:3:\"yes\";s:14:\"holiday_delete\";s:3:\"yes\";s:12:\"holiday_view\";s:3:\"yes\";s:13:\"classesreport\";s:3:\"yes\";s:13:\"studentreport\";s:3:\"yes\";s:12:\"idcardreport\";s:3:\"yes\";s:15:\"admitcardreport\";s:3:\"yes\";s:13:\"routinereport\";s:3:\"yes\";s:18:\"examschedulereport\";s:3:\"yes\";s:16:\"attendancereport\";s:3:\"yes\";s:24:\"attendanceoverviewreport\";s:3:\"yes\";s:18:\"librarybooksreport\";s:3:\"yes\";s:17:\"librarycardreport\";s:3:\"yes\";s:22:\"librarybookissuereport\";s:3:\"yes\";s:14:\"terminalreport\";s:3:\"yes\";s:16:\"meritstagereport\";s:3:\"yes\";s:21:\"tabulationsheetreport\";s:3:\"yes\";s:15:\"marksheetreport\";s:3:\"yes\";s:18:\"progresscardreport\";s:3:\"yes\";s:20:\"studentsessionreport\";s:3:\"yes\";s:16:\"onlineexamreport\";s:3:\"yes\";s:24:\"onlineexamquestionreport\";s:3:\"yes\";s:21:\"onlineadmissionreport\";s:3:\"yes\";s:17:\"certificatereport\";s:3:\"yes\";s:22:\"leaveapplicationreport\";s:3:\"yes\";s:21:\"productpurchasereport\";s:3:\"yes\";s:17:\"productsalereport\";s:3:\"yes\";s:23:\"searchpaymentfeesreport\";s:3:\"yes\";s:10:\"feesreport\";s:3:\"yes\";s:13:\"duefeesreport\";s:3:\"yes\";s:17:\"balancefeesreport\";s:3:\"yes\";s:17:\"transactionreport\";s:3:\"yes\";s:17:\"studentfinereport\";s:3:\"yes\";s:12:\"salaryreport\";s:3:\"yes\";s:19:\"accountledgerreport\";s:3:\"yes\";s:15:\"onlineadmission\";s:3:\"yes\";s:11:\"visitorinfo\";s:3:\"yes\";s:18:\"visitorinfo_delete\";s:3:\"yes\";s:16:\"visitorinfo_view\";s:3:\"yes\";s:10:\"schoolyear\";s:3:\"yes\";s:14:\"schoolyear_add\";s:3:\"yes\";s:15:\"schoolyear_edit\";s:3:\"yes\";s:17:\"schoolyear_delete\";s:3:\"yes\";s:12:\"studentgroup\";s:3:\"yes\";s:16:\"studentgroup_add\";s:3:\"yes\";s:17:\"studentgroup_edit\";s:3:\"yes\";s:19:\"studentgroup_delete\";s:3:\"yes\";s:8:\"complain\";s:3:\"yes\";s:12:\"complain_add\";s:3:\"yes\";s:13:\"complain_edit\";s:3:\"yes\";s:15:\"complain_delete\";s:3:\"yes\";s:13:\"complain_view\";s:3:\"yes\";s:20:\"certificate_template\";s:3:\"yes\";s:24:\"certificate_template_add\";s:3:\"yes\";s:25:\"certificate_template_edit\";s:3:\"yes\";s:27:\"certificate_template_delete\";s:3:\"yes\";s:25:\"certificate_template_view\";s:3:\"yes\";s:11:\"systemadmin\";s:3:\"yes\";s:15:\"systemadmin_add\";s:3:\"yes\";s:16:\"systemadmin_edit\";s:3:\"yes\";s:18:\"systemadmin_delete\";s:3:\"yes\";s:16:\"systemadmin_view\";s:3:\"yes\";s:13:\"resetpassword\";s:3:\"yes\";s:10:\"sociallink\";s:3:\"yes\";s:14:\"sociallink_add\";s:3:\"yes\";s:15:\"sociallink_edit\";s:3:\"yes\";s:17:\"sociallink_delete\";s:3:\"yes\";s:18:\"mailandsmstemplate\";s:3:\"yes\";s:22:\"mailandsmstemplate_add\";s:3:\"yes\";s:23:\"mailandsmstemplate_edit\";s:3:\"yes\";s:25:\"mailandsmstemplate_delete\";s:3:\"yes\";s:23:\"mailandsmstemplate_view\";s:3:\"yes\";s:10:\"bulkimport\";s:3:\"yes\";s:6:\"backup\";s:3:\"yes\";s:8:\"usertype\";s:3:\"yes\";s:12:\"usertype_add\";s:3:\"yes\";s:13:\"usertype_edit\";s:3:\"yes\";s:15:\"usertype_delete\";s:3:\"yes\";s:10:\"permission\";s:3:\"yes\";s:6:\"update\";s:3:\"yes\";s:16:\"posts_categories\";s:3:\"yes\";s:20:\"posts_categories_add\";s:3:\"yes\";s:21:\"posts_categories_edit\";s:3:\"yes\";s:23:\"posts_categories_delete\";s:3:\"yes\";s:5:\"posts\";s:3:\"yes\";s:9:\"posts_add\";s:3:\"yes\";s:10:\"posts_edit\";s:3:\"yes\";s:12:\"posts_delete\";s:3:\"yes\";s:5:\"pages\";s:3:\"yes\";s:9:\"pages_add\";s:3:\"yes\";s:10:\"pages_edit\";s:3:\"yes\";s:12:\"pages_delete\";s:3:\"yes\";s:12:\"frontendmenu\";s:3:\"yes\";s:7:\"setting\";s:3:\"yes\";s:16:\"frontend_setting\";s:3:\"yes\";s:15:\"paymentsettings\";s:3:\"yes\";s:11:\"smssettings\";s:3:\"yes\";s:12:\"emailsetting\";s:3:\"yes\";s:11:\"marksetting\";s:3:\"yes\";s:9:\"take_exam\";s:3:\"yes\";}'),('baa062b57de45745ef9965fd5732359b834380b7','49.36.26.50',1575983388,_binary '__ci_last_regenerate|i:1575983388;'),('b66b12a3ccb5b6f4d2ead5e3092662c9cd16ed72','82.102.27.115',1576090643,_binary '__ci_last_regenerate|i:1576090643;'),('05f9e9525a378ed1a2ba6d7b567682f7b389f536','45.77.48.44',1577030983,_binary '__ci_last_regenerate|i:1577030982;'),('ae5ab6801a22a45f5ee597176f2174d8713916e0','51.158.117.104',1577145093,_binary '__ci_last_regenerate|i:1577145093;'),('faffc2f2b62b539b396e01307907a87ee781689b','51.158.117.104',1577150330,_binary '__ci_last_regenerate|i:1577150330;'),('1cae788423992501379022e2db07366556587483','107.175.156.144',1578054282,_binary '__ci_last_regenerate|i:1578054282;'),('08f8159058a4e8bfe5aa1169da704b9e6124f473','107.175.156.144',1578132222,_binary '__ci_last_regenerate|i:1578132222;'),('b755209cac4207154ba2505a5401b73097554252','107.175.156.144',1578132280,_binary '__ci_last_regenerate|i:1578132280;'),('7aab926648efdf32cc11aa14e74db52c7bc92cb1','107.175.156.144',1578367856,_binary '__ci_last_regenerate|i:1578367856;'),('aad9ceb79c69db42f2e8c1467d015420ad5ce0e0','107.175.156.144',1578408778,_binary '__ci_last_regenerate|i:1578408778;'),('9f9cbbd655a87f1d44c00a90017c0c307a4686ac','89.187.178.142',1578980240,_binary '__ci_last_regenerate|i:1578980240;'),('42ba6e43684d6196faa9a2d12df30242e713720a','89.187.178.238',1579474839,_binary '__ci_last_regenerate|i:1579474839;'),('0053d45bbb1ed6516cdd6c168d87e7b15771080b','38.145.91.189',1579825858,_binary '__ci_last_regenerate|i:1579825857;'),('7dc0d3fb00a1795e46e404af448df17d367a3910','95.211.147.143',1579902623,_binary '__ci_last_regenerate|i:1579902620;'),('7547e19699343368dfa93892dccab31f9f262f6e','49.36.52.8',1580145558,_binary '__ci_last_regenerate|i:1580145558;english|N;loginuserID|s:1:\"1\";name|s:6:\"vishal\";email|s:26:\"support@blitzsoftwares.com\";usertypeID|s:1:\"1\";usertype|s:5:\"Admin\";username|s:6:\"vishal\";photo|s:11:\"default.png\";lang|s:7:\"english\";defaultschoolyearID|s:1:\"1\";varifyvaliduser|b:1;loggedin|b:1;get_permission|b:1;master_permission_set|a:372:{s:9:\"dashboard\";s:3:\"yes\";s:7:\"student\";s:3:\"yes\";s:11:\"student_add\";s:3:\"yes\";s:12:\"student_edit\";s:3:\"yes\";s:14:\"student_delete\";s:3:\"yes\";s:12:\"student_view\";s:3:\"yes\";s:7:\"parents\";s:3:\"yes\";s:11:\"parents_add\";s:3:\"yes\";s:12:\"parents_edit\";s:3:\"yes\";s:14:\"parents_delete\";s:3:\"yes\";s:12:\"parents_view\";s:3:\"yes\";s:7:\"teacher\";s:3:\"yes\";s:11:\"teacher_add\";s:3:\"yes\";s:12:\"teacher_edit\";s:3:\"yes\";s:14:\"teacher_delete\";s:3:\"yes\";s:12:\"teacher_view\";s:3:\"yes\";s:4:\"user\";s:3:\"yes\";s:8:\"user_add\";s:3:\"yes\";s:9:\"user_edit\";s:3:\"yes\";s:11:\"user_delete\";s:3:\"yes\";s:9:\"user_view\";s:3:\"yes\";s:7:\"classes\";s:3:\"yes\";s:11:\"classes_add\";s:3:\"yes\";s:12:\"classes_edit\";s:3:\"yes\";s:14:\"classes_delete\";s:3:\"yes\";s:7:\"section\";s:3:\"yes\";s:11:\"section_add\";s:3:\"yes\";s:12:\"section_edit\";s:3:\"yes\";s:15:\"semester_delete\";s:3:\"yes\";s:14:\"section_delete\";s:3:\"yes\";s:7:\"subject\";s:3:\"yes\";s:11:\"subject_add\";s:3:\"yes\";s:12:\"subject_edit\";s:3:\"yes\";s:14:\"subject_delete\";s:3:\"yes\";s:8:\"syllabus\";s:3:\"yes\";s:12:\"syllabus_add\";s:3:\"yes\";s:13:\"syllabus_edit\";s:3:\"yes\";s:15:\"syllabus_delete\";s:3:\"yes\";s:10:\"assignment\";s:3:\"yes\";s:14:\"assignment_add\";s:3:\"yes\";s:15:\"assignment_edit\";s:3:\"yes\";s:17:\"assignment_delete\";s:3:\"yes\";s:15:\"assignment_view\";s:3:\"yes\";s:7:\"routine\";s:3:\"yes\";s:11:\"routine_add\";s:3:\"yes\";s:12:\"routine_edit\";s:3:\"yes\";s:14:\"routine_delete\";s:3:\"yes\";s:11:\"sattendance\";s:3:\"yes\";s:15:\"sattendance_add\";s:3:\"yes\";s:16:\"sattendance_view\";s:3:\"yes\";s:11:\"tattendance\";s:3:\"yes\";s:15:\"tattendance_add\";s:3:\"yes\";s:16:\"tattendance_view\";s:3:\"yes\";s:11:\"uattendance\";s:3:\"yes\";s:15:\"uattendance_add\";s:3:\"yes\";s:16:\"uattendance_view\";s:3:\"yes\";s:4:\"exam\";s:3:\"yes\";s:8:\"exam_add\";s:3:\"yes\";s:9:\"exam_edit\";s:3:\"yes\";s:11:\"exam_delete\";s:3:\"yes\";s:12:\"examschedule\";s:3:\"yes\";s:16:\"examschedule_add\";s:3:\"yes\";s:17:\"examschedule_edit\";s:3:\"yes\";s:19:\"examschedule_delete\";s:3:\"yes\";s:5:\"grade\";s:3:\"yes\";s:9:\"grade_add\";s:3:\"yes\";s:10:\"grade_edit\";s:3:\"yes\";s:12:\"grade_delete\";s:3:\"yes\";s:11:\"eattendance\";s:3:\"yes\";s:15:\"eattendance_add\";s:3:\"yes\";s:4:\"mark\";s:3:\"yes\";s:8:\"mark_add\";s:3:\"yes\";s:9:\"mark_view\";s:3:\"yes\";s:14:\"markpercentage\";s:3:\"yes\";s:18:\"markpercentage_add\";s:3:\"yes\";s:19:\"markpercentage_edit\";s:3:\"yes\";s:21:\"markpercentage_delete\";s:3:\"yes\";s:9:\"promotion\";s:3:\"yes\";s:12:\"conversation\";s:3:\"yes\";s:5:\"media\";s:3:\"yes\";s:9:\"media_add\";s:3:\"yes\";s:12:\"media_delete\";s:3:\"yes\";s:10:\"mailandsms\";s:3:\"yes\";s:14:\"mailandsms_add\";s:3:\"yes\";s:15:\"mailandsms_view\";s:3:\"yes\";s:14:\"question_group\";s:3:\"yes\";s:18:\"question_group_add\";s:3:\"yes\";s:19:\"question_group_edit\";s:3:\"yes\";s:21:\"question_group_delete\";s:3:\"yes\";s:14:\"question_level\";s:3:\"yes\";s:18:\"question_level_add\";s:3:\"yes\";s:19:\"question_level_edit\";s:3:\"yes\";s:21:\"question_level_delete\";s:3:\"yes\";s:13:\"question_bank\";s:3:\"yes\";s:17:\"question_bank_add\";s:3:\"yes\";s:18:\"question_bank_edit\";s:3:\"yes\";s:20:\"question_bank_delete\";s:3:\"yes\";s:18:\"question_bank_view\";s:3:\"yes\";s:11:\"online_exam\";s:3:\"yes\";s:15:\"online_exam_add\";s:3:\"yes\";s:16:\"online_exam_edit\";s:3:\"yes\";s:18:\"online_exam_delete\";s:3:\"yes\";s:11:\"instruction\";s:3:\"yes\";s:15:\"instruction_add\";s:3:\"yes\";s:16:\"instruction_edit\";s:3:\"yes\";s:18:\"instruction_delete\";s:3:\"yes\";s:16:\"instruction_view\";s:3:\"yes\";s:15:\"salary_template\";s:3:\"yes\";s:19:\"salary_template_add\";s:3:\"yes\";s:20:\"salary_template_edit\";s:3:\"yes\";s:22:\"salary_template_delete\";s:3:\"yes\";s:20:\"salary_template_view\";s:3:\"yes\";s:15:\"hourly_template\";s:3:\"yes\";s:19:\"hourly_template_add\";s:3:\"yes\";s:20:\"hourly_template_edit\";s:3:\"yes\";s:22:\"hourly_template_delete\";s:3:\"yes\";s:13:\"manage_salary\";s:3:\"yes\";s:17:\"manage_salary_add\";s:3:\"yes\";s:18:\"manage_salary_edit\";s:3:\"yes\";s:20:\"manage_salary_delete\";s:3:\"yes\";s:18:\"manage_salary_view\";s:3:\"yes\";s:12:\"make_payment\";s:3:\"yes\";s:6:\"vendor\";s:3:\"yes\";s:10:\"vendor_add\";s:3:\"yes\";s:11:\"vendor_edit\";s:3:\"yes\";s:13:\"vendor_delete\";s:3:\"yes\";s:8:\"location\";s:3:\"yes\";s:12:\"location_add\";s:3:\"yes\";s:13:\"location_edit\";s:3:\"yes\";s:15:\"location_delete\";s:3:\"yes\";s:14:\"asset_category\";s:3:\"yes\";s:18:\"asset_category_add\";s:3:\"yes\";s:19:\"asset_category_edit\";s:3:\"yes\";s:21:\"asset_category_delete\";s:3:\"yes\";s:5:\"asset\";s:3:\"yes\";s:9:\"asset_add\";s:3:\"yes\";s:10:\"asset_edit\";s:3:\"yes\";s:12:\"asset_delete\";s:3:\"yes\";s:10:\"asset_view\";s:3:\"yes\";s:16:\"asset_assignment\";s:3:\"yes\";s:20:\"asset_assignment_add\";s:3:\"yes\";s:21:\"asset_assignment_edit\";s:3:\"yes\";s:23:\"asset_assignment_delete\";s:3:\"yes\";s:21:\"asset_assignment_view\";s:3:\"yes\";s:8:\"purchase\";s:3:\"yes\";s:12:\"purchase_add\";s:3:\"yes\";s:13:\"purchase_edit\";s:3:\"yes\";s:15:\"purchase_delete\";s:3:\"yes\";s:15:\"productcategory\";s:3:\"yes\";s:19:\"productcategory_add\";s:3:\"yes\";s:20:\"productcategory_edit\";s:3:\"yes\";s:22:\"productcategory_delete\";s:3:\"yes\";s:7:\"product\";s:3:\"yes\";s:11:\"product_add\";s:3:\"yes\";s:12:\"product_edit\";s:3:\"yes\";s:14:\"product_delete\";s:3:\"yes\";s:16:\"productwarehouse\";s:3:\"yes\";s:20:\"productwarehouse_add\";s:3:\"yes\";s:21:\"productwarehouse_edit\";s:3:\"yes\";s:23:\"productwarehouse_delete\";s:3:\"yes\";s:15:\"productsupplier\";s:3:\"yes\";s:19:\"productsupplier_add\";s:3:\"yes\";s:20:\"productsupplier_edit\";s:3:\"yes\";s:22:\"productsupplier_delete\";s:3:\"yes\";s:15:\"productpurchase\";s:3:\"yes\";s:19:\"productpurchase_add\";s:3:\"yes\";s:20:\"productpurchase_edit\";s:3:\"yes\";s:22:\"productpurchase_delete\";s:3:\"yes\";s:20:\"productpurchase_view\";s:3:\"yes\";s:11:\"productsale\";s:3:\"yes\";s:15:\"productsale_add\";s:3:\"yes\";s:16:\"productsale_edit\";s:3:\"yes\";s:18:\"productsale_delete\";s:3:\"yes\";s:16:\"productsale_view\";s:3:\"yes\";s:13:\"leavecategory\";s:3:\"yes\";s:17:\"leavecategory_add\";s:3:\"yes\";s:18:\"leavecategory_edit\";s:3:\"yes\";s:20:\"leavecategory_delete\";s:3:\"yes\";s:11:\"leaveassign\";s:3:\"yes\";s:15:\"leaveassign_add\";s:3:\"yes\";s:16:\"leaveassign_edit\";s:3:\"yes\";s:18:\"leaveassign_delete\";s:3:\"yes\";s:10:\"leaveapply\";s:3:\"yes\";s:14:\"leaveapply_add\";s:3:\"yes\";s:15:\"leaveapply_edit\";s:3:\"yes\";s:17:\"leaveapply_delete\";s:3:\"yes\";s:15:\"leaveapply_view\";s:3:\"yes\";s:16:\"leaveapplication\";s:3:\"yes\";s:18:\"activitiescategory\";s:3:\"yes\";s:22:\"activitiescategory_add\";s:3:\"yes\";s:23:\"activitiescategory_edit\";s:3:\"yes\";s:25:\"activitiescategory_delete\";s:3:\"yes\";s:10:\"activities\";s:3:\"yes\";s:14:\"activities_add\";s:3:\"yes\";s:17:\"activities_delete\";s:3:\"yes\";s:9:\"childcare\";s:3:\"yes\";s:13:\"childcare_add\";s:3:\"yes\";s:14:\"childcare_edit\";s:3:\"yes\";s:16:\"childcare_delete\";s:3:\"yes\";s:7:\"lmember\";s:3:\"yes\";s:11:\"lmember_add\";s:3:\"yes\";s:12:\"lmember_edit\";s:3:\"yes\";s:14:\"lmember_delete\";s:3:\"yes\";s:12:\"lmember_view\";s:3:\"yes\";s:4:\"book\";s:3:\"yes\";s:8:\"book_add\";s:3:\"yes\";s:9:\"book_edit\";s:3:\"yes\";s:11:\"book_delete\";s:3:\"yes\";s:5:\"issue\";s:3:\"yes\";s:9:\"issue_add\";s:3:\"yes\";s:10:\"issue_edit\";s:3:\"yes\";s:10:\"issue_view\";s:3:\"yes\";s:6:\"ebooks\";s:3:\"yes\";s:10:\"ebooks_add\";s:3:\"yes\";s:11:\"ebooks_edit\";s:3:\"yes\";s:13:\"ebooks_delete\";s:3:\"yes\";s:11:\"ebooks_view\";s:3:\"yes\";s:9:\"transport\";s:3:\"yes\";s:13:\"transport_add\";s:3:\"yes\";s:14:\"transport_edit\";s:3:\"yes\";s:16:\"transport_delete\";s:3:\"yes\";s:7:\"tmember\";s:3:\"yes\";s:11:\"tmember_add\";s:3:\"yes\";s:12:\"tmember_edit\";s:3:\"yes\";s:14:\"tmember_delete\";s:3:\"yes\";s:12:\"tmember_view\";s:3:\"yes\";s:6:\"hostel\";s:3:\"yes\";s:10:\"hostel_add\";s:3:\"yes\";s:11:\"hostel_edit\";s:3:\"yes\";s:13:\"hostel_delete\";s:3:\"yes\";s:8:\"category\";s:3:\"yes\";s:12:\"category_add\";s:3:\"yes\";s:13:\"category_edit\";s:3:\"yes\";s:15:\"category_delete\";s:3:\"yes\";s:7:\"hmember\";s:3:\"yes\";s:11:\"hmember_add\";s:3:\"yes\";s:12:\"hmember_edit\";s:3:\"yes\";s:14:\"hmember_delete\";s:3:\"yes\";s:12:\"hmember_view\";s:3:\"yes\";s:8:\"feetypes\";s:3:\"yes\";s:12:\"feetypes_add\";s:3:\"yes\";s:13:\"feetypes_edit\";s:3:\"yes\";s:15:\"feetypes_delete\";s:3:\"yes\";s:7:\"invoice\";s:3:\"yes\";s:11:\"invoice_add\";s:3:\"yes\";s:12:\"invoice_edit\";s:3:\"yes\";s:14:\"invoice_delete\";s:3:\"yes\";s:12:\"invoice_view\";s:3:\"yes\";s:14:\"paymenthistory\";s:3:\"yes\";s:19:\"paymenthistory_edit\";s:3:\"yes\";s:21:\"paymenthistory_delete\";s:3:\"yes\";s:7:\"expense\";s:3:\"yes\";s:11:\"expense_add\";s:3:\"yes\";s:12:\"expense_edit\";s:3:\"yes\";s:14:\"expense_delete\";s:3:\"yes\";s:6:\"income\";s:3:\"yes\";s:10:\"income_add\";s:3:\"yes\";s:11:\"income_edit\";s:3:\"yes\";s:13:\"income_delete\";s:3:\"yes\";s:14:\"global_payment\";s:3:\"yes\";s:6:\"notice\";s:3:\"yes\";s:10:\"notice_add\";s:3:\"yes\";s:11:\"notice_edit\";s:3:\"yes\";s:13:\"notice_delete\";s:3:\"yes\";s:11:\"notice_view\";s:3:\"yes\";s:5:\"event\";s:3:\"yes\";s:9:\"event_add\";s:3:\"yes\";s:10:\"event_edit\";s:3:\"yes\";s:12:\"event_delete\";s:3:\"yes\";s:10:\"event_view\";s:3:\"yes\";s:7:\"holiday\";s:3:\"yes\";s:11:\"holiday_add\";s:3:\"yes\";s:12:\"holiday_edit\";s:3:\"yes\";s:14:\"holiday_delete\";s:3:\"yes\";s:12:\"holiday_view\";s:3:\"yes\";s:13:\"classesreport\";s:3:\"yes\";s:13:\"studentreport\";s:3:\"yes\";s:12:\"idcardreport\";s:3:\"yes\";s:15:\"admitcardreport\";s:3:\"yes\";s:13:\"routinereport\";s:3:\"yes\";s:18:\"examschedulereport\";s:3:\"yes\";s:16:\"attendancereport\";s:3:\"yes\";s:24:\"attendanceoverviewreport\";s:3:\"yes\";s:18:\"librarybooksreport\";s:3:\"yes\";s:17:\"librarycardreport\";s:3:\"yes\";s:22:\"librarybookissuereport\";s:3:\"yes\";s:14:\"terminalreport\";s:3:\"yes\";s:16:\"meritstagereport\";s:3:\"yes\";s:21:\"tabulationsheetreport\";s:3:\"yes\";s:15:\"marksheetreport\";s:3:\"yes\";s:18:\"progresscardreport\";s:3:\"yes\";s:20:\"studentsessionreport\";s:3:\"yes\";s:16:\"onlineexamreport\";s:3:\"yes\";s:24:\"onlineexamquestionreport\";s:3:\"yes\";s:21:\"onlineadmissionreport\";s:3:\"yes\";s:17:\"certificatereport\";s:3:\"yes\";s:22:\"leaveapplicationreport\";s:3:\"yes\";s:21:\"productpurchasereport\";s:3:\"yes\";s:17:\"productsalereport\";s:3:\"yes\";s:23:\"searchpaymentfeesreport\";s:3:\"yes\";s:10:\"feesreport\";s:3:\"yes\";s:13:\"duefeesreport\";s:3:\"yes\";s:17:\"balancefeesreport\";s:3:\"yes\";s:17:\"transactionreport\";s:3:\"yes\";s:17:\"studentfinereport\";s:3:\"yes\";s:12:\"salaryreport\";s:3:\"yes\";s:19:\"accountledgerreport\";s:3:\"yes\";s:15:\"onlineadmission\";s:3:\"yes\";s:11:\"visitorinfo\";s:3:\"yes\";s:18:\"visitorinfo_delete\";s:3:\"yes\";s:16:\"visitorinfo_view\";s:3:\"yes\";s:10:\"schoolyear\";s:3:\"yes\";s:14:\"schoolyear_add\";s:3:\"yes\";s:15:\"schoolyear_edit\";s:3:\"yes\";s:17:\"schoolyear_delete\";s:3:\"yes\";s:12:\"studentgroup\";s:3:\"yes\";s:16:\"studentgroup_add\";s:3:\"yes\";s:17:\"studentgroup_edit\";s:3:\"yes\";s:19:\"studentgroup_delete\";s:3:\"yes\";s:8:\"complain\";s:3:\"yes\";s:12:\"complain_add\";s:3:\"yes\";s:13:\"complain_edit\";s:3:\"yes\";s:15:\"complain_delete\";s:3:\"yes\";s:13:\"complain_view\";s:3:\"yes\";s:20:\"certificate_template\";s:3:\"yes\";s:24:\"certificate_template_add\";s:3:\"yes\";s:25:\"certificate_template_edit\";s:3:\"yes\";s:27:\"certificate_template_delete\";s:3:\"yes\";s:25:\"certificate_template_view\";s:3:\"yes\";s:11:\"systemadmin\";s:3:\"yes\";s:15:\"systemadmin_add\";s:3:\"yes\";s:16:\"systemadmin_edit\";s:3:\"yes\";s:18:\"systemadmin_delete\";s:3:\"yes\";s:16:\"systemadmin_view\";s:3:\"yes\";s:13:\"resetpassword\";s:3:\"yes\";s:10:\"sociallink\";s:3:\"yes\";s:14:\"sociallink_add\";s:3:\"yes\";s:15:\"sociallink_edit\";s:3:\"yes\";s:17:\"sociallink_delete\";s:3:\"yes\";s:18:\"mailandsmstemplate\";s:3:\"yes\";s:22:\"mailandsmstemplate_add\";s:3:\"yes\";s:23:\"mailandsmstemplate_edit\";s:3:\"yes\";s:25:\"mailandsmstemplate_delete\";s:3:\"yes\";s:23:\"mailandsmstemplate_view\";s:3:\"yes\";s:10:\"bulkimport\";s:3:\"yes\";s:6:\"backup\";s:3:\"yes\";s:8:\"usertype\";s:3:\"yes\";s:12:\"usertype_add\";s:3:\"yes\";s:13:\"usertype_edit\";s:3:\"yes\";s:15:\"usertype_delete\";s:3:\"yes\";s:10:\"permission\";s:3:\"yes\";s:6:\"update\";s:3:\"yes\";s:16:\"posts_categories\";s:3:\"yes\";s:20:\"posts_categories_add\";s:3:\"yes\";s:21:\"posts_categories_edit\";s:3:\"yes\";s:23:\"posts_categories_delete\";s:3:\"yes\";s:5:\"posts\";s:3:\"yes\";s:9:\"posts_add\";s:3:\"yes\";s:10:\"posts_edit\";s:3:\"yes\";s:12:\"posts_delete\";s:3:\"yes\";s:5:\"pages\";s:3:\"yes\";s:9:\"pages_add\";s:3:\"yes\";s:10:\"pages_edit\";s:3:\"yes\";s:12:\"pages_delete\";s:3:\"yes\";s:12:\"frontendmenu\";s:3:\"yes\";s:7:\"setting\";s:3:\"yes\";s:16:\"frontend_setting\";s:3:\"yes\";s:15:\"paymentsettings\";s:3:\"yes\";s:11:\"smssettings\";s:3:\"yes\";s:12:\"emailsetting\";s:3:\"yes\";s:11:\"marksetting\";s:3:\"yes\";s:9:\"take_exam\";s:3:\"yes\";}'),('a886b7947e2d72acfc84fbe13a7e204cfa3cb9c7','49.36.52.8',1580148783,_binary '__ci_last_regenerate|i:1580148783;english|N;loginuserID|s:1:\"1\";name|s:6:\"vishal\";email|s:26:\"support@blitzsoftwares.com\";usertypeID|s:1:\"1\";usertype|s:5:\"Admin\";username|s:6:\"vishal\";photo|s:11:\"default.png\";lang|s:7:\"english\";defaultschoolyearID|s:1:\"1\";varifyvaliduser|b:1;loggedin|b:1;get_permission|b:1;master_permission_set|a:372:{s:9:\"dashboard\";s:3:\"yes\";s:7:\"student\";s:3:\"yes\";s:11:\"student_add\";s:3:\"yes\";s:12:\"student_edit\";s:3:\"yes\";s:14:\"student_delete\";s:3:\"yes\";s:12:\"student_view\";s:3:\"yes\";s:7:\"parents\";s:3:\"yes\";s:11:\"parents_add\";s:3:\"yes\";s:12:\"parents_edit\";s:3:\"yes\";s:14:\"parents_delete\";s:3:\"yes\";s:12:\"parents_view\";s:3:\"yes\";s:7:\"teacher\";s:3:\"yes\";s:11:\"teacher_add\";s:3:\"yes\";s:12:\"teacher_edit\";s:3:\"yes\";s:14:\"teacher_delete\";s:3:\"yes\";s:12:\"teacher_view\";s:3:\"yes\";s:4:\"user\";s:3:\"yes\";s:8:\"user_add\";s:3:\"yes\";s:9:\"user_edit\";s:3:\"yes\";s:11:\"user_delete\";s:3:\"yes\";s:9:\"user_view\";s:3:\"yes\";s:7:\"classes\";s:3:\"yes\";s:11:\"classes_add\";s:3:\"yes\";s:12:\"classes_edit\";s:3:\"yes\";s:14:\"classes_delete\";s:3:\"yes\";s:7:\"section\";s:3:\"yes\";s:11:\"section_add\";s:3:\"yes\";s:12:\"section_edit\";s:3:\"yes\";s:15:\"semester_delete\";s:3:\"yes\";s:14:\"section_delete\";s:3:\"yes\";s:7:\"subject\";s:3:\"yes\";s:11:\"subject_add\";s:3:\"yes\";s:12:\"subject_edit\";s:3:\"yes\";s:14:\"subject_delete\";s:3:\"yes\";s:8:\"syllabus\";s:3:\"yes\";s:12:\"syllabus_add\";s:3:\"yes\";s:13:\"syllabus_edit\";s:3:\"yes\";s:15:\"syllabus_delete\";s:3:\"yes\";s:10:\"assignment\";s:3:\"yes\";s:14:\"assignment_add\";s:3:\"yes\";s:15:\"assignment_edit\";s:3:\"yes\";s:17:\"assignment_delete\";s:3:\"yes\";s:15:\"assignment_view\";s:3:\"yes\";s:7:\"routine\";s:3:\"yes\";s:11:\"routine_add\";s:3:\"yes\";s:12:\"routine_edit\";s:3:\"yes\";s:14:\"routine_delete\";s:3:\"yes\";s:11:\"sattendance\";s:3:\"yes\";s:15:\"sattendance_add\";s:3:\"yes\";s:16:\"sattendance_view\";s:3:\"yes\";s:11:\"tattendance\";s:3:\"yes\";s:15:\"tattendance_add\";s:3:\"yes\";s:16:\"tattendance_view\";s:3:\"yes\";s:11:\"uattendance\";s:3:\"yes\";s:15:\"uattendance_add\";s:3:\"yes\";s:16:\"uattendance_view\";s:3:\"yes\";s:4:\"exam\";s:3:\"yes\";s:8:\"exam_add\";s:3:\"yes\";s:9:\"exam_edit\";s:3:\"yes\";s:11:\"exam_delete\";s:3:\"yes\";s:12:\"examschedule\";s:3:\"yes\";s:16:\"examschedule_add\";s:3:\"yes\";s:17:\"examschedule_edit\";s:3:\"yes\";s:19:\"examschedule_delete\";s:3:\"yes\";s:5:\"grade\";s:3:\"yes\";s:9:\"grade_add\";s:3:\"yes\";s:10:\"grade_edit\";s:3:\"yes\";s:12:\"grade_delete\";s:3:\"yes\";s:11:\"eattendance\";s:3:\"yes\";s:15:\"eattendance_add\";s:3:\"yes\";s:4:\"mark\";s:3:\"yes\";s:8:\"mark_add\";s:3:\"yes\";s:9:\"mark_view\";s:3:\"yes\";s:14:\"markpercentage\";s:3:\"yes\";s:18:\"markpercentage_add\";s:3:\"yes\";s:19:\"markpercentage_edit\";s:3:\"yes\";s:21:\"markpercentage_delete\";s:3:\"yes\";s:9:\"promotion\";s:3:\"yes\";s:12:\"conversation\";s:3:\"yes\";s:5:\"media\";s:3:\"yes\";s:9:\"media_add\";s:3:\"yes\";s:12:\"media_delete\";s:3:\"yes\";s:10:\"mailandsms\";s:3:\"yes\";s:14:\"mailandsms_add\";s:3:\"yes\";s:15:\"mailandsms_view\";s:3:\"yes\";s:14:\"question_group\";s:3:\"yes\";s:18:\"question_group_add\";s:3:\"yes\";s:19:\"question_group_edit\";s:3:\"yes\";s:21:\"question_group_delete\";s:3:\"yes\";s:14:\"question_level\";s:3:\"yes\";s:18:\"question_level_add\";s:3:\"yes\";s:19:\"question_level_edit\";s:3:\"yes\";s:21:\"question_level_delete\";s:3:\"yes\";s:13:\"question_bank\";s:3:\"yes\";s:17:\"question_bank_add\";s:3:\"yes\";s:18:\"question_bank_edit\";s:3:\"yes\";s:20:\"question_bank_delete\";s:3:\"yes\";s:18:\"question_bank_view\";s:3:\"yes\";s:11:\"online_exam\";s:3:\"yes\";s:15:\"online_exam_add\";s:3:\"yes\";s:16:\"online_exam_edit\";s:3:\"yes\";s:18:\"online_exam_delete\";s:3:\"yes\";s:11:\"instruction\";s:3:\"yes\";s:15:\"instruction_add\";s:3:\"yes\";s:16:\"instruction_edit\";s:3:\"yes\";s:18:\"instruction_delete\";s:3:\"yes\";s:16:\"instruction_view\";s:3:\"yes\";s:15:\"salary_template\";s:3:\"yes\";s:19:\"salary_template_add\";s:3:\"yes\";s:20:\"salary_template_edit\";s:3:\"yes\";s:22:\"salary_template_delete\";s:3:\"yes\";s:20:\"salary_template_view\";s:3:\"yes\";s:15:\"hourly_template\";s:3:\"yes\";s:19:\"hourly_template_add\";s:3:\"yes\";s:20:\"hourly_template_edit\";s:3:\"yes\";s:22:\"hourly_template_delete\";s:3:\"yes\";s:13:\"manage_salary\";s:3:\"yes\";s:17:\"manage_salary_add\";s:3:\"yes\";s:18:\"manage_salary_edit\";s:3:\"yes\";s:20:\"manage_salary_delete\";s:3:\"yes\";s:18:\"manage_salary_view\";s:3:\"yes\";s:12:\"make_payment\";s:3:\"yes\";s:6:\"vendor\";s:3:\"yes\";s:10:\"vendor_add\";s:3:\"yes\";s:11:\"vendor_edit\";s:3:\"yes\";s:13:\"vendor_delete\";s:3:\"yes\";s:8:\"location\";s:3:\"yes\";s:12:\"location_add\";s:3:\"yes\";s:13:\"location_edit\";s:3:\"yes\";s:15:\"location_delete\";s:3:\"yes\";s:14:\"asset_category\";s:3:\"yes\";s:18:\"asset_category_add\";s:3:\"yes\";s:19:\"asset_category_edit\";s:3:\"yes\";s:21:\"asset_category_delete\";s:3:\"yes\";s:5:\"asset\";s:3:\"yes\";s:9:\"asset_add\";s:3:\"yes\";s:10:\"asset_edit\";s:3:\"yes\";s:12:\"asset_delete\";s:3:\"yes\";s:10:\"asset_view\";s:3:\"yes\";s:16:\"asset_assignment\";s:3:\"yes\";s:20:\"asset_assignment_add\";s:3:\"yes\";s:21:\"asset_assignment_edit\";s:3:\"yes\";s:23:\"asset_assignment_delete\";s:3:\"yes\";s:21:\"asset_assignment_view\";s:3:\"yes\";s:8:\"purchase\";s:3:\"yes\";s:12:\"purchase_add\";s:3:\"yes\";s:13:\"purchase_edit\";s:3:\"yes\";s:15:\"purchase_delete\";s:3:\"yes\";s:15:\"productcategory\";s:3:\"yes\";s:19:\"productcategory_add\";s:3:\"yes\";s:20:\"productcategory_edit\";s:3:\"yes\";s:22:\"productcategory_delete\";s:3:\"yes\";s:7:\"product\";s:3:\"yes\";s:11:\"product_add\";s:3:\"yes\";s:12:\"product_edit\";s:3:\"yes\";s:14:\"product_delete\";s:3:\"yes\";s:16:\"productwarehouse\";s:3:\"yes\";s:20:\"productwarehouse_add\";s:3:\"yes\";s:21:\"productwarehouse_edit\";s:3:\"yes\";s:23:\"productwarehouse_delete\";s:3:\"yes\";s:15:\"productsupplier\";s:3:\"yes\";s:19:\"productsupplier_add\";s:3:\"yes\";s:20:\"productsupplier_edit\";s:3:\"yes\";s:22:\"productsupplier_delete\";s:3:\"yes\";s:15:\"productpurchase\";s:3:\"yes\";s:19:\"productpurchase_add\";s:3:\"yes\";s:20:\"productpurchase_edit\";s:3:\"yes\";s:22:\"productpurchase_delete\";s:3:\"yes\";s:20:\"productpurchase_view\";s:3:\"yes\";s:11:\"productsale\";s:3:\"yes\";s:15:\"productsale_add\";s:3:\"yes\";s:16:\"productsale_edit\";s:3:\"yes\";s:18:\"productsale_delete\";s:3:\"yes\";s:16:\"productsale_view\";s:3:\"yes\";s:13:\"leavecategory\";s:3:\"yes\";s:17:\"leavecategory_add\";s:3:\"yes\";s:18:\"leavecategory_edit\";s:3:\"yes\";s:20:\"leavecategory_delete\";s:3:\"yes\";s:11:\"leaveassign\";s:3:\"yes\";s:15:\"leaveassign_add\";s:3:\"yes\";s:16:\"leaveassign_edit\";s:3:\"yes\";s:18:\"leaveassign_delete\";s:3:\"yes\";s:10:\"leaveapply\";s:3:\"yes\";s:14:\"leaveapply_add\";s:3:\"yes\";s:15:\"leaveapply_edit\";s:3:\"yes\";s:17:\"leaveapply_delete\";s:3:\"yes\";s:15:\"leaveapply_view\";s:3:\"yes\";s:16:\"leaveapplication\";s:3:\"yes\";s:18:\"activitiescategory\";s:3:\"yes\";s:22:\"activitiescategory_add\";s:3:\"yes\";s:23:\"activitiescategory_edit\";s:3:\"yes\";s:25:\"activitiescategory_delete\";s:3:\"yes\";s:10:\"activities\";s:3:\"yes\";s:14:\"activities_add\";s:3:\"yes\";s:17:\"activities_delete\";s:3:\"yes\";s:9:\"childcare\";s:3:\"yes\";s:13:\"childcare_add\";s:3:\"yes\";s:14:\"childcare_edit\";s:3:\"yes\";s:16:\"childcare_delete\";s:3:\"yes\";s:7:\"lmember\";s:3:\"yes\";s:11:\"lmember_add\";s:3:\"yes\";s:12:\"lmember_edit\";s:3:\"yes\";s:14:\"lmember_delete\";s:3:\"yes\";s:12:\"lmember_view\";s:3:\"yes\";s:4:\"book\";s:3:\"yes\";s:8:\"book_add\";s:3:\"yes\";s:9:\"book_edit\";s:3:\"yes\";s:11:\"book_delete\";s:3:\"yes\";s:5:\"issue\";s:3:\"yes\";s:9:\"issue_add\";s:3:\"yes\";s:10:\"issue_edit\";s:3:\"yes\";s:10:\"issue_view\";s:3:\"yes\";s:6:\"ebooks\";s:3:\"yes\";s:10:\"ebooks_add\";s:3:\"yes\";s:11:\"ebooks_edit\";s:3:\"yes\";s:13:\"ebooks_delete\";s:3:\"yes\";s:11:\"ebooks_view\";s:3:\"yes\";s:9:\"transport\";s:3:\"yes\";s:13:\"transport_add\";s:3:\"yes\";s:14:\"transport_edit\";s:3:\"yes\";s:16:\"transport_delete\";s:3:\"yes\";s:7:\"tmember\";s:3:\"yes\";s:11:\"tmember_add\";s:3:\"yes\";s:12:\"tmember_edit\";s:3:\"yes\";s:14:\"tmember_delete\";s:3:\"yes\";s:12:\"tmember_view\";s:3:\"yes\";s:6:\"hostel\";s:3:\"yes\";s:10:\"hostel_add\";s:3:\"yes\";s:11:\"hostel_edit\";s:3:\"yes\";s:13:\"hostel_delete\";s:3:\"yes\";s:8:\"category\";s:3:\"yes\";s:12:\"category_add\";s:3:\"yes\";s:13:\"category_edit\";s:3:\"yes\";s:15:\"category_delete\";s:3:\"yes\";s:7:\"hmember\";s:3:\"yes\";s:11:\"hmember_add\";s:3:\"yes\";s:12:\"hmember_edit\";s:3:\"yes\";s:14:\"hmember_delete\";s:3:\"yes\";s:12:\"hmember_view\";s:3:\"yes\";s:8:\"feetypes\";s:3:\"yes\";s:12:\"feetypes_add\";s:3:\"yes\";s:13:\"feetypes_edit\";s:3:\"yes\";s:15:\"feetypes_delete\";s:3:\"yes\";s:7:\"invoice\";s:3:\"yes\";s:11:\"invoice_add\";s:3:\"yes\";s:12:\"invoice_edit\";s:3:\"yes\";s:14:\"invoice_delete\";s:3:\"yes\";s:12:\"invoice_view\";s:3:\"yes\";s:14:\"paymenthistory\";s:3:\"yes\";s:19:\"paymenthistory_edit\";s:3:\"yes\";s:21:\"paymenthistory_delete\";s:3:\"yes\";s:7:\"expense\";s:3:\"yes\";s:11:\"expense_add\";s:3:\"yes\";s:12:\"expense_edit\";s:3:\"yes\";s:14:\"expense_delete\";s:3:\"yes\";s:6:\"income\";s:3:\"yes\";s:10:\"income_add\";s:3:\"yes\";s:11:\"income_edit\";s:3:\"yes\";s:13:\"income_delete\";s:3:\"yes\";s:14:\"global_payment\";s:3:\"yes\";s:6:\"notice\";s:3:\"yes\";s:10:\"notice_add\";s:3:\"yes\";s:11:\"notice_edit\";s:3:\"yes\";s:13:\"notice_delete\";s:3:\"yes\";s:11:\"notice_view\";s:3:\"yes\";s:5:\"event\";s:3:\"yes\";s:9:\"event_add\";s:3:\"yes\";s:10:\"event_edit\";s:3:\"yes\";s:12:\"event_delete\";s:3:\"yes\";s:10:\"event_view\";s:3:\"yes\";s:7:\"holiday\";s:3:\"yes\";s:11:\"holiday_add\";s:3:\"yes\";s:12:\"holiday_edit\";s:3:\"yes\";s:14:\"holiday_delete\";s:3:\"yes\";s:12:\"holiday_view\";s:3:\"yes\";s:13:\"classesreport\";s:3:\"yes\";s:13:\"studentreport\";s:3:\"yes\";s:12:\"idcardreport\";s:3:\"yes\";s:15:\"admitcardreport\";s:3:\"yes\";s:13:\"routinereport\";s:3:\"yes\";s:18:\"examschedulereport\";s:3:\"yes\";s:16:\"attendancereport\";s:3:\"yes\";s:24:\"attendanceoverviewreport\";s:3:\"yes\";s:18:\"librarybooksreport\";s:3:\"yes\";s:17:\"librarycardreport\";s:3:\"yes\";s:22:\"librarybookissuereport\";s:3:\"yes\";s:14:\"terminalreport\";s:3:\"yes\";s:16:\"meritstagereport\";s:3:\"yes\";s:21:\"tabulationsheetreport\";s:3:\"yes\";s:15:\"marksheetreport\";s:3:\"yes\";s:18:\"progresscardreport\";s:3:\"yes\";s:20:\"studentsessionreport\";s:3:\"yes\";s:16:\"onlineexamreport\";s:3:\"yes\";s:24:\"onlineexamquestionreport\";s:3:\"yes\";s:21:\"onlineadmissionreport\";s:3:\"yes\";s:17:\"certificatereport\";s:3:\"yes\";s:22:\"leaveapplicationreport\";s:3:\"yes\";s:21:\"productpurchasereport\";s:3:\"yes\";s:17:\"productsalereport\";s:3:\"yes\";s:23:\"searchpaymentfeesreport\";s:3:\"yes\";s:10:\"feesreport\";s:3:\"yes\";s:13:\"duefeesreport\";s:3:\"yes\";s:17:\"balancefeesreport\";s:3:\"yes\";s:17:\"transactionreport\";s:3:\"yes\";s:17:\"studentfinereport\";s:3:\"yes\";s:12:\"salaryreport\";s:3:\"yes\";s:19:\"accountledgerreport\";s:3:\"yes\";s:15:\"onlineadmission\";s:3:\"yes\";s:11:\"visitorinfo\";s:3:\"yes\";s:18:\"visitorinfo_delete\";s:3:\"yes\";s:16:\"visitorinfo_view\";s:3:\"yes\";s:10:\"schoolyear\";s:3:\"yes\";s:14:\"schoolyear_add\";s:3:\"yes\";s:15:\"schoolyear_edit\";s:3:\"yes\";s:17:\"schoolyear_delete\";s:3:\"yes\";s:12:\"studentgroup\";s:3:\"yes\";s:16:\"studentgroup_add\";s:3:\"yes\";s:17:\"studentgroup_edit\";s:3:\"yes\";s:19:\"studentgroup_delete\";s:3:\"yes\";s:8:\"complain\";s:3:\"yes\";s:12:\"complain_add\";s:3:\"yes\";s:13:\"complain_edit\";s:3:\"yes\";s:15:\"complain_delete\";s:3:\"yes\";s:13:\"complain_view\";s:3:\"yes\";s:20:\"certificate_template\";s:3:\"yes\";s:24:\"certificate_template_add\";s:3:\"yes\";s:25:\"certificate_template_edit\";s:3:\"yes\";s:27:\"certificate_template_delete\";s:3:\"yes\";s:25:\"certificate_template_view\";s:3:\"yes\";s:11:\"systemadmin\";s:3:\"yes\";s:15:\"systemadmin_add\";s:3:\"yes\";s:16:\"systemadmin_edit\";s:3:\"yes\";s:18:\"systemadmin_delete\";s:3:\"yes\";s:16:\"systemadmin_view\";s:3:\"yes\";s:13:\"resetpassword\";s:3:\"yes\";s:10:\"sociallink\";s:3:\"yes\";s:14:\"sociallink_add\";s:3:\"yes\";s:15:\"sociallink_edit\";s:3:\"yes\";s:17:\"sociallink_delete\";s:3:\"yes\";s:18:\"mailandsmstemplate\";s:3:\"yes\";s:22:\"mailandsmstemplate_add\";s:3:\"yes\";s:23:\"mailandsmstemplate_edit\";s:3:\"yes\";s:25:\"mailandsmstemplate_delete\";s:3:\"yes\";s:23:\"mailandsmstemplate_view\";s:3:\"yes\";s:10:\"bulkimport\";s:3:\"yes\";s:6:\"backup\";s:3:\"yes\";s:8:\"usertype\";s:3:\"yes\";s:12:\"usertype_add\";s:3:\"yes\";s:13:\"usertype_edit\";s:3:\"yes\";s:15:\"usertype_delete\";s:3:\"yes\";s:10:\"permission\";s:3:\"yes\";s:6:\"update\";s:3:\"yes\";s:16:\"posts_categories\";s:3:\"yes\";s:20:\"posts_categories_add\";s:3:\"yes\";s:21:\"posts_categories_edit\";s:3:\"yes\";s:23:\"posts_categories_delete\";s:3:\"yes\";s:5:\"posts\";s:3:\"yes\";s:9:\"posts_add\";s:3:\"yes\";s:10:\"posts_edit\";s:3:\"yes\";s:12:\"posts_delete\";s:3:\"yes\";s:5:\"pages\";s:3:\"yes\";s:9:\"pages_add\";s:3:\"yes\";s:10:\"pages_edit\";s:3:\"yes\";s:12:\"pages_delete\";s:3:\"yes\";s:12:\"frontendmenu\";s:3:\"yes\";s:7:\"setting\";s:3:\"yes\";s:16:\"frontend_setting\";s:3:\"yes\";s:15:\"paymentsettings\";s:3:\"yes\";s:11:\"smssettings\";s:3:\"yes\";s:12:\"emailsetting\";s:3:\"yes\";s:11:\"marksetting\";s:3:\"yes\";s:9:\"take_exam\";s:3:\"yes\";}'),('35bacbdce91caa990285007c096ba9451ad24483','157.245.152.131',1580148628,_binary '__ci_last_regenerate|i:1580148608;'),('20484974ee591420a795970a30c2da4aae7fdd56','49.36.52.8',1580148793,_binary '__ci_last_regenerate|i:1580148793;'),('1f90ee7e7d4ca56eea3e4a8ed1d2c5a6d560ac86','194.33.45.137',1580549288,_binary '__ci_last_regenerate|i:1580549277;');
/*!40000 ALTER TABLE `school_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schoolyear`
--

DROP TABLE IF EXISTS `schoolyear`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `schoolyear` (
  `schoolyearID` int(11) NOT NULL AUTO_INCREMENT,
  `schooltype` varchar(40) DEFAULT NULL,
  `schoolyear` varchar(128) NOT NULL,
  `schoolyeartitle` varchar(128) DEFAULT NULL,
  `startingdate` date NOT NULL,
  `endingdate` date NOT NULL,
  `semestercode` int(11) DEFAULT NULL,
  `create_date` datetime NOT NULL,
  `modify_date` datetime NOT NULL,
  `create_userID` int(11) NOT NULL,
  `create_username` varchar(100) NOT NULL,
  `create_usertype` varchar(100) NOT NULL,
  PRIMARY KEY (`schoolyearID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schoolyear`
--

LOCK TABLES `schoolyear` WRITE;
/*!40000 ALTER TABLE `schoolyear` DISABLE KEYS */;
INSERT INTO `schoolyear` VALUES (1,'classbase','2019-2020','','2019-01-01','2019-12-31',NULL,'2019-01-01 12:35:25','2019-11-10 07:21:11',1,'admin','Admin');
/*!40000 ALTER TABLE `schoolyear` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `section`
--

DROP TABLE IF EXISTS `section`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `section` (
  `sectionID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `section` varchar(60) NOT NULL,
  `category` varchar(128) NOT NULL,
  `capacity` int(11) NOT NULL,
  `classesID` int(11) NOT NULL,
  `teacherID` int(11) NOT NULL,
  `note` text,
  `create_date` datetime NOT NULL,
  `modify_date` datetime NOT NULL,
  `create_userID` int(11) NOT NULL,
  `create_username` varchar(60) NOT NULL,
  `create_usertype` varchar(60) NOT NULL,
  PRIMARY KEY (`sectionID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `section`
--

LOCK TABLES `section` WRITE;
/*!40000 ALTER TABLE `section` DISABLE KEYS */;
/*!40000 ALTER TABLE `section` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `setting`
--

DROP TABLE IF EXISTS `setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `setting` (
  `fieldoption` varchar(100) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`fieldoption`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `setting`
--

LOCK TABLES `setting` WRITE;
/*!40000 ALTER TABLE `setting` DISABLE KEYS */;
INSERT INTO `setting` VALUES ('absent_auto_sms','1'),('address','Pune'),('attendance','day'),('attendance_notification','none'),('attendance_notification_template','0'),('attendance_smsgateway','0'),('automation','5'),('auto_invoice_generate','0'),('auto_update_notification','0'),('backend_theme','default'),('captcha_status','1'),('currency_code','inr'),('currency_symbol','₹'),('email','support@blitzsoftwares.com'),('ex_class','0'),('footer','Copyright &copy; Blitzschool Demo'),('frontendorbackend','YES'),('frontend_theme','default'),('google_analytics',''),('language','english'),('language_status','0'),('marktypeID','0'),('mark_1','1'),('note','1'),('phone','+919371145666'),('photo','fc5d14198dc1ac7f4ac7e5a00c186be03c77d3d8c59808228d2adea8a5a72b96da6005c01941b7f2e92073fc8e2eda66031b0c13c6842534a037a9b6702d2447.png'),('profile_edit','0'),('purchase_code','c8adb153-c7c6-42ba-8307-f50d0e5bfa2b'),('purchase_username','punetotal'),('recaptcha_secret_key',''),('recaptcha_site_key',''),('school_type','classbase'),('school_year','1'),('sname','Blitzschool Demo'),('student_ID_format','1'),('time_zone','Asia/Kolkata'),('updateversion','4.3'),('weekends','');
/*!40000 ALTER TABLE `setting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `slider`
--

DROP TABLE IF EXISTS `slider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `slider` (
  `sliderID` int(11) NOT NULL AUTO_INCREMENT,
  `pagesID` int(11) NOT NULL,
  `slider` int(11) NOT NULL,
  PRIMARY KEY (`sliderID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `slider`
--

LOCK TABLES `slider` WRITE;
/*!40000 ALTER TABLE `slider` DISABLE KEYS */;
/*!40000 ALTER TABLE `slider` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `smssettings`
--

DROP TABLE IF EXISTS `smssettings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `smssettings` (
  `smssettingsID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `types` varchar(255) DEFAULT NULL,
  `field_names` varchar(255) DEFAULT NULL,
  `field_values` varchar(255) DEFAULT NULL,
  `smssettings_extra` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`smssettingsID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `smssettings`
--

LOCK TABLES `smssettings` WRITE;
/*!40000 ALTER TABLE `smssettings` DISABLE KEYS */;
INSERT INTO `smssettings` VALUES (1,'clickatell','clickatell_username','',NULL),(2,'clickatell','clickatell_password','',NULL),(3,'clickatell','clickatell_api_key','',NULL),(4,'twilio','twilio_accountSID','',NULL),(5,'twilio','twilio_authtoken','',NULL),(6,'twilio','twilio_fromnumber','',NULL),(7,'bulk','bulk_username','',NULL),(8,'bulk','bulk_password','',NULL),(9,'msg91','msg91_authKey','',NULL),(10,'msg91','msg91_senderID','',NULL);
/*!40000 ALTER TABLE `smssettings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sociallink`
--

DROP TABLE IF EXISTS `sociallink`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `sociallink` (
  `sociallinkID` int(11) NOT NULL AUTO_INCREMENT,
  `usertypeID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `facebook` varchar(200) NOT NULL,
  `twitter` varchar(200) NOT NULL,
  `linkedin` varchar(200) NOT NULL,
  `googleplus` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`sociallinkID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sociallink`
--

LOCK TABLES `sociallink` WRITE;
/*!40000 ALTER TABLE `sociallink` DISABLE KEYS */;
/*!40000 ALTER TABLE `sociallink` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student`
--

DROP TABLE IF EXISTS `student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `student` (
  `studentID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `dob` date DEFAULT NULL,
  `sex` varchar(10) NOT NULL,
  `religion` varchar(25) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `phone` tinytext,
  `address` text,
  `classesID` int(11) NOT NULL,
  `sectionID` int(11) NOT NULL,
  `roll` int(11) NOT NULL,
  `bloodgroup` varchar(5) DEFAULT NULL,
  `country` varchar(128) DEFAULT NULL,
  `registerNO` varchar(128) DEFAULT NULL,
  `state` varchar(128) DEFAULT NULL,
  `library` int(11) NOT NULL,
  `hostel` int(11) NOT NULL,
  `transport` int(11) NOT NULL,
  `photo` varchar(200) DEFAULT NULL,
  `parentID` int(11) DEFAULT NULL,
  `createschoolyearID` int(11) NOT NULL,
  `schoolyearID` int(11) NOT NULL,
  `username` varchar(40) NOT NULL,
  `password` varchar(128) NOT NULL,
  `usertypeID` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `modify_date` datetime NOT NULL,
  `create_userID` int(11) NOT NULL,
  `create_username` varchar(60) NOT NULL,
  `create_usertype` varchar(60) NOT NULL,
  `active` int(11) NOT NULL,
  PRIMARY KEY (`studentID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student`
--

LOCK TABLES `student` WRITE;
/*!40000 ALTER TABLE `student` DISABLE KEYS */;
/*!40000 ALTER TABLE `student` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `studentextend`
--

DROP TABLE IF EXISTS `studentextend`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `studentextend` (
  `studentextendID` int(11) NOT NULL AUTO_INCREMENT,
  `studentID` int(11) NOT NULL,
  `studentgroupID` int(11) NOT NULL,
  `optionalsubjectID` int(11) NOT NULL,
  `extracurricularactivities` text,
  `remarks` text,
  PRIMARY KEY (`studentextendID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `studentextend`
--

LOCK TABLES `studentextend` WRITE;
/*!40000 ALTER TABLE `studentextend` DISABLE KEYS */;
/*!40000 ALTER TABLE `studentextend` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `studentgroup`
--

DROP TABLE IF EXISTS `studentgroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `studentgroup` (
  `studentgroupID` int(11) NOT NULL AUTO_INCREMENT,
  `group` varchar(128) NOT NULL,
  PRIMARY KEY (`studentgroupID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `studentgroup`
--

LOCK TABLES `studentgroup` WRITE;
/*!40000 ALTER TABLE `studentgroup` DISABLE KEYS */;
INSERT INTO `studentgroup` VALUES (1,'Science'),(2,'Arts'),(3,'Commerce');
/*!40000 ALTER TABLE `studentgroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `studentrelation`
--

DROP TABLE IF EXISTS `studentrelation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `studentrelation` (
  `studentrelationID` int(11) NOT NULL AUTO_INCREMENT,
  `srstudentID` int(11) DEFAULT NULL,
  `srname` varchar(40) NOT NULL,
  `srclassesID` int(11) DEFAULT NULL,
  `srclasses` varchar(40) DEFAULT NULL,
  `srroll` int(11) DEFAULT NULL,
  `srregisterNO` varchar(128) DEFAULT NULL,
  `srsectionID` int(11) DEFAULT NULL,
  `srsection` varchar(40) DEFAULT NULL,
  `srstudentgroupID` int(11) NOT NULL,
  `sroptionalsubjectID` int(11) NOT NULL,
  `srschoolyearID` int(11) DEFAULT NULL,
  PRIMARY KEY (`studentrelationID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `studentrelation`
--

LOCK TABLES `studentrelation` WRITE;
/*!40000 ALTER TABLE `studentrelation` DISABLE KEYS */;
/*!40000 ALTER TABLE `studentrelation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sub_attendance`
--

DROP TABLE IF EXISTS `sub_attendance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `sub_attendance` (
  `attendanceID` int(200) unsigned NOT NULL AUTO_INCREMENT,
  `schoolyearID` int(11) NOT NULL,
  `studentID` int(11) NOT NULL,
  `classesID` int(11) NOT NULL,
  `sectionID` int(11) NOT NULL,
  `subjectID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `usertype` varchar(60) NOT NULL,
  `monthyear` varchar(10) NOT NULL,
  `a1` varchar(3) DEFAULT NULL,
  `a2` varchar(3) DEFAULT NULL,
  `a3` varchar(3) DEFAULT NULL,
  `a4` varchar(3) DEFAULT NULL,
  `a5` varchar(3) DEFAULT NULL,
  `a6` varchar(3) DEFAULT NULL,
  `a7` varchar(3) DEFAULT NULL,
  `a8` varchar(3) DEFAULT NULL,
  `a9` varchar(3) DEFAULT NULL,
  `a10` varchar(3) DEFAULT NULL,
  `a11` varchar(3) DEFAULT NULL,
  `a12` varchar(3) DEFAULT NULL,
  `a13` varchar(3) DEFAULT NULL,
  `a14` varchar(3) DEFAULT NULL,
  `a15` varchar(3) DEFAULT NULL,
  `a16` varchar(3) DEFAULT NULL,
  `a17` varchar(3) DEFAULT NULL,
  `a18` varchar(3) DEFAULT NULL,
  `a19` varchar(3) DEFAULT NULL,
  `a20` varchar(3) DEFAULT NULL,
  `a21` varchar(3) DEFAULT NULL,
  `a22` varchar(3) DEFAULT NULL,
  `a23` varchar(3) DEFAULT NULL,
  `a24` varchar(3) DEFAULT NULL,
  `a25` varchar(3) DEFAULT NULL,
  `a26` varchar(3) DEFAULT NULL,
  `a27` varchar(3) DEFAULT NULL,
  `a28` varchar(3) DEFAULT NULL,
  `a29` varchar(3) DEFAULT NULL,
  `a30` varchar(3) DEFAULT NULL,
  `a31` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`attendanceID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sub_attendance`
--

LOCK TABLES `sub_attendance` WRITE;
/*!40000 ALTER TABLE `sub_attendance` DISABLE KEYS */;
/*!40000 ALTER TABLE `sub_attendance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subject`
--

DROP TABLE IF EXISTS `subject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `subject` (
  `subjectID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `classesID` int(11) NOT NULL,
  `type` int(100) NOT NULL,
  `passmark` int(11) NOT NULL,
  `finalmark` int(11) NOT NULL,
  `subject` varchar(60) NOT NULL,
  `subject_author` varchar(100) DEFAULT NULL,
  `subject_code` tinytext NOT NULL,
  `teacher_name` varchar(60) NOT NULL,
  `create_date` datetime NOT NULL,
  `modify_date` datetime NOT NULL,
  `create_userID` int(11) NOT NULL,
  `create_username` varchar(60) NOT NULL,
  `create_usertype` varchar(60) NOT NULL,
  PRIMARY KEY (`subjectID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subject`
--

LOCK TABLES `subject` WRITE;
/*!40000 ALTER TABLE `subject` DISABLE KEYS */;
/*!40000 ALTER TABLE `subject` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subjectteacher`
--

DROP TABLE IF EXISTS `subjectteacher`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `subjectteacher` (
  `subjectteacherID` int(11) NOT NULL AUTO_INCREMENT,
  `subjectID` int(11) NOT NULL,
  `classesID` int(11) NOT NULL,
  `teacherID` int(11) NOT NULL,
  PRIMARY KEY (`subjectteacherID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subjectteacher`
--

LOCK TABLES `subjectteacher` WRITE;
/*!40000 ALTER TABLE `subjectteacher` DISABLE KEYS */;
/*!40000 ALTER TABLE `subjectteacher` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `syllabus`
--

DROP TABLE IF EXISTS `syllabus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `syllabus` (
  `syllabusID` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `description` text,
  `date` date NOT NULL,
  `usertypeID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `originalfile` text NOT NULL,
  `file` text,
  `classesID` int(11) NOT NULL,
  `schoolyearID` int(11) NOT NULL,
  PRIMARY KEY (`syllabusID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `syllabus`
--

LOCK TABLES `syllabus` WRITE;
/*!40000 ALTER TABLE `syllabus` DISABLE KEYS */;
/*!40000 ALTER TABLE `syllabus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `systemadmin`
--

DROP TABLE IF EXISTS `systemadmin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `systemadmin` (
  `systemadminID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `dob` date NOT NULL,
  `sex` varchar(10) NOT NULL,
  `religion` varchar(25) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `phone` tinytext,
  `address` text,
  `jod` date NOT NULL,
  `photo` varchar(200) DEFAULT NULL,
  `username` varchar(40) NOT NULL,
  `password` varchar(128) NOT NULL,
  `usertypeID` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `modify_date` datetime NOT NULL,
  `create_userID` int(11) NOT NULL,
  `create_username` varchar(60) NOT NULL,
  `create_usertype` varchar(60) NOT NULL,
  `active` int(11) NOT NULL,
  `systemadminextra1` varchar(128) DEFAULT NULL,
  `systemadminextra2` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`systemadminID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `systemadmin`
--

LOCK TABLES `systemadmin` WRITE;
/*!40000 ALTER TABLE `systemadmin` DISABLE KEYS */;
INSERT INTO `systemadmin` VALUES (1,'vishal','2019-12-09','Male','Unknown','support@blitzsoftwares.com','','','2019-12-09','default.png','vishal','b7b827f018a1af6e0fba564641e3a7bc5b95f19a346ef31ed240c5fc04ddea06ead5ec2e20e9e29b5be1f95d5ae51b02b935c9a84b726d4a6c98629a116c3ae8',1,'2019-12-09 04:03:27','2019-12-09 04:03:27',0,'vishal','Admin',1,'','');
/*!40000 ALTER TABLE `systemadmin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tattendance`
--

DROP TABLE IF EXISTS `tattendance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tattendance` (
  `tattendanceID` int(200) unsigned NOT NULL AUTO_INCREMENT,
  `schoolyearID` int(11) NOT NULL,
  `teacherID` int(11) NOT NULL,
  `usertypeID` int(11) NOT NULL,
  `monthyear` varchar(10) NOT NULL,
  `a1` varchar(3) DEFAULT NULL,
  `a2` varchar(3) DEFAULT NULL,
  `a3` varchar(3) DEFAULT NULL,
  `a4` varchar(3) DEFAULT NULL,
  `a5` varchar(3) DEFAULT NULL,
  `a6` varchar(3) DEFAULT NULL,
  `a7` varchar(3) DEFAULT NULL,
  `a8` varchar(3) DEFAULT NULL,
  `a9` varchar(3) DEFAULT NULL,
  `a10` varchar(3) DEFAULT NULL,
  `a11` varchar(3) DEFAULT NULL,
  `a12` varchar(3) DEFAULT NULL,
  `a13` varchar(3) DEFAULT NULL,
  `a14` varchar(3) DEFAULT NULL,
  `a15` varchar(3) DEFAULT NULL,
  `a16` varchar(3) DEFAULT NULL,
  `a17` varchar(3) DEFAULT NULL,
  `a18` varchar(3) DEFAULT NULL,
  `a19` varchar(3) DEFAULT NULL,
  `a20` varchar(3) DEFAULT NULL,
  `a21` varchar(3) DEFAULT NULL,
  `a22` varchar(3) DEFAULT NULL,
  `a23` varchar(3) DEFAULT NULL,
  `a24` varchar(3) DEFAULT NULL,
  `a25` varchar(3) DEFAULT NULL,
  `a26` varchar(3) DEFAULT NULL,
  `a27` varchar(3) DEFAULT NULL,
  `a28` varchar(3) DEFAULT NULL,
  `a29` varchar(3) DEFAULT NULL,
  `a30` varchar(3) DEFAULT NULL,
  `a31` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`tattendanceID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tattendance`
--

LOCK TABLES `tattendance` WRITE;
/*!40000 ALTER TABLE `tattendance` DISABLE KEYS */;
/*!40000 ALTER TABLE `tattendance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teacher`
--

DROP TABLE IF EXISTS `teacher`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `teacher` (
  `teacherID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `designation` varchar(128) NOT NULL,
  `dob` date NOT NULL,
  `sex` varchar(10) NOT NULL,
  `religion` varchar(25) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `phone` tinytext,
  `address` text,
  `jod` date NOT NULL,
  `photo` varchar(200) DEFAULT NULL,
  `username` varchar(40) NOT NULL,
  `password` varchar(128) NOT NULL,
  `usertypeID` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `modify_date` datetime NOT NULL,
  `create_userID` int(11) NOT NULL,
  `create_username` varchar(60) NOT NULL,
  `create_usertype` varchar(60) NOT NULL,
  `active` int(11) NOT NULL,
  PRIMARY KEY (`teacherID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teacher`
--

LOCK TABLES `teacher` WRITE;
/*!40000 ALTER TABLE `teacher` DISABLE KEYS */;
/*!40000 ALTER TABLE `teacher` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `themes`
--

DROP TABLE IF EXISTS `themes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `themes` (
  `themesID` int(11) NOT NULL AUTO_INCREMENT,
  `sortID` int(11) NOT NULL DEFAULT '1',
  `themename` varchar(128) NOT NULL,
  `backend` int(11) NOT NULL DEFAULT '1',
  `frontend` int(11) NOT NULL DEFAULT '1',
  `topcolor` text NOT NULL,
  `leftcolor` text NOT NULL,
  PRIMARY KEY (`themesID`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `themes`
--

LOCK TABLES `themes` WRITE;
/*!40000 ALTER TABLE `themes` DISABLE KEYS */;
INSERT INTO `themes` VALUES (1,1,'Default',1,1,'#FFFFFF','#2d353c'),(2,0,'Blue',0,1,'#3c8dbc','#2d353c'),(3,3,'Black',1,1,'#fefefe','#222222'),(4,4,'Purple',1,1,'#605ca8','#2d353c'),(5,5,'Green',1,1,'#00a65a','#2d353c'),(6,6,'Red',1,1,'#dd4b39','#2d353c'),(7,0,'Yellow',0,1,'#f39c12','#2d353c'),(8,7,'Blue Light',1,1,'#3c8dbc','#f9fafc'),(9,8,'Black Light',1,1,'#fefefe','#f9fafc'),(10,9,'Purple Light',1,1,'#605ca8','#f9fafc'),(11,10,'Green Light',1,1,'#00a65a','#f9fafc'),(12,11,'Red Light',1,1,'#dd4b39','#f9fafc'),(13,12,'Yellow Light',1,1,'#f39c12','#f9fafc'),(14,2,'White Blue',1,1,'#ffffff','#132035');
/*!40000 ALTER TABLE `themes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tmember`
--

DROP TABLE IF EXISTS `tmember`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tmember` (
  `tmemberID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `studentID` int(11) NOT NULL,
  `transportID` int(11) NOT NULL,
  `name` varchar(60) NOT NULL,
  `email` varchar(40) DEFAULT NULL,
  `phone` tinytext,
  `tbalance` varchar(11) DEFAULT NULL,
  `tjoindate` date NOT NULL,
  PRIMARY KEY (`tmemberID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tmember`
--

LOCK TABLES `tmember` WRITE;
/*!40000 ALTER TABLE `tmember` DISABLE KEYS */;
/*!40000 ALTER TABLE `tmember` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transport`
--

DROP TABLE IF EXISTS `transport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `transport` (
  `transportID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `route` text NOT NULL,
  `vehicle` int(11) NOT NULL,
  `fare` varchar(11) NOT NULL,
  `note` text,
  PRIMARY KEY (`transportID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transport`
--

LOCK TABLES `transport` WRITE;
/*!40000 ALTER TABLE `transport` DISABLE KEYS */;
/*!40000 ALTER TABLE `transport` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `uattendance`
--

DROP TABLE IF EXISTS `uattendance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `uattendance` (
  `uattendanceID` int(200) unsigned NOT NULL AUTO_INCREMENT,
  `schoolyearID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `usertypeID` int(11) NOT NULL,
  `monthyear` varchar(10) NOT NULL,
  `a1` varchar(3) DEFAULT NULL,
  `a2` varchar(3) DEFAULT NULL,
  `a3` varchar(3) DEFAULT NULL,
  `a4` varchar(3) DEFAULT NULL,
  `a5` varchar(3) DEFAULT NULL,
  `a6` varchar(3) DEFAULT NULL,
  `a7` varchar(3) DEFAULT NULL,
  `a8` varchar(3) DEFAULT NULL,
  `a9` varchar(3) DEFAULT NULL,
  `a10` varchar(3) DEFAULT NULL,
  `a11` varchar(3) DEFAULT NULL,
  `a12` varchar(3) DEFAULT NULL,
  `a13` varchar(3) DEFAULT NULL,
  `a14` varchar(3) DEFAULT NULL,
  `a15` varchar(3) DEFAULT NULL,
  `a16` varchar(3) DEFAULT NULL,
  `a17` varchar(3) DEFAULT NULL,
  `a18` varchar(3) DEFAULT NULL,
  `a19` varchar(3) DEFAULT NULL,
  `a20` varchar(3) DEFAULT NULL,
  `a21` varchar(3) DEFAULT NULL,
  `a22` varchar(3) DEFAULT NULL,
  `a23` varchar(3) DEFAULT NULL,
  `a24` varchar(3) DEFAULT NULL,
  `a25` varchar(3) DEFAULT NULL,
  `a26` varchar(3) DEFAULT NULL,
  `a27` varchar(3) DEFAULT NULL,
  `a28` varchar(3) DEFAULT NULL,
  `a29` varchar(3) DEFAULT NULL,
  `a30` varchar(3) DEFAULT NULL,
  `a31` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`uattendanceID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `uattendance`
--

LOCK TABLES `uattendance` WRITE;
/*!40000 ALTER TABLE `uattendance` DISABLE KEYS */;
/*!40000 ALTER TABLE `uattendance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `update`
--

DROP TABLE IF EXISTS `update`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `update` (
  `updateID` int(11) NOT NULL AUTO_INCREMENT,
  `version` varchar(100) NOT NULL,
  `date` datetime NOT NULL,
  `userID` int(11) NOT NULL,
  `usertypeID` int(11) NOT NULL,
  `log` longtext NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`updateID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `update`
--

LOCK TABLES `update` WRITE;
/*!40000 ALTER TABLE `update` DISABLE KEYS */;
INSERT INTO `update` VALUES (1,'4.3','2019-12-09 04:03:27',1,1,'<h4>1. initial install</h4>',1);
/*!40000 ALTER TABLE `update` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user` (
  `userID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `dob` date NOT NULL,
  `sex` varchar(10) NOT NULL,
  `religion` varchar(25) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `phone` tinytext,
  `address` text,
  `jod` date NOT NULL,
  `photo` varchar(200) DEFAULT NULL,
  `username` varchar(40) NOT NULL,
  `password` varchar(128) NOT NULL,
  `usertypeID` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `modify_date` datetime NOT NULL,
  `create_userID` int(11) NOT NULL,
  `create_username` varchar(60) NOT NULL,
  `create_usertype` varchar(60) NOT NULL,
  `active` int(11) NOT NULL,
  PRIMARY KEY (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usertype`
--

DROP TABLE IF EXISTS `usertype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `usertype` (
  `usertypeID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `usertype` varchar(60) NOT NULL,
  `create_date` datetime NOT NULL,
  `modify_date` datetime NOT NULL,
  `create_userID` int(11) NOT NULL,
  `create_username` varchar(60) NOT NULL,
  `create_usertype` varchar(60) NOT NULL,
  PRIMARY KEY (`usertypeID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usertype`
--

LOCK TABLES `usertype` WRITE;
/*!40000 ALTER TABLE `usertype` DISABLE KEYS */;
INSERT INTO `usertype` VALUES (1,'Admin','2016-06-24 07:12:49','2016-06-24 07:12:49',1,'admin','Super Admin'),(2,'Teacher','2016-06-24 07:13:13','2016-06-24 07:13:13',1,'admin','Super Admin'),(3,'Student','2016-06-24 07:13:27','2016-06-24 07:13:27',1,'admin','Super Admin'),(4,'Parents','2016-06-24 07:13:42','2016-10-25 01:12:52',1,'admin','Super Admin'),(5,'Accountant','2016-06-24 07:13:54','2016-06-24 07:13:54',1,'admin','Super Admin'),(6,'Librarian','2016-06-24 09:09:43','2016-06-24 09:09:43',1,'admin','Super Admin'),(7,'Receptionist','2016-10-30 06:24:41','2016-10-30 06:24:41',1,'admin','Admin'),(8,'Moderator','2016-10-30 07:00:20','2016-12-06 06:08:38',1,'admin','Admin');
/*!40000 ALTER TABLE `usertype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vendor`
--

DROP TABLE IF EXISTS `vendor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `vendor` (
  `vendorID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `contact_name` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`vendorID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vendor`
--

LOCK TABLES `vendor` WRITE;
/*!40000 ALTER TABLE `vendor` DISABLE KEYS */;
/*!40000 ALTER TABLE `vendor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `visitorinfo`
--

DROP TABLE IF EXISTS `visitorinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `visitorinfo` (
  `visitorID` bigint(12) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(60) DEFAULT NULL,
  `email_id` varchar(128) DEFAULT NULL,
  `phone` text NOT NULL,
  `photo` varchar(128) DEFAULT NULL,
  `company_name` varchar(128) DEFAULT NULL,
  `coming_from` varchar(128) DEFAULT NULL,
  `representing` varchar(128) DEFAULT NULL,
  `to_meet_personID` int(11) NOT NULL,
  `to_meet_usertypeID` int(11) NOT NULL,
  `check_in` timestamp NULL DEFAULT NULL,
  `check_out` timestamp NULL DEFAULT NULL,
  `status` int(11) NOT NULL,
  `schoolyearID` int(11) NOT NULL,
  PRIMARY KEY (`visitorID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `visitorinfo`
--

LOCK TABLES `visitorinfo` WRITE;
/*!40000 ALTER TABLE `visitorinfo` DISABLE KEYS */;
/*!40000 ALTER TABLE `visitorinfo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `weaverandfine`
--

DROP TABLE IF EXISTS `weaverandfine`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `weaverandfine` (
  `weaverandfineID` int(11) NOT NULL AUTO_INCREMENT,
  `globalpaymentID` int(11) NOT NULL,
  `invoiceID` int(11) NOT NULL,
  `paymentID` int(11) NOT NULL,
  `studentID` int(11) NOT NULL,
  `schoolyearID` int(11) NOT NULL,
  `weaver` double NOT NULL,
  `fine` double NOT NULL,
  PRIMARY KEY (`weaverandfineID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `weaverandfine`
--

LOCK TABLES `weaverandfine` WRITE;
/*!40000 ALTER TABLE `weaverandfine` DISABLE KEYS */;
/*!40000 ALTER TABLE `weaverandfine` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'blitzrye_blitzschool'
--

--
-- Dumping routines for database 'blitzrye_blitzschool'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-02-02  0:50:01
